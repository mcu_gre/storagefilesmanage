/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */
 
#ifndef EBOOK_FILE_INFO_H
#define  EBOOK_FILE_INFO_H

#include <string>
#include <list>
#include <documentFileInfo.h>


namespace File
{

  class EBookFileInfo : public DocumentFileInfo
  {
    using DocumentFileInfo::DocumentFileInfo;		  
  public:    
    EBookFileInfo(FileSystemItem& fileInfo,bool bBuildHash);
    virtual void readMetaInfo() override;
    virtual std::string buildNewName() override;
    void setAuthor(std::string);
    void addSubject(std::string);
    void setTitle(std::string title);
    std::string getAuthor();
    std::string getUUID() { return m_uuid;}
    std::string getIsbn() {return m_isbn;}
    virtual std::string getCategorie() override;
    virtual std::list<std::string> getInfos() override;
    virtual void autoClassification() override;
  private:    
    virtual void setCategorie();
    virtual void setLevelPoint(int point,const std::string& comment);
    bool extractTitleFromFleuveNoirTitleFormat(const std::string& title);
    bool extractTitleTwoPart(std::list<std::string>& list);
    bool extractTitleThreePart(std::list<std::string>& list);
    bool extractTitle(std::list<std::string>& list);
    void extractVolumeNumber(const std::string& str);  
    // Attributs    
    std::string m_title;
    std::string m_author;
    std::string m_publisher;
    std::string m_isbn;
    std::string m_uuid;
    std::string m_serieName;
    std::string m_serieNum;
    int m_serieNumI;
    std::string m_officalCategorie;
    std::vector<std::string> m_subjects;

   static const FileInfo::MapSubjectCategory m_mapSubjectCategory;
  };
};
#endif

/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#ifndef DIRECTORY__H
#define  DIRECTORY__H

#include <string>

#include "fileInfo.h"
#include "fileSystemItem.h"
#include "container.h"

namespace File
{
  class ManageStorage;

class Directory : public FileSystemItem
{
	
public:
  // Constructor as header directory 
  Directory(ManageStorage & manageStorage);
  // Constructor of 
  Directory(ManageStorage & manageStorage, const std::string & directoryFullPath                                        ,FileSystemItem* parentDirectory);
  Directory(ManageStorage & manageStorage, const std::string & directoryPath     ,const std::string & directoryName     ,FileSystemItem* parentDirectory);
  
  // starting parse
  void scanDirectories();
  
  Directory* createDirectory(std::string);
  void move(FileInfo & file); 
  void rename(std::string newDirName); 
  virtual std::list<std::string> getInfos() override;
  std::shared_ptr<FileInfo> removeFile(FileSystemItem& fileSystemItem);
  // Debug Information methods
  void printDirectories();
  void printFiles();
  std::string printInfoDirectorie(std::string msg);

  ManageStorage & getManageStorage();

  virtual void updatePathDependancy();

  static unsigned long m_fileCount;
  static unsigned long m_folderCount;

protected:
  virtual std::string buildNewName() override;

private:
// ----------
//  Private Methods
  // Parse the current directory of this object
  void scanDirectory();
  // Parse directories of the the current directory of this object
  void scanSubDirectories();
  // Ajoute un sous reportoire local (de this): appel en cours de parsing
  Directory* AddDirectory(std::string& folderName);  
  void AddFile(FileSystemItem& fileSystemItem);
// ----------
//  Attributs
    fileAlgorithme::SFilesList  m_listFileInfo;
    fileAlgorithme::SDirectoriesList  m_SubDirectories;
    ManageStorage & m_manageStorage;
};

  typedef std::shared_ptr<Directory> SDirectories;


};

#endif

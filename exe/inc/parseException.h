/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

/**
********************************************************************************
**
**  \file Exception.hpp
**
********************************************************************************
**
**  \brief
**
********************************************************************************
**
*/

#ifndef PARSEFILE_EXCEPTION_HPP
#define PARSEFILE_EXCEPTION_HPP

#include <exception>

namespace File
{
class FileException : public std::exception
{
  public:
    enum ErrorId
    {
  Error,
    };
    FileException(const char * err):m_myerror(err){};
    FileException(const std::string err):m_myerror(err){};
    virtual const char * what () const throw ()
    {
        return m_myerror.c_str();
    };
  
  protected:  
    std::string m_myerror;
};


}
#endif //PARSEFILE_EXCEPTION_HPP



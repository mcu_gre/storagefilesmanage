/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */
 
#ifndef VIDEO_FILE_INFO_H
#define VIDEO_FILE_INFO_H

#include "audioVideoFileInfo.h"

namespace File
{  
  class VideoFileInfo : public AudioVideoFileInfo
  {
    using AudioVideoFileInfo::AudioVideoFileInfo;
    public:
      //VideoFileInfo(std::string& path,std::string& fileName,const std::size_t& iSize,bool bBuildHash);
    //virtual  std::string buildNewName() override;
     /* std::vector<Tag>& getTags();
      static std::vector<Tag> m_tags;*/
  };
};
#endif

/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#ifndef FILE_STATISTIC_H
#define  FILE_STATISTIC_H


#include <list>
#include <map>
#include <string>
#include <vector>
#include <iostream>

#include "fileSystemItem.h"

namespace File
{
 class FileStatistic
 {
	public:
	FileStatistic(std::string name,std::string strKey,std::string mother);
	FileStatistic(const FileStatistic & copy)
	{
		m_name =copy.m_name;
		m_strKey =copy.m_strKey;
		m_pMother =copy.m_pMother;
		m_count =copy.m_count;
	}
	~FileStatistic()
	{
		//printf("~FileStatistic  %s\n",m_strKey.c_str());
	}
	void setMother(const FileStatistic * mother)
	{
		//std::cout << "Set Mother from %s for" << mother->getKey() << " for "<< getKey()<< std::endl;
		//printf("Set Mother from %s for %s\n",mother->getKey().c_str(),getKey().c_str());
		m_pMother = mother;
	}
	static void add(std::string strKey);
	
	static void printAll(FileStatistic* mother);
	void print(int depth);
	
	const std::string getKey() const
	{
		return m_strKey;
	}
	std::string getName()
	{
		return m_name;
	}	
	void plus() const
	{
		m_count++;
	}
	int getCount()
	{
		return m_count;
	}	
	const FileStatistic * getMother()
	{
		return m_pMother;
	}
	
	private:
	std::string m_name;
	std::string m_strKey;
	mutable int m_count;
	const FileStatistic * m_pMother;
	//static std::vector<FileStatistic> m_list;
 };
 std::vector<FileStatistic*>& getList();
};
#endif

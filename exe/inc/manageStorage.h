/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#ifndef MANAGE_STORAGE__H
#define  MANAGE_STORAGE__H

#include <string>
#include <vector>
#include <map>
#include <list>
#include "container.h"
#include "fileInfo.h"

namespace File
{
  class Directory;

class Option
{
  public:
    enum EACTION
    {
      DUPLICATE                       =   1,
      RENAME_WRONG_NAME               =   DUPLICATE<<1 ,
      FIND_DIFFERENCE                 =   RENAME_WRONG_NAME<<1 ,
      DELETE_DUPLICATE_FILES          =   FIND_DIFFERENCE<<1,
      DELETE_DUPLICATE_MAX            =   DELETE_DUPLICATE_FILES<<1,
      FILE_CLASSIFICATION             =   DELETE_DUPLICATE_MAX<<1,
      SIMULATE                        =   FILE_CLASSIFICATION<<1,
      INFO_LISTE                      =   SIMULATE<<1,
      DELETE_DUPLICATE_FILE_BY_NAME   =   INFO_LISTE<<1,
      DELETE_DUPLICATE_FILE_BY_UUID1  =  DELETE_DUPLICATE_FILE_BY_NAME<<1,
      DELETE_DUPLICATE_FILE_BY_UUID2  =  DELETE_DUPLICATE_FILE_BY_UUID1<<1,
      COUNT_MEDIA_FILES               =  DELETE_DUPLICATE_FILE_BY_UUID2<<1
    };
    Option(EACTION optionId,std::string cmdName,std::string help):m_optionId(optionId),m_cmdName(cmdName),m_help(help){}
    EACTION m_optionId;
    std::string m_cmdName;
    std::string m_help;
};

class ManageStorage
{
  public:

    typedef std::map<std::string,Option> MapCmd;

    ManageStorage();

    void printCommands();
    void run();
    void scanDirectories();
    void addMasterDirectories(std::string path);
    void addFile(std::weak_ptr<File::FileInfo> aWfile);
    void addDir(std::shared_ptr<File::Directory>& dir);

    void addPatternKeepingFile(std::string path);
    void addPatternDeletingFile(std::string path);

    bool isAFileToKeep(FileInfo& file);
    bool isAFileToDelete(FileInfo& file);
    bool isAListWithKeepFile(fileAlgorithme::FilesList& list);

    void setAction(Option::EACTION action);
    void setSizeOfFileToHash(std::size_t size);
    void setMinFileSize(std::size_t minFileSize);
    std::size_t getMinFileSize();

    void setReportFolder(const std::string folder);
    bool isRequestedAction(Option::EACTION action);
    fileAlgorithme::SStringFileMap& getMap(){ return m_hashFilesList;}

    static std::string printAvailableCommands(std::string space);
    void addCommand(std::string);
    
  private:
    void addFilesInList(std::weak_ptr<File::FileInfo> &aWfile);
    void findDuplicateFilesFromHash();
    void findDuplicateFilesFromName();
    void findDuplicateFilesFromUUID();
    void findDuplicateFilesFromUUID2();

    void filesInfo();

   // void findDuplicateFiles_deleteFiles(const File::FileInfo::ListOfFilesList& );
    void findDuplicateFiles_deleteFiles(const fileAlgorithme::SListOfFileListExtended& aDuplicateFilesList);
    //void findDuplicateFiles_deleteFiles_classe_areprendre();
    void findUniquesFiles();
    void renameFiles();
    void fileClassification();

    void deleteFile(FileInfo& file);
    void deleteFile(fileAlgorithme::FilesListExtended& filesListExtended);
    bool isAFileToDelete(bool isAKeepOne,bool isANotKeepOne,bool keepOneFound,bool noKeepOneFoud);

// Attribute
    fileAlgorithme::SStringFileMap m_hashFilesList ;
    fileAlgorithme::SStringFileMap m_nameFilesList ;
    fileAlgorithme::SStringFileMap m_uuid1FilesList ;
    fileAlgorithme::SStringFileMap m_uuid2FilesList ;
    fileAlgorithme::WFilesList m_fileList;
    fileAlgorithme::SSizeFileMap m_sizeFilesList;
    fileAlgorithme::SDirectoriesList m_masterDirList;
    fileAlgorithme::WDirectoriesList m_dirList;
    //File::FileInfo::ListOfFilesList m_duplicateFilesList;
    fileAlgorithme::SStringFileMap m_listOfunique;

    unsigned int m_action;
    std::string m_reportFolder;
    std::size_t m_minFileSize;
    unsigned int m_fileDeleted;
    std::vector<std::string> m_patternKeepingFileList;
    std::vector<std::string> m_patternDeletingFileList;
    static const MapCmd m_mapCmd;
};
};

#endif

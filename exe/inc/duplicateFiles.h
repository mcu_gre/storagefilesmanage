/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#ifndef DUPLICATE_FILES__H
#define  DUPLICATE_FILES__H

namespace File
{

class DuplicateFiles
{
  public:
    DuplicateFiles():m_treatedFilesCount(0),m_FilesCount(0),m_FilesSize(0){};
 // private:
    //File::FileInfo::ListOfFilesList m_duplicateFilesList;
    unsigned int m_treatedFilesCount;
    unsigned int m_FilesCount;
    unsigned int m_FilesSize;
};
};

#endif

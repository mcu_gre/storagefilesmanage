/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#ifndef FILE_COMMON_DEF__H
#define  FILE_COMMON_DEF__H

#include <string>

namespace File
{
#ifndef WIN32
    #include <sys/types.h>
    #define CLEAR "clear"
    /* system("clear") pour UNIX */
const std::string separator = "/";
#else
    #define CLEAR "cls"
    /* system("cls") pour Windows */
const std::string separator = "\\";
#endif
}

#endif

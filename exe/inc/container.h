/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */
#ifndef FILTRE_CONTAINER__H
#define  FILTRE_CONTAINER__H

#include <string>
#include <map>
#include <list>
#include <iostream>
#include <memory>


namespace File
{
  class FileInfo;
  class Directory;
}
namespace fileAlgorithme
{
  typedef std::list<File::FileInfo*> FilesList;
  typedef std::list<std::shared_ptr<File::FileInfo>> SFilesList;
  typedef std::list<std::weak_ptr<File::FileInfo>> WFilesList;
  typedef std::shared_ptr<std::list<std::weak_ptr<File::FileInfo>>> SWFilesList;

  class FilesListExtended
  {
    public:
      FilesListExtended():masterFile(){};
     std::weak_ptr<File::FileInfo> masterFile;
     WFilesList m_fileList;
  };

  typedef std::list<FilesListExtended*> ListOfFileListExtended;
  typedef std::list<std::shared_ptr<FilesListExtended>> SListOfFileListExtended;

  typedef std::list<File::Directory*> DirectoriesList;
  typedef std::list<std::shared_ptr<File::Directory>> SDirectoriesList;
  typedef std::list<std::weak_ptr<File::Directory>> WDirectoriesList;

  typedef std::map<std::string,fileAlgorithme::FilesList*> StringFileMap;
  typedef std::map<std::string,SWFilesList> SStringFileMap;

  typedef std::map<std::size_t,fileAlgorithme::FilesList*> SizeFileMap;
  typedef std::map<std::size_t,SWFilesList> SSizeFileMap;
}

#endif

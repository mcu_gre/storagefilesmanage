/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#ifndef TOOLS__H
#define  TOOLS__H



#include <stdio.h>
#include <map>
#include <string>
#include <vector>
#include "container.h"

#include "directory.h"
#include "duplicateFiles.h"

//#define TIME_MEASURE tools::TimingMeasure timeMe(__FUNCTION__)
#define TIME_MEASURE
namespace tools
{
  void writeReportDuplicate(std::string & reportPath,fileAlgorithme::SListOfFileListExtended& list,File::DuplicateFiles& duplicateFiles,bool printScreen=false)  ;
  void WriteReportUnique(std::string & reportPath,fileAlgorithme::SStringFileMap& listOfunique,bool printScreen);
  std::string convertBinaryToReadeableFormat(const unsigned char* szBinary,int iSize);

  unsigned long int GetTime();
  unsigned long int TranslateTime(struct timeval timeval);
  typedef std::map<std::string,File::FileInfo::ETYPE> TypeOfFile;
  //static TypeOfFile typeOfFile;
  //void initMapFileType();

  extern int limite100;
  extern int limite1000;

  class Statistic
  {
    public:
      Statistic(const char * szFunctionName):m_functionName(szFunctionName),m_callNumber(0),m_totaleTime(0),m_instance(0)
      {
      }
      const char * m_functionName;
      unsigned long int m_callNumber;
      unsigned long int m_totaleTime;
      unsigned long int m_instance; // To manage recursivity
  };

  class TimingMeasure
  {
    public:
      typedef std::map<std::string,Statistic*> MapOfStatistics;
      typedef MapOfStatistics::iterator IteratorMapOfStatistics;
      TimingMeasure(const char * szFunctionName);
      Statistic* GetStatObj(const char * szFunctionName);
      ~TimingMeasure();
      static void ShowMap();
    private:
      Statistic * m_myStatisticObjet;
      static MapOfStatistics m_statObjets;
      unsigned long int m_tempTime;
  };
  void printInfo(int quota, int value,std::string msg );
  void printInfo(int quota, int value,std::string msg,int value2,std::string msg2 );
  void printListOfFileList(const fileAlgorithme::SListOfFileListExtended& lists);
};
#endif

/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#ifndef LOG__H
#define  LOG__H

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <string>

#define LOG_MESSAGE_ENABLE
namespace tools
{
  enum Etrace
  {
    TRACE_NONE,         //0
    TRACE_ERROR,        //1
    TRACE_WARNING,      //2
    TRACE_TIMING,       //3
    TRACE_INFO,         //4
    TRACE_DEBUG_LIGHT,  //5
    TRACE_DEBUG_MEDIUM, //6
    TRACE_DEBUG_HIGH,   //7
  } ;

  void createDir(std::string& path);
  void LogWrite(std::string  msg, Etrace level=TRACE_DEBUG_HIGH);
  std::string BuildDebugHeader(Etrace level,const char * szFile,const char * szFct,const int line);
#ifdef LOG_MESSAGE_ENABLE
#define APPLI_LOG(levelMsg,x) tools::LogWrite(tools::BuildDebugHeader(levelMsg,__FILE__,__FUNCTION__,__LINE__) + "["+(std::string(x)+ "]"),levelMsg);
#define PRD(msg) std::cout << (msg) << std::endl
#else
#define APPLI_LOG(levelMsg,x)
#define PRD(msg)
#endif
#define APPLI_TIMING(x) tools::LogWrite(tools::BuildDebugHeader(tools::Etrace::TRACE_TIMING,__FILE__,__FUNCTION__,__LINE__) + ":[timing]:" +(x),tools::Etrace::TRACE_TIMING);
  void SetLevel(Etrace level);
};
#endif

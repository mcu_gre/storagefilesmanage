/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#ifndef AUDIOVIDEO_FILE_INFO_H
#define AUDIOVIDEO_FILE_INFO_H


#include <string>
#include <vector>
#include "fileInfo.h"
#include "tag.h"

namespace  MediaInfoLib
{
class MediaInfo;
}

namespace File
{

  class AudioVideoFileInfo : public FileInfo
  {
    using FileInfo::FileInfo;
    public:
      //VideoFileInfo(std::string& path,std::string& fileName,const std::size_t& iSize,bool bBuildHash);
     virtual std::string buildNewName() override;
     virtual void autoClassification() override;
     void updateFolder();
     /* std::vector<Tag>& getTags();
      static std::vector<Tag> m_tags;*/
    protected:
      // Methods reading meta data from audio vide files
      virtual void readMetaInfo() override;
      virtual void readResolution(MediaInfoLib::MediaInfo& MI);
      virtual void readAudio(MediaInfoLib::MediaInfo& MI);
      virtual void readSubTitle(MediaInfoLib::MediaInfo& MI);
      virtual tools::NameRefactored buidRealName();
      
      // Methods to build new name from meta info read
      virtual std::string buildResolution(std::list<tools::Tag>& tags);
      virtual std::string buildAudioLanguage(std::list<tools::Tag>& tags);
      virtual std::string buildSubtitleLanguage(std::list<tools::Tag>& tags);
      virtual std::string buildFormatedName(std::string name);
      virtual bool readProductionYear(const std::string& name);   
      virtual bool readSeasonSerieInfo(const std::string& name);
      std::vector<tools::Tag>& getTags();
      unsigned int m_height;
      unsigned int m_width;
      int m_season;
      int m_episode;
      int m_productionYear;
      std::string m_audio;
      std::string m_subtitle;
      std::string m_realTitle;
  };
};
#endif

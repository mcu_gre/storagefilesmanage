/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#ifndef FILE_SYSTEM_ITEM_H
#define  FILE_SYSTEM_ITEM_H

#include <string>
#include <list>
#include "tag.h"

namespace File
{

class FileSystemItem
{

public:

  enum EFILE_TYPE
  {
    EDIRECTORY,
    ELINKLOCALDIRECTORY,
    EFILE,
    ESIMLINK,
    HEADER,
    EUNKHNOW
  };

  FileSystemItem(FileSystemItem * parent,std::string path,std::string name);

  FileSystemItem(FileSystemItem * parent,std::string path);

  virtual ~FileSystemItem();
  // Return the name (without extension)
  virtual std::string getName();
  // Set the name (without extension)
  virtual void setName(std::string name);

  // Return the name with Extension if has, implemented in daugter class
  virtual std::string getFullName();
  // Return the path without the name
  virtual std::string getPath();
  // Return the path without the name
  virtual void setPath(std::string path);
  // Return the path with the name
  virtual std::string getFullPath();

  virtual std::string buildNewName()
  {
     return getName();
  };

  virtual bool updateName(std::string &newName);
  virtual bool updateName(std::string &newName,std::string newPath);

  bool updateName();

  EFILE_TYPE whatFileIs() ;

  EFILE_TYPE getType()
  {
      return m_itemType;
  }
  std::size_t getSize()
  {
      return m_size;
  }
  FileSystemItem * getMaster()
  {
      return m_master;
  }
  FileSystemItem * getParent()
  {
      return m_parent;
  }

  void setParent(FileSystemItem * newParent)
  {
      m_parent = newParent;
  }

  virtual std::list<std::string> getInfos();

  virtual std::vector<tools::Tag>& getTags();

  virtual void updatePathDependancy(){};
  static const std::string m_magicName;

  /*!
   *  \brief : parse the file name in order to try to retrieve effective name (without tags pubs ...)
   *              - remove bracket
   *              - remove some tags
   *              - retrieve from tag some information
   *
   *  \return the transformed name
   */
  tools::NameRefactored extractUsefullInfoFromFileName();
  
 protected:

  // Contient le path du top auquel appartient cet element (nom de l item non compris)
  FileSystemItem * m_parent;
  FileSystemItem * m_master;
  // Contient le path (nom de l item non compris)
  std::string m_path;
  // Contient uniquement le nom (sans l extension pour un fichier)
  std::string m_name;
  EFILE_TYPE m_itemType;
  std::size_t m_size;
};

};

#endif

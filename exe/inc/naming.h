/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */
 
#ifndef NAMING__H
#define  NAMING__H



namespace tools
{
   int testSimilitude( std::string c1,std::string c2 );
   int jacardSimilitude( std::string c1,std::string c2 );
};
#endif

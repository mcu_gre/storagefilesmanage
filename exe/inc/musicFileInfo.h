/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#ifndef MUSIC_FILE_INFO_H
#define MUSIC_FILE_INFO_H

#include "audioVideoFileInfo.h"

namespace File
{
  class MusicFileInfo : public AudioVideoFileInfo
  {
    using AudioVideoFileInfo::AudioVideoFileInfo;	  	  
   /* public:
    //MusicFileInfo(std::string& path,std::string& fileName,const std::size_t& iSize,bool bBuildHash);*/
  };
};
#endif

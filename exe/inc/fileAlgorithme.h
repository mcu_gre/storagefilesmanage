/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */
#ifndef FILE_ALGORITHME__H
#define  FILE_ALGORITHME__H

#include <string>
#include <iostream>

#include "container.h"
#include "duplicateFiles.h"
#include "log.h"
#include "tools.h"

namespace fileAlgorithme
{

  //using namespace TOOLS;


template<typename MAP,typename KEY>
void addInMap(MAP& theMap, const KEY& theKey, std::weak_ptr<File::FileInfo> theItem)
{
  auto sptFile = theItem.lock()  ;
  File::FileInfo::ETYPE type = File::FileInfo::getType(sptFile->getFullName());


  if(type!=File::FileInfo::UNKNOW /*&& type!=File::FileInfo::PHOTO */)
  {
    auto it = theMap.find(theKey);

    if (it != theMap.end())
    {
      APPLI_LOG(tools::TRACE_INFO,"existing item in hash map " + sptFile->getFullPath());
      ((it->second))->push_back(theItem);
    }
    else
    {
      APPLI_LOG(tools::TRACE_DEBUG_HIGH,"NEW item in hash map " + sptFile->getFullName());
      // KEY not found: first occurence, allocate a file list
      SWFilesList newList = std::make_shared<WFilesList> ();
      newList->push_back(theItem);
      theMap[theKey] = newList;
    }
  }
}


/*************************************************************************************************/

// Entree file list
// Sortie FilesListExtended
typedef  fileAlgorithme::FilesListExtended (*FilesListTreatment)(const WFilesList&) ;

// Entree map
// Sortie  list of file lists
template<typename MAP>
fileAlgorithme::SListOfFileListExtended buildListOfDuplicate(MAP map, FilesListTreatment filesListTreatment)
{
  SListOfFileListExtended listOfile;
  for(auto listFile: map)
  {
    if(listFile.second->size()>1)
    {
       fileAlgorithme::FilesListExtended newList = filesListTreatment(*listFile.second);
       if(newList.m_fileList.size()>1)
       {
         auto  newSharedList = std::make_shared<fileAlgorithme::FilesListExtended> (newList);         
         listOfile.push_back(newSharedList);
       }
    }
  }
  return listOfile;
    //findDuplicateFiles_deleteFiles
}

// Entry: a file
// Output: a key and a treatment on the file
template<typename KEY>
using FileTreatment = KEY (*)(File::FileInfo& file);


// Entry a map
// Outup: a new MAP with the KEY as key
template<typename MAPIN,typename MAPOUT, typename KEY>
File::DuplicateFiles  findDuplicateFiles(const MAPIN& mapin,MAPOUT& mapout, FileTreatment<KEY> fileTreatment)
{
  std::cout << std::endl <<"Start duplicate file analyzed (Comput Algo hash)" << std::endl;
  File::DuplicateFiles duplicateFiles;
  for(auto mapItem:mapin)
  {
    // If several files have the same size: compute algo hash to find possible duplicate file
    if(mapItem.second->size()>1)
    {
      for(auto weakFileItem: *mapItem.second)
      {
        auto  fileItem = weakFileItem.lock();		  
        KEY key = fileTreatment(*fileItem);
        addInMap(mapout, key, weakFileItem);
        duplicateFiles.m_FilesCount++;
        duplicateFiles.m_treatedFilesCount++;
        duplicateFiles.m_FilesSize+=fileItem->getSize();
      }
    }
    else
    {
      duplicateFiles.m_FilesCount++;
    }
    tools::printInfo(tools::limite1000,duplicateFiles.m_FilesCount,"Files analyzed:",duplicateFiles.m_treatedFilesCount,"Files computed:");
  }
  tools::printInfo(1,duplicateFiles.m_FilesCount,"Files analyzed:",duplicateFiles.m_treatedFilesCount,"Files computed:");
  std::cout << std::endl;
  return duplicateFiles;
}



// aListOfFiles: a list of file with same name
// Output: a list of file with same name and:
//    -- Well formated
//    -- with a 80 % same size
fileAlgorithme::FilesListExtended listTreatmentDuplicateName(const WFilesList& aListOfFiles);


fileAlgorithme::FilesListExtended listTreatmentDuplicateHash(const WFilesList& aListOfFiles);

std::string fileTreatementHashAlgo(File::FileInfo& file);

std::string fileTreatementHash(File::FileInfo& file);

void removeBetterFile(fileAlgorithme::FilesListExtended& filesListExtended);

}// End namespace

#endif

/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#ifndef FILE__INFO_H
#define  FILE__INFO_H


#include <list>
#include <map>
#include <string>
#include <vector>

#include "fileSystemItem.h"
#include "fileInfo.h"

namespace File
{
  class ManageStorage;
  class Directory;

class FileInfo : public FileSystemItem
{
  public:
    enum ETYPE
    {
      UNKNOW,
      EBOOK,
      DOCUMENT,
      VIDEO,
      PHOTO,
      MUSIC,
      NOEXT
    } ;

    enum EHASH
    {
      NO_HASH,
      ALGO_HASH,
      FULL_HASH,
    } ;


    typedef std::map<std::string,File::FileInfo::ETYPE> TypeOfFile;
    typedef std::map<std::string,std::string> MapSubjectCategory;

    static const unsigned int m_maxSize=100*1024*1024;

    FileInfo(Directory& parent,std::string& path,std::string& fileName,const std::size_t& iSize,bool bBuildHash);
    FileInfo(FileSystemItem& fileInfo,bool bBuildHash);

    void buildHashFromAlgo();
    void buildHash(std::size_t iSize=0) ;
    void deleteFile();

    long getMe();

    virtual std::string getCategorie();

    bool isBinaryEqual(FileInfo& otherFileInfo);
    virtual std::string getFullName() override;
    virtual std::string getExtension()
    {
      return m_extension;
    }
    virtual void setExtension(std::string ext)
    {
      m_extension = ext;
    }
    bool isDuplicate()
    {
      return m_isADuplicate;
    }

    bool isWellFormated()
    {
      return m_isWellFormated;
    }

    void setDuplicate(bool isADuplicate)
    {
      m_isADuplicate = isADuplicate;
    }
    bool operator==(FileInfo& otherFileInfo);

    File::FileInfo::ETYPE getExtension(std::string& fileName);
    static bool getExtension(std::string fileName,std::string  ext);
    static File::FileInfo::ETYPE getType(std::string fileName);
    static void initMapFileType();

    std::string getUUId1();
    std::string getUUId2();
    
    std::string getHash();
    const unsigned char* getBinaryHash();
    unsigned int getHashSize()
    {
      return m_hashSize;
    }
    static void setMaxDataSize(std::size_t size);

    virtual std::list<std::string> getInfos() override;
    virtual void autoClassification() {};
    virtual void readMetaInfo();
    unsigned int getFormatingLevel();
    
  protected:
    void openFile();
    void closeFile();
    void initHashAlgo();
    void initHashAlgo2();
    void splitName(std::string& fileName);

    // **  Attributes  ** //
    bool m_buildHash;
    std::size_t m_hashState;
    std::string m_extension;
    static const std::size_t m_hashSize=16;
    static std::size_t m_maxHashDataSrc;//512//1024;//1024*1024;
    static const std::size_t m_minBlockSize=100;
    static const float m_pourcentOfFileToHash;
    unsigned int m_nbrPart=5;
    std::size_t m_blockSize;
    unsigned int m_offset;
    unsigned char m_hash[m_hashSize+1];
    std::string m_hashStr;
    bool m_isADuplicate;
    std::ifstream * m_theFile;
    static TypeOfFile typeOfFile;
    bool m_isMetaInfoRead;
    bool m_isWellFormated;
    std::string m_uuid1;
    std::string m_uuid2;
    unsigned int m_formatingLevel;
  private:
   void buildObject(bool bBuildHash);
   EHASH m_hashStatus;
};

  
 class StaticInit
 {
   public:
    StaticInit()
    {
      FileInfo::initMapFileType();
    };
    ~StaticInit()
    {
		//printf("~StaticInit\n");
	}
 };
 
};
#endif

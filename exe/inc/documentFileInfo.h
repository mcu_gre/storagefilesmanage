/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */
 
#ifndef DOCUMENT_FILE_INFO_H
#define  DOCUMENT_FILE_INFO_H

#include <fileInfo.h>

namespace File
{
  class DocumentFileInfo : public FileInfo
  {
    using FileInfo::FileInfo;		  
    public:
    //DocumentFileInfo(std::string& path,std::string& fileName,const std::size_t& iSize,bool bBuildHash);
  };
};
#endif

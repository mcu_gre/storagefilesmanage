/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */


#include "MobiBook.h"
#include "Epub.h"
#include "Locale.h"
#include <iostream>
#include <string>
#include <algorithm>
#include "eBookFileInfo.h"
#include "stringtools.h"
#include "converter.h"
#include "log.h"
#include "directory.h"

using std::string;
using std::cerr;
using namespace std;

namespace File
{

 const FileInfo::MapSubjectCategory EBookFileInfo::m_mapSubjectCategory = {
{".Policier","Policier"},
{"Action & Adventure","Action-Aventure"},
{"Action & Aventure","Action-Aventure"},
{"Action Adventure","Action-Aventure"},
{"Action And Adventure","Action-Aventure"},
{"Action Aventure","Action-Aventure"},
{"Actualité","Thriller,Historique"},
{"Adolescence","Adolescent"},
{"Adolescent","Adolescent"},
{"Adolescents","Adolescent"},
{"Adventure","Action-Aventure"},
{"Anthologie De Nouvelles De L'Âge D'Or De La Sf","Science-Fiction"},
{"Anti-Globalization Movement","Societe"},
{"Anticipation","Science-Fiction"},
{"Anticipation - Fiction","Science-Fiction"},
{"Anticipation Post Apocalyptique","Science-Fiction"},
{"Anticipation-Fiction","Science-Fiction"},
{"Anticipation-Science Fiction","Science-Fiction"},
{"Apocalypse","Fantastique"},
{"Apocalypse, Cité Bulle, Catastrophe Nucléaire, Projet Mars, Embryons, Clones","Fantastique"},
{"Asimov Universe","Science-Fiction"},
{"Auto-Biographie","Biographie"},
{"Autobiographie","Biographie"},
{"Aventure","Action-Aventure"},
{"Aventure Fantastique","Fantastique"},
{"Aventures","Action-Aventure"},
{"Aventures - Jeunesse","Action-Aventure"},
{"Bandes Dessinées","BD"},
{"Biographie & Autobiographie","Biographie"},
{"Biographie Et Autobiographie","Biographie"},
{"Biographie Histoire Aventure","Biographie"},
{"Biographies Et Mémoires","Biographie"},
{"Biography & Autobiography","Biographie"},
{"Bit Lit","Bit-Lit"},
{"Bit-Lit","Bit-Lit"},
{"Camus","Litterature"},
{"Cantos D'Hyperion","Science-Fiction"},
{"Children'S Books","Enfant"},
{"Children'S Ebooks","Enfant"},
{"Children'S Stories","Enfant"},
{"Chroniques Cheysulis","Fantasy"},
{"Ciencia Ficción, Terror","Fantastique"},
{"City And Town Life","Roman"},
{"Classics","Classique"},
{"Classique","Classique"},
{"Classique Anglais","Classique"},
{"Classique Français","Classique"},
{"Classiques","Classique"},
{"Collections Littéraires","Litterature"},
{"Colombie","Roman"},
{"Comédie","Ete"},
{"Conte Musical","Contes"},
{"Contes","Contes"},
{"Contes & Jeunesse & Nouvelles","Contes"},
{"Contes De Fée, Folklore & Mythologie","Contes"},
{"Contes Et Légendes","Contes"},
{"Contes Pour Adultes","Contes"},
{"Contes Revisités","ContesRevisites"},
{"Créatures Fantastiques","Fantastiques"},
{"Créatures Surnaturelles","Fantastiques"},
{"Crime","Policier"},
{"Crime Fiction","Policier"},
{"Critique Littéraire","Critique"},
{"Critique Sociale","Critique"},
{"Cycle De Tschaï","Science-Fiction"},
{"Cycle Le Cycle Des Dieux","Fantastique"},
{"Dark Fantasy","Fantasy"},
{"Défis Fantastiques","Fantastique"},
{"Detective And Mystery Stories","Policier"},
{"Détective Privé","Policier"},
{"Documentaire","Documentaire-Essaie"},
{"Documents - Essais","Documentaire-Essaie"},
{"Documents - Essais - Historique","Documentaire-Essaie"},
{"Documents Essais","Documentaire-Essaie"},
{"Documents Essais Historique","Documentaire-Essaie"},
{"Documents Et Essais","Documentaire-Essaie"},
{"Dragons","Fantastique"},
{"Drama","Romans"},
{"Drame","Romans"},
{"Dystopie - Science Fiction","Science-Fiction"},
{"Egypte","Roman"},
{"English Fiction","Romans"},
{"Enquête","Policier"},
{"Epouvante","Horreur"},
{"Epouvanteur","Fantastique"},
{"Erotica","Erotique"},
{"Erotique","Erotique"},
{"Érotique","Erotique"},
{"Érotisme","Erotique"},
{"Espionage","Espionage"},
{"Espionnage","Espionage"},
{"Essai","Documentaire-Essaie"},
{"Essai Littérature","Documentaire-Essaie"},
{"Essais","Documentaire-Essaie"},
{"Essais Politique","Documentaire-Essaie"},
{"Extra-Terrestre","Fantastique"},
{"Extraterrestres","Fantastique"},
{"Fantastique","Fantastique"},
{"Fantastique - Science-Fiction","Fantastique"},
{"Fantastique & Sf","Fantastique"},
{"Fantastique Et Science-Fiction","Fantastique"},
{"Fantastique Et Sf","Fantastique"},
{"Fantastique Et Terreur","Fantastique"},
{"Fantastique Science-Fiction","Science -Fiction"},
{"Fantastique, Nouvelle","Fantastique"},
{"Fantastique/Fantasy","Fantasy"},
{"Fantastique/Terreur/Horreur","Fantastique"},
{"Fantastiques","Fantastique"},
{"Fantasy","Fantasy"},
{"Fantasy - Mythique/Historique","Fantasy"},
{"Fantasy / Science-Fiction","Fantasy"},
{"Fantasy & Magic","Fantasy"},
{"Fantasy 13 Ans+","Fantasy"},
{"Fantasy 9782811235857","Fantasy"},
{"Fantasy And Magic","Fantasy"},
{"Fantasy Épique","Fantasy"},
{"Fantasy Et Terreur","Fantasy"},
{"Fantasy Fiction","Fantasy"},
{"Fantasy Française","Fantasy"},
{"Fantasy Gay","Fantasy"},
{"Fantasy Héroïque","Fantasy"},
{"Fantasy Historique","Fantasy"},
{"Fantasy Jeunesse","Fantasy"},
{"Fantasy Mediévale","Fantasy"},
{"Fantasy Québécoise","Fantasy"},
{"Fantasy Series","Fantasy"},
{"Fantasy Urbaine","Fantasy"},
{"Fantasy-Fantastique-Sf","Fantasy"},
{"Fantasy, Mythes, Moyen Age, Bouclier, Croisades, Récit Épique,","Fantasy"},
{"Fantôme","Fantastique"},
{"Fantomes","Fantastique"},
{"Fantômes","Fantastique"},
{"Fcience Fiction","Science-Fiction"},
{"Fiction","Roman"},
{"Fiction Et Littérature","Romans"},
{"Fiction Romanesque","Romans"},
{"Fiction Spécial 27","Romans"},
{"Foreign Language Study","Classique"},
{"French Fiction","Roman"},
{"French Literature","Litterature"},
{"French Poetry","Poesie"},
{"General Fiction","Romans"},
{"Genre Roman","Roman"},
{"Genre Sf","Science -Fiction"},
{"Genre:Sciencefiction","Science-Fiction"},
{"Geopolitics","Geopolitics"},
{"Guerre","Guerre"},
{"Guerre 1939/1945","Guerre"},
{"Guerre D'Algérie","Guerre"},
{"Guerre De Sécession","Guerre"},
{"Guerres","Guerre"},
{"Guerrière","Guerre"},
{"Guerriers","Guerre"},
{"Guide Pratique","Societe"},
{"Harry Potter","Fantasy"},
{"Harry Potter Et Les Reliques De La Mort","Fantasy"},
{"Heroic Fantasy","Fantasy"},
{"Heroïc Fantasy","Fantasy"},
{"Heroic-Fantasy","Fantasy"},
{"Histoire","Histoire"},
{"Historical","Histoire"},
{"Historical Fiction","FictionHistorique"},
{"Historique","Histoire"},
{"History","Histoire"},
{"History & Criticism","Histoire"},
{"History And Criticism","Histoire"},
{"Horreur","Fantastique"},
{"Horror","Fantastique"},
{"Horror Fiction","Fantastique"},
{"Horror Films","Fantastique"},
{"Humorous Fiction","Roman"},
{"Humour","Humour"},
{"Intelligence Service","Geopolitics"},
{"Intimacy Psychology)","Roman"},
{"Japonais","Roman"},
{"Jeunesse","Enfant"},
{"L'échiquier Du Mal","Fantastique"},
{"La Tour Sombre","Fantastique"},
{"Le Chant Des Sorcières","Fantastique"},
{"Le Cycle D'Ea","Fantastique"},
{"Lit. Américaine","Litterature"},
{"Lit. Fr","Litterature"},
{"Lit. Française","Litterature"},
{"Lit. Sud Américane","Litterature"},
{"Lit.Sud Américane","Litterature"},
{"Literary","Litterature"},
{"Literary Collections","Litterature"},
{"Literary Criticism","Litterature"},
{"Literary Fiction","Litterature"},
{"Literature & Fiction","Litterature"},
{"Litt. Étrangère","Litterature"},
{"Litt. Française","Litterature"},
{"Litt. Usa","Litterature"},
{"Litt.étrangère","Litterature"},
{"Litt.Française","Litterature"},
{"Litt.Générale","Litterature"},
{"Litt.Usa","Litterature"},
{"Litterature","Litterature"},
{"Littérature","Litterature"},
{"Littérature Allemande","Litterature"},
{"Littérature Américaine","Litterature"},
{"Littérature Américaine Et Canadienne","Litterature"},
{"Littérature Anglaise","Litterature"},
{"Littérature Anglo-Saxonne","Litterature"},
{"Litterature Asiatique","Litterature"},
{"Littérature Canadienne","Litterature"},
{"Littérature Classique","Litterature"},
{"Littérature Colombienne","Litterature"},
{"Littérature égyptienne","Litterature"},
{"Littérature Étrangère","Litterature"},
{"Littérature étrangère","Litterature"},
{"Littérature Fantasy","Litterature"},
{"Littérature Francaise","Litterature"},
{"Littérature Française","Litterature"},
{"Littérature Francophone","Litterature"},
{"Littérature Générale","Litterature"},
{"Littérature Germanique","Litterature"},
{"Littérature Hispanique","Litterature"},
{"Littérature Islandaise","Litterature"},
{"Littérature It","Litterature"},
{"Littérature Italienne","Litterature"},
{"Littérature Japonaise","Litterature"},
{"Littérature Latino-Américaine","Litterature"},
{"Littérature Moderne","Litterature"},
{"Littérature Quebecoise","Litterature"},
{"Littérature Québécoise","Litterature"},
{"Littérature Sentimentale","Litterature"},
{"Littérature Sud-Américaine","Litterature"},
{"Littérature Suédoise","Litterature"},
{"Littérature Suisse Alémanique","Litterature"},
{"Littérature, Fantasy, Animae, Fantastique, Métamorphose","Litterature"},
{"Littérature, Fantasy, Fantastique, Métamorphose","Litterature"},
{"Massage","Massage"},
{"Médiéval","Medieval"},
{"Médiéval Fantastique","Fantastique"},
{"Mystery & Detective","Policier"},
{"Mystery And Detective Stories","Policier"},
{"Nouvelle De Sf","Science-Fiction"},
{"Nouvelles Sf Fantastique","Fantastique"},
{"Paranormal & Urban","Fantastique"},
{"Paranormal Fiction","Fantastique"},
{"Paranormal Romance","Fantastique"},
{"Philosophie","Philosophie"},
{"Philosophie - Spiritualité","Philosophie"},
{"Philosophie, Bouddhisme, Essai","Philosophie"},
{"Philosophy","Philosophie"},
{"Poésie","Poesie"},
{"Poésie Française","Poesie"},
{"Polar","Policier"},
{"Polar-Thriller","Policier"},
{"Police","Policier"},
{"Policer","Policier"},
{"Policier","Policier"},
{"Policier - Crime - Mystère","Policier"},
{"Policier - Mystère","Policier"},
{"Policier - Série Noire","Policier"},
{"Policier & Mystère","Policier"},
{"Policier & Thriller","Policier"},
{"Policier Américain","Policier"},
{"Policier Anglais","Policier"},
{"Policier Crime Mystère","Policier"},
{"Policier Fantastique","Policier"},
{"Policier Français","Policier"},
{"Policier Historique","Policier"},
{"Policier Mystère","Policier"},
{"Policier Néo-Zélandais","Policier"},
{"Policier Thriller","Policier"},
{"Policier, Historique","Policier"},
{"Policier, Historique, Thriller","Policier"},
{"Policier, Thriller","Policier"},
{"Policier,Thriller","Policier"},
{"Policier/Thriller","Policier"},
{"Policier&Thriller","Policier"},
{"Policiers","Policier"},
{"Policiers & Mystères","Policier"},
{"Policiier Mystère Crime","Policier"},
{"Récit Historique","RecitHistorique"},
{"Roman","Roman"},
{"Roman - Fantasy","Roman"},
{"Roman 12 Ans +","Roman"},
{"Roman 13 Ans +","Roman"},
{"Roman 13 Ans+","Roman"},
{"Roman 8-12 Ans","Enfant"},
{"Roman Classique","Roman"},
{"Roman D'Aventures","Roman"},
{"Roman De Cape Et D'Épée","Roman"},
{"Roman Egyptien","Roman"},
{"Roman Égyptien","Roman"},
{"Roman égyptien","Roman"},
{"Roman Espion","Roman"},
{"Roman Fantastique","Roman"},
{"Roman Français","Roman"},
{"Roman Héroique","Roman"},
{"Roman Historique","Roman"},
{"Roman Jeunesse","Roman"},
{"Roman Jeunesse À Partir De 10 Ans","Roman"},
{"Roman Jeunesse À Partir De 12 Ans","Roman"},
{"Roman Jeunesse À Partir De 13 Ans","Roman"},
{"Roman Jeunesse À Partir De 15 Ans","Roman"},
{"Roman Jeunesse, À Partir De 12 Ans","Roman"},
{"Roman Jeunesse, À Partir De 13 Ans","Roman"},
{"Roman Noir","Roman"},
{"Roman Policier","Policier"},
{"Roman, Écologie","Roman"},
{"Romance","Roman"},
{"Romance Contemporaine","Roman"},
{"Romance Fantastique","Roman"},
{"Romance Gay","Roman"},
{"Romance Historique","Roman"},
{"Romance Moyen-Age","Roman"},
{"Romance Paranormale","Roman"},
{"Romance Régence","Roman"},
{"Romans","Roman"},
{"Romans Classiques","Classique"},
{"Romans Et Nouvelles","Roman"},
{"Romans Policier & Mystère","Policier"},
{"Romantique","Roman"},
{"S F","Science-Fiction"},
{"S. Fiction","Science-Fiction"},
{"S.-F.","Science-Fiction"},
{"S.F.","Science-Fiction"},
{"Science Fiction","Science-Fiction"},
{"Science Fiction & Fantasy","Science-Fiction"},
{"Science-Fiction","Science-Fiction"},
{"Science-Fiction 9781473204867","Science-Fiction"},
{"Science-Ficton","Science-Fiction"},
{"Scifi","Science-Fiction"},
{"Scinece-Fiction","Science-Fiction"},
{"Serial Murders","Policier"},
{"Série De Science-Fiction Post-Apocalyptique","Science-Fiction"},
{"Sf","Science-Fiction"},
{"Sf (Nouvelles)","Science-Fiction"},
{"Sf / Post Apocalypse","Science-Fiction"},
{"Sf 9782848591528","Science-Fiction"},
{"Sf Fantastique Fantasy","Science-Fiction"},
{"Sf Fantasy","Science-Fiction"},
{"Sf Fr","Science-Fiction"},
{"Sf Nouvelles","Science-Fiction"},
{"Sf-Space","Science-Fiction"},
{"Sf, Abprod'","Science-Fiction"},
{"Sf, Star Trek","Science-Fiction"},
{"Sf, Star Wars","Science-Fiction"},
{"Sf, Warhammer40K","Science-Fiction"},
{"Sf,Star Trek","Science-Fiction"},
{"Sf,Star Wars","Science-Fiction"},
{"Sf,Star Wars,Jeunesse","Science-Fiction"},
{"Sf/ Post Apocalypse","Science-Fiction"},
{"Sf/Anticipation","Science-Fiction"},
{"Sf/Post Apocalyps","Science-Fiction"},
{"Sf/Post Apocalypse","Science-Fiction"},
{"Sf/Postapocalypse","Science-Fiction"},
{"Shakespeare","Classique"},
{"Space Opera","Science-Fiction"},
{"Space Opéra","Science-Fiction"},
{"Space-Opera","Science-Fiction"},
{"Spaceopera","Science-Fiction"},
{"Star","Science-Fiction"},
{"Star Trek","Science-Fiction"},
{"Star Wars","Science-Fiction"},
{"Suspence","Policier"},
{"Suspens","Policier"},
{"Suspense","Policier"},
{"Suspense Fiction","Policier"},
{"Techno-Thriller","Techno-Thriller"},
{"Terreur","Terreur"},
{"Théatre","Theatre"},
{"Théâtre","Theatre"},
{"Théâtre - Historique","Theatre"},
{"Thriller","Thriller"},
{"Thriller 147 Pages","Thriller"},
{"Thriller À Cannes","Thriller"},
{"Thriller Fantastique","Thriller"},
{"Thriller Historique","Thriller"},
{"Thriller, Historique","Thriller"},
{"Thriller,Historique","Thriller"},
{"Thrillers","Thriller"},
{"Thrillers & Suspense","Thriller"},
{"Thrillers Post-Apocalyptiques","Thriller"},
{"Vaisseau Spatial","Science-Fiction"},
{"Western","Western"},


};

EBookFileInfo::EBookFileInfo(FileSystemItem& fileInfo,bool bBuildHash)
              :DocumentFileInfo(fileInfo,bBuildHash)
{
}

std::string EBookFileInfo::buildNewName()
{
  readMetaInfo();
  std::string newName;
  if (m_author.size()!=0&& m_title.size()!=0)
  {
    newName = m_author+ "-" + m_title;

    if(m_serieName.size()!=0)
    {
      newName+= "-[" +m_serieName;

      if(m_serieNum.size()!=0)
      {
        newName+= "-"+ m_serieNum ;
      }
      newName+= "]";
    }
  }
  return newName;
}

void EBookFileInfo::readMetaInfo()
{
  if(!m_isMetaInfoRead)
  {
    Ebook * m = nullptr;
    string file = getFullPath();
    APPLI_LOG(tools::TRACE_DEBUG_MEDIUM,"start");
    if(getExtension().compare("mobi")==0)
    {
      m = (Ebook*) MobiBook::createFromFile(file.c_str());
    }
    else
    if(getExtension().compare("epub")==0)
    {
      m = (Ebook*) Epub::createFromFile(file.c_str());
    }

    if(m==nullptr)
    {
      cerr << "Unable to open ebook " << file << std::endl;
      m_isWellFormated = false;
    }
    else
    {
      m_isWellFormated = true;
      //std::cout << "analzye:" << getFullPath()<<std::endl;
      
      APPLI_LOG(tools::TRACE_DEBUG_HIGH,"Title:["+m->getTitle()+"]\n");
      APPLI_LOG(tools::TRACE_DEBUG_HIGH,"Autor:["+m->getAuthor()+"]\n");
      APPLI_LOG(tools::TRACE_DEBUG_HIGH,"Publisher:["+m->getPublisher()+"]\n");

      setTitle(m->getTitle());
      setAuthor(m->getAuthor());
      m_publisher = m->getPublisher();
      m_isbn = tools::trim(m->getIsbn());

      if(m_isbn.size()!=0)
      {
        m_uuid1 = m_isbn;
        setLevelPoint(1,"isbn");
      }
      else
      {
        m_uuid1 = tools::generateUUID();
      }

      m_uuid2 = tools::trim(m->getUUID());
      if(m_uuid2.size()==0)
      {
        m_uuid2 = tools::generateUUID();
      }
      std::string serieName = tools::replaceForbiddenCharacter(tools::trim(m->getSerieName()),'-');

      int serieNum  = std::atoi(m->getSerieNum().c_str());

      if(serieName.size()!=0)
      {
        if(m_serieName.size()==0)
        {
          setLevelPoint(3,"metainfo serie name");
        }
        m_serieName = serieName;
      }

      if(serieNum!=0)
      {
        m_serieNum = std::to_string( serieNum);
      }
      // Traitement à faire: Supprimer espace superfleu, mettre premier mot en majuscule
      for(auto sub:  m->getSubject())
      {
        addSubject(tools::trim(sub));
        //   std::cout << "subject:\t\t" << tools::trim(sub) << std::endl;
      }
      std::sort(m_subjects.begin(), m_subjects.end());
      setCategorie();
#if 0
      std::cout << "file:\t\t" << file << std::endl;
      std::cout << "Title:\t\t" << m_title << std::endl;
      std::cout << "Author:\t\t" <<m_author << std::endl;
      std::cout << "Publisher:\t" << m_publisher << std::endl;
      std::cout << "ISBN:\t" << m_isbn << std::endl;
      std::cout << "UUID:\t" << m_uuid << std::endl;
      std::cout << "Serie:\t" << m_serieName << std::endl;
      std::cout << "Num of serie:\t" << m_serieNum << std::endl;
      for(auto sub:  m->getSubject())
      {
        std::cout << "subject:\t\t" << tools::trim(sub) << std::endl;
      }
        /*
       std::cout << "Language:\t" << Locale::getName(m->getLocale()) << std::endl;
       std::cout << "Length:\t\t" << m->getTextSize() << std::endl;
       std::cout << "Images:\t\t" << m->imagesCount << std::endl;
       */
#endif
      delete m;
    }
    m_isMetaInfoRead = true;
  }
}

//bool EBookFileInfo::setTitle
bool EBookFileInfo::extractTitle(std::list<std::string>& list)
{
  bool isAuterSet = false;
  switch(list.size())
  {
    case 1:
      m_title = tools::trim(list.front());
      isAuterSet = true;
    break;
    case 2:
      isAuterSet = extractTitleTwoPart(list);
    break;
    case 3:
      isAuterSet = extractTitleThreePart(list);
    break;
    default:
    break;
  }
  return isAuterSet;
}

void EBookFileInfo::extractVolumeNumber(const std::string& str)
{
  std::list<std::string> tagVol = {"","Tome","Episode","Vol.","ép.","T","volume"};

  for(auto tag: tagVol)
  {
    try
    {
      m_serieNumI = tools::extractNumberFromString(str,&tag);
      return;
    }
    catch(std::exception & e)
    {
    }
  }
  throw std::exception();
  return ;
}

bool EBookFileInfo::extractTitleThreePart(std::list<std::string>& list)
{
  bool isAuterSet = false;
  try
  {
    // First: Serie
    m_serieName = list.front();
    list.pop_front();
    // Second Num
    extractVolumeNumber(list.front());
    list.pop_front();
    // Third: Name
    m_title = list.front();
    setLevelPoint(2,"setTitle serie name");
    isAuterSet = true;
  }
  catch(std::exception & e)
  {
    m_serieName ="";
    m_serieNum ="";
    m_title ="";
  }
  return isAuterSet;
}

// Serie - Titre
bool EBookFileInfo::extractTitleTwoPart(std::list<std::string>& list)
{
  // First: Serie
  m_title = tools::trim(list.front());
  list.pop_front();
  // Second: Title
  setAuthor(list.front());
  //m_author = tools::trim(list.front());
  return true;
}

bool EBookFileInfo::extractTitleFromFleuveNoirTitleFormat(const std::string& title)
{
  bool isAuterSet = false;

  std::size_t fnaPos = title.find("FNA ");

  if(fnaPos!=std::string::npos)
  {
    auto list = tools::split(title,'-',true);

    if(fnaPos==0)
    {
      std::size_t nbreItem= list.size();
      if(nbreItem!=1)
      {
        // Remove FNA
        list.pop_front();
        isAuterSet = extractTitle(list);
      }
      else
      {
        //FNA 0710-Bera Paul-La nuit est morte
        list =  tools::split(title,'-',false);
        // Remove FNA
        list.pop_front();
        isAuterSet =  extractTitle(list);
      }
    }
    else
    //[Sassar - FNA 1717]:  [TITLE - FNA INT]
    {
      m_title = tools::trim(list.front());
      isAuterSet = true;
    }
  }
  return isAuterSet;
}

void EBookFileInfo::setTitle(std::string title)
{
  bool isAuterSet=false;
  if(title.size()!=0)
  {
    setLevelPoint(1,"setTitle title !=0");
  }
  isAuterSet = extractTitleFromFleuveNoirTitleFormat(title);
  if(!isAuterSet)
  {
    auto list = tools::split(title,'-');
    if(list.size()>1)
    {
       isAuterSet=extractTitle(list);
    }
    else
    {
     list = tools::split(title,' ');
     isAuterSet=extractTitle(list);
    }
  }
  if(!isAuterSet)
  {
    m_title = title;
  }
  m_title=tools::trim(m_title);
  m_title=tools::replaceForbiddenCharacter(m_title,',');
  // Debug
  APPLI_LOG(tools::TRACE_DEBUG_HIGH,"title:"+title);
  APPLI_LOG(tools::TRACE_DEBUG_HIGH,"m_title:"+m_title);
  APPLI_LOG(tools::TRACE_DEBUG_HIGH,"m_author:"+m_author);
  APPLI_LOG(tools::TRACE_DEBUG_HIGH,"m_serieName:"+m_serieName);
  APPLI_LOG(tools::TRACE_DEBUG_HIGH,"m_serieNum:"+m_serieNum);

//
}
void EBookFileInfo::setAuthor(std::string author)
{
  // Balzac, Honoré de (1799-1850)
  // Manage composed name with -
  m_author="";
  auto list = tools::split(author,'-',true);
  if(list.size()>2)
  {
    try
    {
      // First: autor
      author = list.front();
      list.pop_front();

      // Second serie
      m_serieName = list.front();
      list.pop_front();

      // Third: serie number
      m_serieNum = std::to_string(std::stoi(list.front()));
      list.pop_front();

      setLevelPoint(2,"setTitle serie by autor");
    }
    catch(std::exception & e)
    {

    }
  }

  list = tools::split(author,',');
  if(list.size()==2)
  {
    // Case: [LastName,FirstName]
    for (auto str :list)
    {
      // Remove parenthese
      std::vector<std::string> list2 = tools::splitbySep( str, "()");
      if(list2.size()!=0 && tools::trim(tools::checkAlpha(list2[1])).size())
      {
        m_author+= tools::formatName(tools::trim(list2[0]+ " " +list2[2] )) + "," ;
      }
      else
      {
        m_author+= tools::formatName(tools::trim(str)) + "," ;
      }
    }
    m_author.pop_back();
    setLevelPoint(2,"autor directly");
   }
   else
   {
     list = tools::split(author,' ');
     if(list.size()==2)
     {
       for (auto it = list.rbegin(); it != list.rend(); ++it)
       {
         m_author+= tools::formatName(tools::trim(*it)) + "," ;
       }
       m_author.pop_back();
       setLevelPoint(1,"autor by space");
    }
    else
    {
      //PRD("auteur anormal:"+author +":"+getName());
      // Anormal case: only one word: supect to wrong name: no set
      m_author = author;
    }
  }
  m_author=tools::trim(m_author);
  m_author=tools::replaceForbiddenCharacter(m_author,' ');
}
std::string EBookFileInfo::getAuthor()
{
  return m_author;
}

void EBookFileInfo::addSubject(std::string lstr)
{
  bool isFirstLetter = true;
  bool isFirstOther  = false;
  //  std::cout << "Subject:["<< lstr <<"]" <<  std::endl;
  std::wstring str  = tools::convertToWs(lstr);
  std::string sCategorie;
  // std::wstring categorie;
  std::size_t index = 0;
  for(wchar_t charac: str)
  {
    unsigned int iC = tools::getNbreOfByteCode((unsigned char )lstr[index]);
    if(tools::isAlphaDigit(charac))
    {
      isFirstOther = true;
      if(isFirstLetter)
      {
        for(unsigned int i=0; i<iC ;i++)
        {
          sCategorie.push_back(std::toupper(lstr[index+i],std::locale(tools::local_filter)));
        }
        isFirstLetter = false;
      }
      else
      {
        for(unsigned int i=0; i<iC ;i++)
        {
          sCategorie.push_back(std::tolower(lstr[index+i],std::locale(tools::local_filter)));
        }
        //sCategorie.push_back(std::tolower(lstr[index],std::locale(tools::local_filter)));
      }
    }
    else
    {
      isFirstLetter = true;
      if(isFirstOther)
      {
        //sCategorie.push_back(lstr[index]);
        for(unsigned int i=0; i<iC ;i++)
        {
          sCategorie.push_back(lstr[index+i]);
        }
        isFirstOther = false;
      }
      else
      {
        //sCategorie.push_back(std::tolower(lstr[index],std::locale(tools::local_filter)));
      }
    }
    index+=iC;
  }

  APPLI_LOG(tools::TRACE_WARNING,"Subject:["+ sCategorie +"]");
  m_subjects.push_back(sCategorie);
}

std::string EBookFileInfo::getCategorie()
{
  readMetaInfo();
  return m_officalCategorie;
}

void EBookFileInfo::autoClassification()
{
  std::string officialCategory = getCategorie();
  if(officialCategory.size()!=0)
  {
    Directory * master =  (Directory*) getMaster();
    Directory * newDir = master->createDirectory(officialCategory);
    newDir->move(*this);
  }
}

void EBookFileInfo::setCategorie()
{
  std::string officialCategory;
  for(auto categ:m_subjects)
  {
    auto officialCategoryMap = m_mapSubjectCategory.find(categ);
    if (officialCategoryMap != m_mapSubjectCategory.end())
    {
      m_officalCategorie = officialCategoryMap->second;
      setLevelPoint(2,"setCategorie");
      break;
    }
  }
}

std::list<std::string> EBookFileInfo::getInfos()
{
  readMetaInfo();
  std::list<std::string> vec;
  vec.push_back("epub");
  vec.push_back(m_author);
  vec.push_back(m_title);
  return vec;
}

void EBookFileInfo::setLevelPoint(int point,const std::string& comment)
{
  m_formatingLevel+= point;
  APPLI_LOG(tools::TRACE_DEBUG_MEDIUM,"point:"  + std::to_string(point) +  comment +":" + std::to_string(m_formatingLevel));
}

}

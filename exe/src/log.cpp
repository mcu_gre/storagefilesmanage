/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */


#include <sys/stat.h>
#include <locale>

#include "log.h"


using namespace std;

namespace tools
{
/**
 *
 *
 * */
  Etrace g_softwareTracelevel=TRACE_NONE;
  void createDir(std::string& path)
  {
    struct stat st = {0};
    if (stat(path.c_str(), &st) == -1)
    {
      mkdir(path.c_str(), 0777);
    }
  }

/**
 *
 *
 * */
std::string BuildDebugHeader(Etrace level,const char * szFile,const char * szFct,const int line)
{
  std::time_t timeObj = std::time(nullptr);
  char mbstr[100];
  std::strftime(mbstr, sizeof(mbstr), "%D-%H-%M-%S\t", std::localtime(&timeObj));
  std::string strLevel;

  switch(level)
  {
    case TRACE_ERROR:
    strLevel+="Error";
    break;

    case TRACE_WARNING:
      strLevel+="Warning";
    break;

    case TRACE_INFO:
      strLevel+="Info";
    break;

    case TRACE_TIMING:
      strLevel+="Timing";
    break;

    case TRACE_DEBUG_LIGHT:
      strLevel+="Debug L";
    break;

    case TRACE_DEBUG_MEDIUM:
      strLevel+="Debug M";
    break;

    case TRACE_DEBUG_HIGH:
      strLevel+="Debug H";
    break;
  }

  std::string header(mbstr);
  header+=strLevel;
  header+="\t";

  // Extract file name from path
  char *dirc = strdup(szFile);
  char* name = basename(dirc);
  //printf("szFile:%s\n",name);
  header+=name;
  free(dirc);
  header+="\t";

  header+=szFct;

  header+=":" + to_string(line);

  size_t spaceSeparator =1;

  if(header.size() < 80)
  {
    spaceSeparator = 80 - header.size();
  }

  std::string space (spaceSeparator,' ');

  header+=space;

  return header;
}

void SetLevel(Etrace level)
{
  g_softwareTracelevel = level;
}

void LogWrite(std::string  msg, Etrace level)
 {
  if(level<=g_softwareTracelevel)
  {
    std::cout << msg << std::endl;
  }
}
/*
void LogWrite(std::ostringstream& msg,Etrace level)
{
  if(level<=g_softwareTracelevel)
  {
    std::cout << "flux:" << msg.str() << std::endl;
  }
}*/
}


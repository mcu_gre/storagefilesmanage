/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */



#include <stdio.h>
#include <string>
#include "directory.h"
#include "fileAlgorithme.h"
#include "naming.h"
#include "tools.h"

namespace fileAlgorithme
{

using namespace tools;


// aListOfFiles: a list of file with same name (requ: min size >1)
// Output: a list of file with same name and:
//    -- Well formated
//    -- with a 80 % same size
fileAlgorithme::FilesListExtended listTreatmentDuplicateName(const WFilesList& aListOfFiles)
{
  fileAlgorithme::FilesListExtended fileList;
  fileList.masterFile = aListOfFiles.front();

  // Traitement 1: on cherche la taille max des fichier (si bien formate) et on set le master file
  for(auto weakFile :aListOfFiles)
  {
    auto file = weakFile.lock();
    file->readMetaInfo();
    if(file->isWellFormated() &&file->getSize() > fileList.masterFile.lock()->getSize())
    {
      fileList.masterFile = weakFile;
    }
  }
  auto masterFile = fileList.masterFile.lock();
  if(masterFile->getSize()!=0)
  {
    // Traiement 2: on ajoute le fichier a une liste suivant le ratio avec la taille max et si bien formaté et si meme extension (TODO)
    for(auto weakFile :aListOfFiles)
    {
      auto file = weakFile.lock();
      unsigned ratio = (file->getSize()*100)/ masterFile->getSize();
      unsigned distance = tools::jacardSimilitude(file->getName(), masterFile->getName());

#define FILEDATA    "r[" << ratio << "]" \
                  <<":d[" << distance << "]"\
                  <<":s[" << file->getFormatingLevel() << "]"\
                  << ":id1[" << file->getUUId1()<< "]"\
                  << ":id2[" << file->getUUId2()<< "]"\
                  << ":s["<< file->getSize()<< "]"\
                  << ":Ms[" << masterFile->getSize()<< "]"\
                  <<":well[" << file->isWellFormated()<< "]"\
                  << ":["<< file->getFullPath() << "]"

      if(ratio>= 70 && distance>=70&& file->isWellFormated()&& file->getExtension().compare("epub")==0)
      {
        fileList.m_fileList.push_back(weakFile);
        std::cout << "Add:" << FILEDATA << std::endl;
      }
      else
      {
        std::cout << "Not Add:" << FILEDATA << std::endl;
      }
    }
  }
  return fileList;
}

fileAlgorithme::FilesListExtended listTreatmentDuplicateHash(const WFilesList& aListOfFiles)
{
  fileAlgorithme::FilesListExtended newList;
  newList.m_fileList = aListOfFiles;
  for(auto weakFileItem:aListOfFiles)
  {
    auto fileItem = weakFileItem.lock();
    fileItem->setDuplicate(true);
  }
  return newList;
}

std::string fileTreatementHashAlgo(File::FileInfo& file)
{
  file.buildHashFromAlgo();
  return file.getHash();
}

std::string fileTreatementHash(File::FileInfo& file)
{
  file.buildHash();
  return file.getHash();
}


void removeBetterFile(fileAlgorithme::FilesListExtended& filesListExtended)
{
  auto iterFileToKeep = filesListExtended.m_fileList.begin();

  for (auto iterFile = filesListExtended.m_fileList.begin(), __end = filesListExtended.m_fileList.end(); iterFile != __end; ++iterFile)
  {
    auto itemFile = (*iterFile).lock();
    auto fileToKeep = (*iterFileToKeep).lock();
    if( itemFile->getFormatingLevel() > fileToKeep->getFormatingLevel() )
    {
      iterFileToKeep = iterFile;
    }
  }

  // We can add a supp filter to select the file to delete
  filesListExtended.masterFile = *iterFileToKeep;
  filesListExtended.m_fileList.erase(iterFileToKeep);
}

}// End namespace

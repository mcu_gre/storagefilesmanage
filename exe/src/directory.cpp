/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>

#include "filecommondef.h"
#include "fileStatistic.h"
#include "directory.h"
#include "manageStorage.h"
#include "tools.h"
#include "stringtools.h"
#include "videoFileInfo.h"
#include "photoFileInfo.h"
#include "documentFileInfo.h"
#include "eBookFileInfo.h"
#include "musicFileInfo.h"
#include "systemtools.h"
#include "log.h"

namespace File
{
  unsigned long Directory::m_fileCount=0;
  unsigned long Directory::m_folderCount=0;
/**
 *
 *
 * */
/*
Directory::Directory(ManageStorage & manageStorage,FileInfo::ListOfFiles * listOfFile):
           FileSystemItem("",""),
           m_topDirectory(nullptr),
           m_parentDirectory(nullptr),
           m_manageStorage(manageStorage)
{
  APPLI_LOG(tools::TRACE_DEBUG_HIGH,"tell me what is");
  m_listFileInfo.insert(m_listFileInfo.end(),listOfFile->begin(),listOfFile->end());
}*/
/**
 *
 *
 * */
Directory::Directory(ManageStorage & manageStorage):
           FileSystemItem(this,FileSystemItem::m_magicName),
           m_manageStorage(manageStorage)
{
  APPLI_LOG(tools::TRACE_DEBUG_HIGH,"Constructor header Directory");
};


/**
 *
 *
 * */
Directory::Directory(ManageStorage & manageStorage,const std::string & directoryPath,const std::string & directoryName,FileSystemItem* parentDirectory):
           FileSystemItem(parentDirectory,directoryPath,directoryName),
           m_manageStorage(manageStorage)
{
  APPLI_LOG(tools::TRACE_DEBUG_MEDIUM,printInfoDirectorie("Constructor Directory"));
};

/**
 *
 *
 * */
Directory::Directory(ManageStorage & manageStorage,const std::string & directoryFullPath,FileSystemItem* parentDirectory):
           FileSystemItem(parentDirectory,directoryFullPath),
           m_manageStorage(manageStorage)
{
  APPLI_LOG(tools::TRACE_DEBUG_MEDIUM,printInfoDirectorie("Constructor Directory"));
};



/**
 *
 *
 * */
ManageStorage & Directory::getManageStorage()
{
  return m_manageStorage;
}

/**
 *
 *
 * */
Directory* Directory::AddDirectory(std::string& folderName)
{
  TIME_MEASURE;
  auto dir = std::make_shared<Directory>(getManageStorage(),getFullPath(),folderName,this);

  APPLI_LOG(tools::TRACE_DEBUG_LIGHT,dir->printInfoDirectorie(""));
  m_folderCount++;
  m_SubDirectories.push_front(dir);
  getManageStorage().addDir(dir);
  return dir.get();
}

std::shared_ptr<FileInfo> Directory::removeFile(FileSystemItem& fileSystemItem)
{
  std::shared_ptr<FileInfo>  smFile;
  for (auto fileItem = m_listFileInfo.begin(); fileItem != m_listFileInfo.end(); fileItem++)	
  {
	smFile = *fileItem;
	if( fileSystemItem.getFullPath().compare( smFile.get()->getFullPath())==0)
	{
		m_listFileInfo.erase(fileItem);
		break;
	}
  }
  return smFile;
}

void Directory::AddFile(FileSystemItem& fileSystemItem)
{
  TIME_MEASURE;
  
  File::FileInfo::ETYPE type = File::FileInfo::getType(fileSystemItem.getFullName());

  bool bBuildHash = false;
  //bool bRename = getManageStorage().isRequestedAction(Option::RENAME_WRONG_NAME);

  APPLI_LOG(tools::TRACE_INFO,std::string("path:")+ fileSystemItem.getPath()+":file:"+fileSystemItem.getFullName()+":size:"+std::to_string(fileSystemItem.getSize()));

  std::shared_ptr<FileInfo> aFile ;

  switch(type)
  {
    case File::FileInfo::EBOOK:
      aFile = std::make_shared<EBookFileInfo> (fileSystemItem,bBuildHash);
    break;

    case File::FileInfo::DOCUMENT:
      aFile = std::make_shared<DocumentFileInfo> (fileSystemItem,bBuildHash);
    break;

    case File::FileInfo::MUSIC:
      aFile = std::make_shared<MusicFileInfo> (fileSystemItem,bBuildHash);
    break;

    case File::FileInfo::VIDEO:
      aFile = std::make_shared<VideoFileInfo> (fileSystemItem,bBuildHash);
    break;

    case File::FileInfo::PHOTO:
      aFile = std::make_shared<PhotoFileInfo> (fileSystemItem,bBuildHash);
    break;

    default:
      aFile = std::make_shared<FileInfo> (fileSystemItem,bBuildHash);
  }
  APPLI_LOG(tools::TRACE_DEBUG_HIGH,std::string("path:")+ fileSystemItem.getPath()+":file:"+fileSystemItem.getFullName()+":size:"+std::to_string(fileSystemItem.getSize()));

  m_fileCount++;
  FileStatistic::add(aFile->getExtension());
  
  if( tools::hasForbiddenCharacter(aFile->getName()))
  {
    std::cout << "Fordidden char:"  << aFile->getFullPath() << std::endl;
  }
/*
  std::string newname = tools::replaceForbiddenCharacter(ptrFile->getName(),'-');
  if(newname.compare(ptrFile->getName())!=0)
  {
     ptrFile->updateName(newname);
  }
*/
  tools::printInfo(1000,m_fileCount,"Files analyzed:",m_folderCount,"Dir analyzed:");

  m_listFileInfo.push_front(aFile);
  getManageStorage().addFile(aFile);
}


/**
 *
 *
 * */
void Directory::scanDirectories()
{
  TIME_MEASURE;
  scanDirectory();
  scanSubDirectories();
}


/**
 *
 *
 * */
void Directory::scanSubDirectories()
{
  TIME_MEASURE;
  auto it = std::begin(m_SubDirectories);
  while(it!=std::end(m_SubDirectories))
  {
    (*it)->scanDirectories();
    it++;
  }
}

Directory* Directory::createDirectory(std::string newDirectoryName)
{
  std::string newDirectory = getFullPath() + File::separator+ newDirectoryName;
  Directory* newDir = nullptr;
  // First: search if we have it in the dir list TODO

  APPLI_LOG(tools::TRACE_DEBUG_MEDIUM,newDirectory);

  if(getManageStorage().isRequestedAction(Option::SIMULATE))
  {
     std::cout << "SIMU:";
  }
  else
  {
    tools::mkdir_impl(newDirectory);
    newDir = AddDirectory(newDirectoryName);
  }
  std::cout << "create folder:"<<  newDirectory << std::endl;

  return newDir;
}


void Directory::rename(std::string newDirName)
{
  std::string newDirFullName = getPath() + File::separator+ newDirName;

  APPLI_LOG(tools::TRACE_DEBUG_MEDIUM,getFullPath() + " to " + newDirFullName);

  if(getManageStorage().isRequestedAction(Option::SIMULATE))
  {
     std::cout << "SIMU:";
  }
  else
  {
    if(tools::rename_impl(getFullPath().c_str(),newDirFullName.c_str()))
    {
	   setName(newDirName);
	}
  }
}


void Directory::move(FileInfo & file)
{
  std::string newFile = getFullPath() + File::separator+ file.getFullName();

  APPLI_LOG(tools::TRACE_DEBUG_MEDIUM,file.getFullPath() + " to " + newFile);

  if(getManageStorage().isRequestedAction(Option::SIMULATE))
  {
     std::cout << "SIMU:";
  }
  else
  {
    tools::rename_impl(file.getFullPath().c_str(),newFile.c_str());
    Directory *  parentDir = (Directory*) file.getParent(); 
    file.setParent(this);
    std::shared_ptr<FileInfo> smFile = parentDir->removeFile(file);
    m_listFileInfo.push_front(smFile);
  }

  std::cout << "move:"+ file.getFullPath() << " to " << newFile << std::endl;
}
/**
 *
 *
 * */
void Directory::scanDirectory()
{
  TIME_MEASURE;
  //system(CLEAR); /* On efface l'écran. */
  APPLI_LOG(tools::TRACE_DEBUG_MEDIUM,"Folder scanned:"+getFullPath());
  std::string currentDir = getFullPath();
  FileSystemItem::EFILE_TYPE fileType = getType();
  if (fileType==EDIRECTORY)
  {
    DIR* rep = opendir(currentDir.c_str());

    if (rep != nullptr)
    {
      struct dirent* ent = nullptr;
      while ((ent = readdir(rep)) != nullptr) /* Lecture du dossier. */
      {
        FileSystemItem fileSystemItem(this,currentDir,ent->d_name);

        std::string name(ent->d_name);

        FileSystemItem::EFILE_TYPE fileType = fileSystemItem.getType();
        if(fileType==FileSystemItem::EDIRECTORY && (name.find(".@__thumb")==std::string::npos) )
        {
          AddDirectory(name);
        }
        else
        {
          if(fileType==FileSystemItem::EFILE&&fileSystemItem.getSize()>getManageStorage().getMinFileSize() )
          {
            AddFile(fileSystemItem);
          }
        }
      }
      closedir(rep); /* Fermeture du répertoire. */
    }
    else
    {
      APPLI_LOG(tools::TRACE_ERROR,"Folder "+currentDir+" can be open");
    }
  }
  else
  {
    APPLI_LOG(tools::TRACE_ERROR,"Folder "+currentDir+" is not a dir");
    //exit(-1);
  }
  return;
}


/**
 *
 *
 * */
void Directory::printDirectories()
{
  auto it = std::begin(m_SubDirectories);
  while(it!=std::end(m_SubDirectories))
  {
    APPLI_LOG(tools::TRACE_DEBUG_LIGHT,"Folder:" +(*it)->getFullPath() );
    it++;
  }
}


/**
 *
 *
 * */
std::string Directory::printInfoDirectorie(std::string msg)
{
  std::string debug=msg+":dir:["+getFullPath()+"]";

  if(m_master)
  {
    debug+=":top:["+ m_master->getFullPath()+"]";;
  }
  if(m_parent)
  {
    debug+=":parent:["+m_parent->getFullPath()+"]";;
  }

  return debug;
}

void Directory::updatePathDependancy()
{
  for(auto& file:m_listFileInfo)
  {
    file->setPath(getFullPath());
    file->updatePathDependancy();
  }
  for(auto dir:m_SubDirectories)
  {
    dir->setPath(getFullPath());
    dir->updatePathDependancy();
  }
}

std::string Directory::buildNewName()
{
  // Rename directory with audio video rules
  // To modify to remove call of dedicated format from FileSystemItem (FileSystemItem::buildAudioVideoNewName)
  return extractUsefullInfoFromFileName().m_name;
}

/**
 *
 *
 * */
void Directory::printFiles()
{
  auto it = std::begin(m_listFileInfo);
  while(it!=std::end(m_listFileInfo))
  {
    APPLI_LOG(tools::TRACE_DEBUG_LIGHT,"File:" +(*it)->getFullPath() );
    std::cout<< "File:" << (*it)->getFullPath() <<std::endl;
    it++;
  }
}

std::list<std::string> Directory::getInfos()
{
  auto vec = FileSystemItem::getInfos();
  vec.push_front("Dir");
  return vec;
}

}// End namespace

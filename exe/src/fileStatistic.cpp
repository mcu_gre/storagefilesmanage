/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#include <fstream>
#include <string>

#include "filecommondef.h"
#include "fileInfo.h"
#include "fileStatistic.h"
#include "log.h"
#include "parseException.h"
#include "stringtools.h"
#include "systemtools.h"
#include "tools.h"

using namespace std;

namespace File
{
FileStatistic::FileStatistic(std::string name,std::string strKey,std::string mother):m_name(name),m_strKey(strKey),m_count(0),m_pMother(nullptr)
{
  std::vector<FileStatistic*>& list = getList();
  for(const auto & item:list)
  {
	  if(mother.compare( item->getKey())==0)
	  {
		  setMother(item);
		  break;
	  }
  }
  list.push_back(this);
}
	
void FileStatistic::print(int depth)
{
  while(depth--)
    std::cout << "  ";
    
  std::cout << getName() <<":" << getCount() << std::endl;
}

void FileStatistic::printAll(FileStatistic* mother)
{
	static unsigned int depth=0;
	std::vector<FileStatistic*>& list = getList();
	for(auto& item:list)
	{	
		if( mother==item->getMother() )
		{
		  item->print(depth);
	      depth++;
	      item->printAll(item);
	      depth--;		  
	    }
	}
}

void FileStatistic::add(std::string strKey)
{
	for(auto& item:getList())
	{
		if(item->getKey().compare(strKey)==0)
		{
		   item->plus();
	       if(item->getMother()!=nullptr)
	       {
	          item->getMother()->plus();
		   }
	       break;
		}
	}
}

std::vector<File::FileStatistic*> m_list;

std::vector<FileStatistic*>& getList() 
{ 
	return m_list;
}

FileStatistic fileStatisticEbook("Ebook File","Ebook","");
FileStatistic fileStatisticDocument("Document File","Document","");
FileStatistic fileStatisticMusic("Music File","Music","");
FileStatistic fileStatisticPhoto("Photo File","Photo","");
FileStatistic fileStatisticVideo("Video File","Video","");
FileStatistic fileStatisticepub("Epub File","epub","Ebook");
FileStatistic fileStatisticmobi("Mobi File","mobi","Ebook");
FileStatistic fileStatisticPDF("PDF File","pdf","Document");
FileStatistic fileStatisticWord("World File","doc","Document");
FileStatistic fileStatisticmp3("Mp3 File","mp3","Music");
FileStatistic fileStatisticjpg("JPG File","jpg","Photo");
FileStatistic fileStatisticpng("PNG File","png","Photo");
FileStatistic fileStatisticjpeg("JPEG File","jpeg","Photo");
FileStatistic fileStatisticraw("RAW File","raw","Photo");
FileStatistic fileStatisticavi("AVI File","avi","Video");
FileStatistic fileStatisticmkvd("mkv File","mkv","Video");
FileStatistic fileStatisticmp4("mp4 File","mp4","Video");
}

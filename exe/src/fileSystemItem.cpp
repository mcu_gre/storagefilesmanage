/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#include <stdio.h>
#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include <libgen.h>
#include <sys/stat.h>

#include "fileSystemItem.h"
#include "filecommondef.h"
#include "log.h"
#include "stringtools.h"
#include "systemtools.h"


namespace File
{

  const std::string FileSystemItem::m_magicName ="8f8f9146fc8d74695e5a21bad83d7ea9750bd3a317bf0b311fa081a99ff08e20";

  FileSystemItem::FileSystemItem(FileSystemItem * parent,std::string path,std::string name):
  m_parent(parent!=nullptr?parent:this),
  m_master(parent!=nullptr ? parent->m_master:this),
  m_path(path),
  m_name(name)
  {
    whatFileIs();
  };

  FileSystemItem::FileSystemItem(FileSystemItem * parent,std::string path):
  m_parent(parent!=nullptr?parent:this),
  m_master(parent!=nullptr ? parent->m_master:this),
  m_path(path)
  {
    char *dirc = strdup(m_path.c_str());
    char *basec = strdup(m_path.c_str());
    m_name = basename(dirc);
    m_path =dirname(dirc);
    free(dirc);
    free(basec);
    APPLI_LOG(tools::TRACE_DEBUG_HIGH,"path:"+path+" m_name:"+m_name+ " m_path:"+m_path );
    whatFileIs();
  };

  FileSystemItem::~FileSystemItem()
  {
    //std::cout << "Destructor FileSystemItem"<< std::endl;
  }

  std::string FileSystemItem::getName()
  {
    return m_name;
  }

  void  FileSystemItem::setName(std::string name)
  {
    m_name=name;
  }

  void  FileSystemItem::setPath(std::string path)
  {
    m_path=path;
  }

  std::string FileSystemItem::getFullName()
  {
    return getName();
  }
  std::string FileSystemItem::getPath()
  {
    return m_path;
  }
  std::string FileSystemItem::getFullPath()
  {
    return getPath()+File::separator+getFullName();
  }

  bool FileSystemItem::updateName()
  {
    bool isUpdated = false;
    std::string newName = tools::trim(buildNewName());
    if(newName.size()!=0&&newName.compare(getName())!=0)
    {
      isUpdated = updateName(newName);
    }
    return isUpdated;
  }

/*****************************************************************/
  bool FileSystemItem::updateName(std::string &newName)
  {
    return updateName(newName,getPath());
  };

  bool FileSystemItem::updateName(std::string &newName,std::string newPath)
  {
    std::string oldFullPath = getFullPath();
    FileSystemItem OldfileSystemItem = *this; // Attention, nous perdant la classe fille et son extension
    bool isUpdated = false;
    setName(newName);
    setPath(newPath);

    APPLI_LOG(tools::TRACE_DEBUG_MEDIUM,"from:"+OldfileSystemItem.getFullPath() + ":to: "+getFullPath());

    isUpdated = tools::rename_impl(oldFullPath,getFullPath());

    if (!isUpdated)
    {
      std::cout << "File exist:\n" << getFullPath() << "\nfrom\n" << oldFullPath << std::endl;
      *this = OldfileSystemItem;
    }

    return isUpdated;
  }

/**
 *
 *
 * */
FileSystemItem::EFILE_TYPE FileSystemItem::whatFileIs()
{
  struct stat sb;
  EFILE_TYPE ewhatFileIs =EUNKHNOW;

  if(lstat(getFullPath().c_str(), &sb) != -1)
  {
    switch (sb.st_mode & S_IFMT)
    {
      case S_IFBLK:
        APPLI_LOG( tools::TRACE_DEBUG_HIGH, "block device: [" +getFullPath() + "]" );
      break;

      case S_IFCHR:
        APPLI_LOG( tools::TRACE_DEBUG_HIGH, "character device: [" +getFullPath() + "]" );
      break;

      case S_IFDIR:
      if( getName().compare(".")==0 || getName().compare("..")==0 )
      {
        ewhatFileIs =ELINKLOCALDIRECTORY;
        APPLI_LOG( tools::TRACE_DEBUG_MEDIUM, "local link directory: [" +getFullPath() + "]" );
      }
      else
      {
        ewhatFileIs =EDIRECTORY;
        APPLI_LOG( tools::TRACE_DEBUG_MEDIUM, "directory: [" +getFullPath() + "]" );
      }
      break;

      case S_IFIFO:
        APPLI_LOG( tools::TRACE_DEBUG_HIGH, "FIFO/pipe: [" +getFullPath() + "]" );
      break;

      case S_IFLNK:
        ewhatFileIs =ESIMLINK;
        APPLI_LOG( tools::TRACE_DEBUG_HIGH, "ESIMLINK: [" +getFullPath() + "]" );
      break;

      case S_IFREG:
        ewhatFileIs =EFILE;
        m_size = sb.st_size;
        APPLI_LOG( tools::TRACE_DEBUG_HIGH, "regular file: [" +getFullPath() + "]" );
      break;

      case S_IFSOCK:
        APPLI_LOG( tools::TRACE_DEBUG_HIGH, "socket: [" +getFullPath() + "]" );
      break;

      default:
        APPLI_LOG( tools::TRACE_DEBUG_LIGHT, "unknown: [" +getFullPath() + "]" );
      break;
    }
  }
  else
  {
    if(getFullName().compare(m_magicName)==0)
    {
      ewhatFileIs = HEADER;
      APPLI_LOG(tools::TRACE_ERROR,"stat find header: ["+getFullPath() + "]");
    }
    else
    {
      APPLI_LOG(tools::TRACE_ERROR,"stat error on file: ["+getFullPath() + "] errno:" +std::to_string(errno));
    }
    //throw FileException(error.c_str());
  }
  m_itemType = ewhatFileIs;
  return ewhatFileIs ;
}

std::list<std::string> FileSystemItem::getInfos()
{
  std::list<std::string> vec;
  vec.push_back(getPath());
  vec.push_back(getName());
  return vec;
}

std::vector<tools::Tag>& FileSystemItem::getTags()
{
  static std::vector<tools::Tag> tags;
  if( ! tags.size() )
  {
    tags.push_back(tools::Tag("MULTi","Multi",tools::Tag::AUDIO_LANGUAGE,true) );
    tags.push_back(tools::Tag("1080p","1080p",tools::Tag::VIDEO_RESOLUTION,true) );
    tags.push_back(tools::Tag("720p","720p",tools::Tag::VIDEO_RESOLUTION,true) );
    tags.push_back(tools::Tag("BluRay","",tools::Tag::PUB,false) );
    tags.push_back(tools::Tag("x264","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("FASTSUB","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("BDRip","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("TRUEFRENCH","fr",tools::Tag::AUDIO_LANGUAGE,true));
    tags.push_back(tools::Tag("SUBFRENCH","fr",tools::Tag::VIDEO_SUBTITLE,true));
    tags.push_back(tools::Tag("WEB","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("PROPER","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag(".FRENCH.","fr",tools::Tag::AUDIO_LANGUAGE,true));
    tags.push_back(tools::Tag(" FRENCH","fr",tools::Tag::AUDIO_LANGUAGE,true));
    tags.push_back(tools::Tag("WEBRip","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("HDRiP","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("MP3","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("DVDRip","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("www.torrent9.biz","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("www.Torrent9.EC","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("WEB-DL","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("DD5","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("H264-ACOOL","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("x264-Ulysse","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("x264-BRiNK","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("x264-VENUE","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("STV","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("FANSUB","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("VOSTFR","en",tools::Tag::AUDIO_LANGUAGE,true));
    tags.push_back(tools::Tag("VOSTFR","fr",tools::Tag::VIDEO_SUBTITLE,true));
    tags.push_back(tools::Tag("iNTERNAL","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("LiMiTED","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("EXTENDED","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("AMZN","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("DDP5","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("H264-ARK01","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("XviD-EXTREME","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("xvid","",tools::Tag::PUB,false));
    tags.push_back(tools::Tag("UNRATED","",tools::Tag::PUB,false));
  //  tags.push_back(tools::Tag("FINAL","FINAL",tools::Tag::OTHER_INFO,true));
    
    // My tags
    tags.push_back( tools::Tag(".HD.","HD",tools::Tag::VIDEO_RESOLUTION,true) );
    tags.push_back( tools::Tag(".FullHD.","FullHD",tools::Tag::VIDEO_RESOLUTION,true) );
    tags.push_back( tools::Tag(".HDReady.","HDReady",tools::Tag::VIDEO_RESOLUTION,true) );
    tags.push_back( tools::Tag(".DVD.","DVD",tools::Tag::VIDEO_RESOLUTION,true) );
    tags.push_back( tools::Tag(".SD.","SD",tools::Tag::VIDEO_RESOLUTION,true) );
    tags.push_back( tools::Tag("audio-","",tools::Tag::AUDIO_LANGUAGE,true) );
    tags.push_back( tools::Tag("sub-","",tools::Tag::VIDEO_SUBTITLE,true) );    
  }
  return tags;
}

tools::NameRefactored FileSystemItem::extractUsefullInfoFromFileName()
{
  tools::NameRefactored nameRefactored;

  // Exe: name: [torrent] My.Movie.2018.xvid.FRENCH.anotherTags.avi
  APPLI_LOG(tools::TRACE_DEBUG_LIGHT,"File:" +getName() );

  // Remove all [anything] from name
  std::string nameWithoutBracket = tools::removeBracketTag(getName());
 
  // Now name: My.Movie.2018.xvid.FRENCH.anotherTags.avi
  // Find the first recognized tag, but add an artificail dot point to find tag with a final dot.
  nameWithoutBracket+=".";
  std::size_t firstTag = tools::findFirstTag(nameWithoutBracket,FileSystemItem::getTags());
  // remove artificail dot point
  nameWithoutBracket.pop_back();
  // Starting point: name with all bracket deleted
  std::string nameWithoutTag = nameWithoutBracket;

  if(firstTag!=std::string::npos)
  {
    // Get real name (presumed) of the file (without tag)
    nameWithoutTag =  nameWithoutBracket.substr(0,firstTag);
    // Remove last character because it must be a separator ?
    if(nameWithoutTag.back()=='.')
    {
      nameWithoutTag.pop_back();
    }

    for(auto& tag : FileSystemItem::getTags() )
    {
      size_t  pos = tools::findWithoutCase(nameWithoutBracket,tag.m_name);

      if(pos!=std::string::npos)
      {
        APPLI_LOG(tools::TRACE_DEBUG_LIGHT,"tag:" + tag.m_name + ", flag:" + std::to_string(tag.m_enable) + ", type:" + std::to_string(tag.m_type));
        tools::Tag tagToAdd = tag;
        if(tag.m_enable&&tag.m_nameToSet.compare("")==0)
        {
          tagToAdd.m_nameToSet = tools::getEndPrefix(nameWithoutBracket,tag.m_name,pos,'.');
        }
        nameRefactored.m_tags.push_back(tagToAdd);
      }
    }
  }
  nameRefactored.m_name = nameWithoutTag;
  //std::cout << "nameRefactored:" <<nameRefactored.m_name << std::endl;
  return nameRefactored;
}
}


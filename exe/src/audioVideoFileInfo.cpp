/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */
#include "MediaInfo/MediaInfo.h"
#include "ZenLib/Ztring.h"
#include "audioVideoFileInfo.h"
#include "directory.h"
#include "stringtools.h"
#include "converter.h"
#include "naming.h"
#include "log.h"


using namespace MediaInfoLib;

namespace File
{
std::vector<tools::Tag>& AudioVideoFileInfo::getTags()
{
  static std::vector<tools::Tag> tags;
  if( ! tags.size() )
  {
    tags.push_back( tools::Tag(".HD.","HD",tools::Tag::VIDEO_RESOLUTION,true) );
    tags.push_back( tools::Tag(".FullHD.","FullHD",tools::Tag::VIDEO_RESOLUTION,true) );
    tags.push_back( tools::Tag(".HDReady.","HDReady",tools::Tag::VIDEO_RESOLUTION,true) );
    tags.push_back( tools::Tag(".DVD.","DVD",tools::Tag::VIDEO_RESOLUTION,true) );
    tags.push_back( tools::Tag(".SD.","SD",tools::Tag::VIDEO_RESOLUTION,true) );
    tags.push_back( tools::Tag("audio-","",tools::Tag::AUDIO_LANGUAGE,true) );
    tags.push_back( tools::Tag("sub-","",tools::Tag::AUDIO_LANGUAGE,true) );
  }
  return tags;
}

void AudioVideoFileInfo::readResolution(MediaInfoLib::MediaInfo& MI)
{
  ZenLib::Ztring zInfo  = MI.Get(Stream_Video, 0, __T("Width"), Info_Text, Info_Name).c_str();
  m_width = std::atoi(zInfo.To_UTF8().c_str());
  zInfo = MI.Get(Stream_Video, 0, __T("Height"), Info_Text, Info_Name).c_str();
  m_height = std::atoi(zInfo.To_UTF8().c_str());
//    std::cout << "dim:" << m_height <<"*" << m_width<< std::endl;
}

void AudioVideoFileInfo::readAudio(MediaInfoLib::MediaInfo& MI)
{
  ZenLib::Ztring zInfo = MI.Get(Stream_General, 0, __T("Audio_Language_List"), Info_Text, Info_Name).c_str();
  std::string audio =  zInfo.To_UTF8();
//    std::cout << "audio:" << audio<< std::endl;
  if(audio.size()!=0)
  {
    auto lg = tools::split(audio,'/');
    for(auto lgItem:lg)
    {
      m_audio+= tools::formatName(lgItem)+",";
    }
    m_audio.pop_back();
  }
//    std::cout << "m_audio:" << m_audio<< std::endl;
}

void AudioVideoFileInfo::readSubTitle(MediaInfoLib::MediaInfo& MI)
{
  std::string subtitle;
  ZenLib::Ztring zInfo;
  unsigned int indexSub = 0;
  do
  {
    zInfo = MI.Get(Stream_Text,indexSub , __T("Language"), Info_Text, Info_Name).c_str();
    m_subtitle+=zInfo.To_UTF8() +",";
  }
  while(subtitle.size()!=0);
  if(m_subtitle.size()!=0)
  {
    m_subtitle.pop_back();
  }
//    std::cout << "Subtitle:" << m_subtitle<< std::endl;
}

void AudioVideoFileInfo::readMetaInfo()
{
  if(!m_isMetaInfoRead)
  {
    m_height = 0;
    m_width = 0;
    m_audio = "";
    m_subtitle = "";
    //Information about MediaInfo
    MediaInfo MI;
    //An example of how to use the library
    ZenLib::Ztring To_Display = __T("\r\n\r\nOpen\r\n");
    std::wstring path = tools::convertToWs(getFullPath());
#if defined(UNICODE) || defined (_UNICODE)
//unicode
    MI.Open( path );
#else
//pasunicode
    MI.Open( getFullPath() );
#endif
    readResolution(MI);
    readAudio(MI);
    readSubTitle(MI);

#if 0
    To_Display = MI.Get(Stream_Video, 0, __T("Width"), Info_Text, Info_Name).c_str();
    std::cout << "Width:" << To_Display.To_UTF8()<< std::endl;

    To_Display = MI.Get(Stream_Video, 0, __T("Height"), Info_Text, Info_Name).c_str();
    std::cout << "Height:" << To_Display.To_UTF8()<< std::endl;

    MI.Option(__T("Complete"), __T("1"));
    To_Display = MI.Inform().c_str();
    std::cout << "Complete:" << To_Display.To_UTF8()<< std::endl;


    To_Display += __T("\r\n\r\nInform with Complete=false\r\n");
    MI.Option(__T("Complete"));
    To_Display += MI.Inform().c_str();

    To_Display += __T("\r\n\r\nInform with Complete=true\r\n");
    MI.Option(__T("Complete"), __T("1"));
    To_Display += MI.Inform().c_str();

    To_Display += __T("\r\n\r\nCustom Inform\r\n");
    MI.Option(__T("Inform"), __T("General;Example : FileSize=%FileSize%"));
    To_Display += MI.Inform().c_str();

    To_Display += __T("\r\n\r\nGet with Stream=General and Parameter=\"FileSize\"\r\n");
    To_Display += MI.Get(Stream_General, 0, __T("FileSize"), Info_Text, Info_Name).c_str();

    To_Display += __T("\r\n\r\nGetI with Stream=General and Parameter=46\r\n");
    To_Display += MI.Get(Stream_General, 0, 46, Info_Text).c_str();

    To_Display += __T("\r\n\r\nCount_Get with StreamKind=Stream_Audio\r\n");
    To_Display += ZenLib::Ztring::ToZtring(MI.Count_Get(Stream_Audio, -1)); //Warning : this is an integer

    To_Display += __T("\r\n\r\nGet with Stream=General and Parameter=\"AudioCount\"\r\n");
    To_Display += MI.Get(Stream_General, 0, __T("AudioCount"), Info_Text, Info_Name).c_str();

    To_Display += __T("\r\n\r\nGet with Stream=Audio and Parameter=\"StreamCount\"\r\n");
    To_Display += MI.Get(Stream_Audio, 0, __T("StreamCount"), Info_Text, Info_Name).c_str();
#endif
    To_Display += __T("\r\n\r\nClose\r\n");
    MI.Close();

    //std::cout<<To_Display.To_Local().c_str()<<std::endl;
    m_isMetaInfoRead = true;
  }
  return ;
}

std::string AudioVideoFileInfo::buildResolution(std::list<tools::Tag>& tags)
{
  std::string tagResolution;
  if(m_height!=0)
  {
    if(m_height>=1080)
      tagResolution = "FullHD";
    else
    if(m_height>=1000)
      tagResolution = "HD";
    else
    if(m_height>=720)
      tagResolution = "HDReady";
    else
    if(m_height>=480)
      tagResolution = "DVD";
    else
      tagResolution = "SD";
  }
  else
  {
    for (auto & tag:tags)
    {
      if(tag.m_enable && tag.m_type==tools::Tag::VIDEO_RESOLUTION)
          tagResolution = tag.m_nameToSet;
    }
  }	
  return tagResolution;
}

std::string AudioVideoFileInfo::buildAudioLanguage(std::list<tools::Tag>& tags)
{
  std::string audioTag;
  if(m_audio.size()!=0)
  {
    audioTag = m_audio;
    APPLI_LOG(tools::TRACE_DEBUG_LIGHT,"audioTag:" + audioTag );
  }
  else
  {
    for (auto &tag:tags)
    {
      if(tag.m_enable && tag.m_type==tools::Tag::AUDIO_LANGUAGE)
      {
        audioTag = tools::formatName(tag.m_nameToSet);
      }
    }
  }	
 return audioTag;	
}

std::string AudioVideoFileInfo::buildSubtitleLanguage(std::list<tools::Tag>& tags)
{
  std::string subTitleTag;
  
  if(m_subtitle.size()!=0)
  {
    subTitleTag = m_subtitle;
  }
  else
  {
    for (auto &tag:tags)
    {
      if(tag.m_enable && tag.m_type==tools::Tag::VIDEO_SUBTITLE)
      {
        subTitleTag = tools::formatName(tag.m_nameToSet);
      }
    }
  }
  
  return subTitleTag;	
}


bool AudioVideoFileInfo::readSeasonSerieInfo(const std::string& name)
{
  bool isSeasonInfo= false;
  int season,episode;
  if(sscanf(name.c_str(),"S%dE%d",&season,&episode)==2)
  {
	 m_season =season;
	 m_episode =episode;
	 isSeasonInfo= true;
  }
  else
  if(sscanf(name.c_str(),"EP%d",&episode)==1)
  {
	 m_season =-1;
	 m_episode =episode;
	 isSeasonInfo= true;	  
  }
  else
  if(sscanf(name.c_str(),"%dx%d",&season,&episode)==2)
  {
	 m_season =season;
	 m_episode =episode;
	 isSeasonInfo= true;  
  }  
  else
  if(sscanf(name.c_str(),"s%de%d",&season,&episode)==2)
  {
	 m_season =season;
	 m_episode =episode;
	 isSeasonInfo= true;  
  }    
  return isSeasonInfo;
}

bool AudioVideoFileInfo::readProductionYear(const std::string& name)
{
  bool isProductionYear=false;
  m_productionYear = -1;
  try
  {
	std::string::size_type sz;  
    m_productionYear = std::stoi(name,&sz);
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    int currentYear = tm.tm_year + 1900;
    if(sz!=name.size() && ( m_productionYear< 1900|| m_productionYear>currentYear))
    {
	  m_productionYear = -1;
	}
	else
	{
      isProductionYear = true;
	}
  }	
  catch(std::exception & ex)
  {
  }  
  return isProductionYear;
}

std::string AudioVideoFileInfo::buildFormatedName(std::string name)
{
	// Find date
  std::string newName;
  std::list<std::string> listItem = tools::split(name, '.');
   
  if(readProductionYear(listItem.back()))
    listItem.pop_back();
  
  // Recompose title  (without dot separtor)
  enum { ADD_DOT,ADD_SPACE,ADD_NOTHNING} addWhat = ADD_NOTHNING;
  for(auto &item:listItem)
  {
	 if(readSeasonSerieInfo(item))
	 {
	   if(addWhat!=ADD_NOTHNING)
	     newName+=".";
	     
	   if(m_season!=-1)
	   {
		 newName+="s";
		 if(m_season<10)
		   newName+="0";
		 newName+=std::to_string(m_season);
	   }
	   if(m_episode!=-1)
	   {
		 newName+="e";
		 if(m_episode<10)
		   newName+="0";
		 newName+=std::to_string(m_episode);
	   }	   
	   addWhat =  ADD_DOT;
     }
	 else
	 {
	   switch(addWhat)
	   {
	     case ADD_DOT:
	   	   newName+=".";
	     break;

	     case ADD_SPACE:
	   	   newName+=" ";
	     break;			 
	     case ADD_NOTHNING:
	     break;
	   }
	   newName+=item;
	   addWhat = ADD_SPACE;
     }
  }
  
  // If year, add it like: "title (year)"
  if(m_productionYear!=-1)
  {
	 newName += " (" + std::to_string(m_productionYear) + ")";
  }
  
  return newName;
}

void AudioVideoFileInfo::autoClassification()
{
  Directory *  parentDir = (Directory*) getParent();
	
  if(parentDir!=nullptr)
  {
	std::string parentFolderName = parentDir->getName();
    
    std::string realName = buidRealName().m_name;
    
	unsigned distance = tools::jacardSimilitude(realName,parentFolderName);
	APPLI_LOG(tools::TRACE_INFO,"updateFolder File: " +realName + " to " + parentFolderName  + " d: "+ std::to_string(distance));
	//printf("updateFolder [%s] [%s] %d\n",realName.c_str(),nameRefactored.m_name.c_str(),distance);
		
	if(distance<70)
	{
	  // Not same: create a folder and put file inside
	  Directory * newDirectory = parentDir->createDirectory(realName);
	  newDirectory->move(*this);
	}
	else
	if(distance!=0)
	{
	  // Right folder but not the exact same name: renomming folder
		parentDir->rename(realName);
	}
  }
  else
  {
	printf("parentDir==nullptr\n");
  }
}

tools::NameRefactored AudioVideoFileInfo::buidRealName()
{
  tools::NameRefactored nameRefactored = extractUsefullInfoFromFileName();
  
  nameRefactored.m_name = buildFormatedName(nameRefactored.m_name);
  m_realTitle = nameRefactored.m_name;
  return nameRefactored;
}

std::string AudioVideoFileInfo::buildNewName()
{
  readMetaInfo();

  tools::NameRefactored nameRefactored = buidRealName();

  // Search information about resolution quality
  std::string tagResolution = buildResolution(nameRefactored.m_tags);

  if(tagResolution.size()!=0)
  {
    nameRefactored.m_name+="."+tagResolution;
  }

  std::string audioTag = buildAudioLanguage(nameRefactored.m_tags);

  if(audioTag.size()!=0)
  {
    nameRefactored.m_name+=".audio-"+audioTag;
  }
  
  std::string subTitleTag = buildSubtitleLanguage(nameRefactored.m_tags);;
  
  if(subTitleTag.size()!=0)
  {
    nameRefactored.m_name+=".sub-"+subTitleTag;
  }

  return nameRefactored.m_name;

}
}

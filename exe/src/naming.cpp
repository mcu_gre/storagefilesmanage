/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#include <sys/types.h>
#include <sys/sysinfo.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>
#include <ctime>
#include <locale>
#include <naming.h>
#include "tools.h"
#include "fileInfo.h"
#include "log.h"
#include <numeric>
 #include <algorithm>
using namespace std;

namespace tools
{

int levenshtein_distance(const std::string &s1, const std::string &s2)
{
  // To change the type this function manipulates and returns, change
  // the return type and the types of the two variables below.
  int s1len = s1.size();
  int s2len = s2.size();

  auto column_start = (decltype(s1len))1;

  auto column = new decltype(s1len)[s1len + 1];
  std::iota(column + column_start - 1, column + s1len + 1, column_start - 1);

  for (auto x = column_start; x <= s2len; x++)
  {
    column[0] = x;
    auto last_diagonal = x - column_start;
    for (auto y = column_start; y <= s1len; y++)
    {
      auto old_diagonal = column[y];
      auto possibilities =
      {
        column[y] + 1,
        column[y - 1] + 1,
        last_diagonal + (s1[y - 1] == s2[x - 1]? 0 : 1)
      };
      column[y] = std::min(possibilities);
      last_diagonal = old_diagonal;
    }
  }
  auto result = column[s1len];
  delete[] column;
  return result;
}


std::string reduceStr(std::string  strToConvert)
{
  std::string strConverted;
  for (auto c: strToConvert)
  {
    if(isalpha(c))
    {
        strConverted += tolower(c);
    }
    else
    if(isdigit(c))
    {
        strConverted += c;
    }
  }
  return strConverted;
}

int jacard_distance(const std::string &s1, const std::string &s2)
{
  const char *input1=s1.c_str();
  const char *input2=s2.c_str();
  //char input2[]="salam";
  uint8_t Bitmap1[256] = {0};
  uint8_t Bitmap2[256] = {0};
  int i;
  for (i=0;input1[i];i++)
  {
    Bitmap1[input1[i]] = 1;
  }
  for (i=0;input2[i];i++)
  {
    Bitmap2[input2[i]] = 1;
  }
  int in = 0;
  int un = 0;

  for(i=0;i<256;i++)
  {
    in+=Bitmap1[i] && Bitmap2[i];
    un+=Bitmap1[i] || Bitmap2[i];
  }

// Rest of the logic

  //printf("un :: %d\n",un);
  //printf("in :: %d\n",in);

  float Jaccard = (float)in/un*100.0;
  //printf("jaccard :: %f\n", Jaccard);

  return Jaccard;
}



int jacardSimilitude( std::string c1,std::string c2 )
{
  //std::string c1,c2;
  std::size_t l1,l2;
  // Transformer: ne prendre que les caractere qui sont alpha et digit, puis mettre en miniscule
  {
    c1 = reduceStr(c1);
    l1 = c1.size();
    c2 = reduceStr(c2);
    l2 = c2.size();
  }

  std::size_t lmin12 = std::min(l1,l2);
  std::size_t lmax12 = std::max(l1,l2);
  // Prendre la taille mini des deux
  //c1 = c1.substr(0,lmin12);
  //c2 = c2.substr(0,lmin12);

  int jacard = jacard_distance(c1,c2);
  return jacard;
}

int testSimilitude( std::string c1,std::string c2 )
{
  //std::string c1,c2;
  std::size_t l1,l2;
  // Transformer: ne prendre que les caractere qui sont alpha et digit, puis mettre en miniscule
  {
    c1 = reduceStr(c1);
    l1 = c1.size();
    c2 = reduceStr(c2);
    l2 = c2.size();
  }

   std::size_t lmin12 = std::min(l1,l2);
   std::size_t lmax12 = std::max(l1,l2);
   // Prendre la taille mini des deux
   //c1 = c1.substr(0,lmin12);
   //c2 = c2.substr(0,lmin12);

   int res = levenshtein_distance(c1,c2);
   int Jacard = jacard_distance(c1,c2);
   std::cout << "distance de" << std::endl;
   std::cout <<  "["<< l1 << "]" << c1  << std::endl;
   std::cout <<  "["<< l2 << "]" << c2  << std::endl;

   float resNormPlus =( (float) (100*res)/(l1+l2))/100;
   float resNormMin =(  ((float) res)/((float)lmin12));
   float resNormMax =(  ((float) res)/((float)lmax12));
   std::cout << "Distance            :" << res  << std::endl;
   std::cout << "Jacard            :" << Jacard  << std::endl;
   std::cout << "Norme somme longueur:" << resNormPlus  << std::endl;
   std::cout << "Norme min longueur  :" << resNormMin  << std::endl;
   std::cout << "Norme max longueur  :" << resNormMax  << std::endl;
   return 0;
};

}


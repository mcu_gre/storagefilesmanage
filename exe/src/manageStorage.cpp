/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#include <stdio.h>
#include <algorithm>
#include <typeinfo>

#include "directory.h"
#include "fileAlgorithme.h"
#include "fileStatistic.h"
#include "log.h"
#include "manageStorage.h"
#include "stringtools.h"
#include "systemtools.h"
#include "tools.h"

namespace File
{

using namespace tools;


const ManageStorage::MapCmd ManageStorage::m_mapCmd =
{
  { "dup", Option (Option::DUPLICATE,                  "dup",  "Search duplicate file.") },
  { "rename",  Option (Option::RENAME_WRONG_NAME,          "rename",   "Rename file.") },
  { "un",  Option (Option::FIND_DIFFERENCE,            "un",   "Search unique files between master.") },
  { "delete",  Option (Option::DELETE_DUPLICATE_FILES,     "delete",   "Remove duplicate file following rules computed from pattern (dup must be set).") },
  { "deletemax", Option (Option::DELETE_DUPLICATE_MAX,       "deletemax",  "Remove a maximum of duplicate file following rules computed from pattern (dup must be set).") },
  { "class", Option (Option::FILE_CLASSIFICATION,      "class", "Classification of file from meta data.") },
  { "simu",Option (Option::SIMULATE,                   "simu", "Simulate file system action.") },
  { "info",Option (Option::INFO_LISTE,                 "info", "Print infos.") },
  { "deletebyname",Option (Option::DELETE_DUPLICATE_FILE_BY_NAME,                 "deletebyname", "Remove duplicate file with same name.") },
  { "deletebyuuid1",Option (Option::DELETE_DUPLICATE_FILE_BY_UUID1,                 "deletebyuuid1", "Remove duplicate file with same uuid1.") },
  { "deletebyuuid2",Option (Option::DELETE_DUPLICATE_FILE_BY_UUID2,                 "deletebyuuid2", "Remove duplicate file with same uuid2.") },
  { "count",Option (Option::COUNT_MEDIA_FILES,                 "count", "Count media files categorized by extension.") }
};

std::string ManageStorage::printAvailableCommands(std::string space)
{
  std::string cmdList;
  for(auto cmd:m_mapCmd)
  {
    cmdList += space + "<"+cmd.second.m_cmdName + "> "+ cmd.second.m_help+"\n";
  }
  return cmdList;
}

void ManageStorage::printCommands()
{
  std::cout << "Commande requested:" <<std::endl;
  for(auto command:m_mapCmd)
  {
    if ( isRequestedAction(command.second.m_optionId) )
    {
      std::cout << "\t- " <<  command.second.m_cmdName << ": "<< command.second.m_help <<std::endl;
    }
  }
}

void ManageStorage::addCommand(std::string cmd)
{
  auto command = m_mapCmd.find(cmd);

  if (command != m_mapCmd.end())
  {
    setAction( command->second.m_optionId);
    APPLI_LOG(tools::TRACE_INFO,"add command " + command->second.m_help);
  }
  else
  {
    std::cerr <<"Error: command ["+cmd+ "] not found" << std::endl;
    exit (1);
  }
}

ManageStorage::ManageStorage():m_action(0),m_reportFolder("/tmp"),m_minFileSize(0),m_fileDeleted(0)/*m_minFileSize(std::numeric_limits<std::size_t>::max())*/
{};

void ManageStorage::addFile(std::weak_ptr<FileInfo> aWfile)
{
  addFilesInList(aWfile);
  auto spt = aWfile.lock()  ;

  fileAlgorithme::addInMap(m_sizeFilesList, spt->getSize(), aWfile);
  fileAlgorithme::addInMap(m_uuid1FilesList, spt->getUUId1(), aWfile);
  fileAlgorithme::addInMap(m_uuid2FilesList, spt->getUUId2(), aWfile);
  fileAlgorithme::addInMap(m_nameFilesList,tools::toLowerCase(spt->getName()),aWfile);
}

void ManageStorage::addFilesInList(std::weak_ptr<File::FileInfo> &aWfile)
{
  m_fileList.push_back(aWfile);
}

void ManageStorage::addDir(std::shared_ptr<File::Directory>& dir)
{
  m_dirList.push_back(dir);
}

void ManageStorage::addMasterDirectories(std::string path)
{
  auto dir = std::make_shared<Directory> (*this,path,nullptr);
  m_masterDirList.push_back(dir);
}


void ManageStorage::setMinFileSize(std::size_t minFileSize)
{
  m_minFileSize = minFileSize;
}

std::size_t ManageStorage::getMinFileSize()
{
  return m_minFileSize;;
}

void ManageStorage::setReportFolder(const std::string folder)
{
  m_reportFolder = folder;
}

void  ManageStorage::run()
{
  if(isRequestedAction(Option::SIMULATE))
  {
    tools::setSimulated(true);
  }
  else
  {
    tools::setSimulated(false);
  }
  printf("getSimulated %d\n",getSimulated() );
  scanDirectories();
  findDuplicateFilesFromName();
  renameFiles();
  findDuplicateFilesFromHash();
  findUniquesFiles() ;
  fileClassification() ;
  filesInfo();
  findDuplicateFilesFromUUID();
  findDuplicateFilesFromUUID2();
  
  if(isRequestedAction(Option::COUNT_MEDIA_FILES))
  {
    std::cout << "Total Count of files" << std::endl;
    FileStatistic::printAll(nullptr);
  }
}

void ManageStorage::scanDirectories()
{
  for(auto dir:m_masterDirList)
  {
      dir->scanDirectories();
  }
  tools::printInfo(1,Directory::m_fileCount,"Files analyzed:",Directory::m_folderCount,"Dir analyzed:");
  std::cout << std::endl;
  return;
}

void ManageStorage::findDuplicateFilesFromName()
{
  if(isRequestedAction(Option::DELETE_DUPLICATE_FILE_BY_NAME))
  {
    auto listOfFilesList = fileAlgorithme::buildListOfDuplicate(m_nameFilesList, fileAlgorithme::listTreatmentDuplicateName);
    tools::printListOfFileList(listOfFilesList);
    findDuplicateFiles_deleteFiles(listOfFilesList);
  }
}

void ManageStorage::findDuplicateFilesFromUUID()
{
  if(isRequestedAction(Option::DELETE_DUPLICATE_FILE_BY_UUID1))
  {
    auto listOfFilesList = fileAlgorithme::buildListOfDuplicate(m_uuid1FilesList, fileAlgorithme::listTreatmentDuplicateName);
    tools::printListOfFileList(listOfFilesList);
    findDuplicateFiles_deleteFiles(listOfFilesList);
  }
}

void ManageStorage::findDuplicateFilesFromUUID2()
{
  if(isRequestedAction(Option::DELETE_DUPLICATE_FILE_BY_UUID2))
  {
    auto listOfFilesList = fileAlgorithme::buildListOfDuplicate(m_uuid2FilesList, fileAlgorithme::listTreatmentDuplicateName);
    tools::printListOfFileList(listOfFilesList);
    findDuplicateFiles_deleteFiles(listOfFilesList);
  }
}

/**********************************************************************************/

void ManageStorage::findDuplicateFilesFromHash()
{
  if(isRequestedAction(Option::DUPLICATE))
  {
    fileAlgorithme::SStringFileMap algoHashFilesList;
    DuplicateFiles duplicateFiles = fileAlgorithme::findDuplicateFiles( m_sizeFilesList, algoHashFilesList,  fileAlgorithme::fileTreatementHashAlgo) ;

    fileAlgorithme::SStringFileMap hashFilesList;
    duplicateFiles = fileAlgorithme::findDuplicateFiles( algoHashFilesList,hashFilesList, fileAlgorithme::fileTreatementHash) ;

    auto listOfFilesList = fileAlgorithme::buildListOfDuplicate(hashFilesList, fileAlgorithme::listTreatmentDuplicateHash);

    tools::writeReportDuplicate(m_reportFolder,listOfFilesList,duplicateFiles,false);

    m_hashFilesList = hashFilesList;
    if(isRequestedAction(Option::DELETE_DUPLICATE_FILES)||isRequestedAction(Option::DELETE_DUPLICATE_MAX))
    {
      findDuplicateFiles_deleteFiles(listOfFilesList);
    }
  }
}

void ManageStorage::addPatternKeepingFile(std::string path)
{
  APPLI_LOG(tools::TRACE_DEBUG_LIGHT,path);
  m_patternKeepingFileList.push_back(path);
}

void ManageStorage::addPatternDeletingFile(std::string path)
{
  APPLI_LOG(tools::TRACE_DEBUG_LIGHT,path);
  m_patternDeletingFileList.push_back(path);
}


bool ManageStorage::isAListWithKeepFile(fileAlgorithme::FilesList& list)
{
  for(auto file:list)
  {
    if(isAFileToKeep(*file))
    {
      return true;
    }
  }
  return false;
}

bool ManageStorage::isAFileToDelete(FileInfo& file)
{
  for(auto pattern:m_patternDeletingFileList)
  {
    if(file.getPath().find(pattern)!=std::string::npos)
    {
      return true;
    }
  }
  return false;
}

bool ManageStorage::isAFileToKeep(FileInfo& file)
{
  for(auto pattern:m_patternKeepingFileList)
  {
    if(file.getPath().find(pattern)!=std::string::npos)
    {
      return true;
    }
  }
  return false;
}

bool ManageStorage::isAFileToDelete(bool isAKeepOne,bool isANotKeepOne,bool keepOneFound,bool noKeepOneFoud)
{
  bool fileToDelete = false;
  if( !isAKeepOne) // Always false if the file is a keepOne
  {
    if(isANotKeepOne) // true if the file is a notKeepOne
    {
      fileToDelete = true;
    }
    else // not a notKeepOne, not a keepOne
    {
      if(keepOneFound)
      {
        fileToDelete = true;
      }
      else
      if(!noKeepOneFoud && isRequestedAction(Option::DELETE_DUPLICATE_MAX))
      {
        fileToDelete = true;
      }
    }
  }
  return fileToDelete;
}

void ManageStorage::deleteFile(fileAlgorithme::FilesListExtended& filesList)
{
  auto masterFile = filesList.masterFile.lock();
  if(masterFile)
  {
    std::cout << "Keep:"<<masterFile->getFullPath() << std::endl;
  }
  for(auto weakFile: filesList.m_fileList)
  {
    // Delete file only if it is a recognized type
    auto file = weakFile.lock();
    if ( typeid(*file) != typeid(FileInfo) )
      deleteFile(*file);
  }
}

void ManageStorage::deleteFile(FileInfo& file)
{
  if(isRequestedAction(Option::SIMULATE))
  {
    std::cout << "SIMU:";
  }
  else
  {
    file.deleteFile();
  }
  std::cout << "deleted:"<<file.getFullPath() << std::endl;
  m_fileDeleted++;
}

void ManageStorage::filesInfo()
{
  if(isRequestedAction(Option::INFO_LISTE))
  {
    for(auto &fileItem:m_fileList)
    {
      auto infoListes = fileItem.lock()->getInfos();
      if(infoListes.size()!=0) // Pourquoi faire ce test ?
      {
        for(auto inf: infoListes)
        {
          std::cout << inf << "|";
        }
        std::cout << std::endl;
      }
    }
    for(auto &item:m_dirList)
    {
      auto infoListes = item.lock()->getInfos();
      if(infoListes.size()!=0) // Pourquoi faire ce test ?
      {
        for(auto inf: infoListes)
        {
          std::cout << inf << "|";
        }
        std::cout << std::endl;
      }
    }
  }
}

void ManageStorage::fileClassification()
{
  if(isRequestedAction(Option::FILE_CLASSIFICATION))
  {
    auto localFileList = m_fileList;
    for(auto &fileItem:localFileList)
    {
      auto file = fileItem.lock();
      file->autoClassification();
    }
  }
}

void ManageStorage::findDuplicateFiles_deleteFiles(const fileAlgorithme::SListOfFileListExtended& aDuplicateFilesList)
{
  std::cout << std::endl <<"Starting deleting duplicated files..." << std::endl;

  for(auto duplicateList:aDuplicateFilesList)
  {
    fileAlgorithme::FilesListExtended filesToDelete;
    bool keepOneFound=false;// =  isAListWithKeepFile(*duplicateList);
    bool noKeepOneFound=false;// =  isAListWithKeepFile(*duplicateList);

    APPLI_LOG(tools::TRACE_DEBUG_LIGHT,"Looking file hash:" +  duplicateList->masterFile.lock()->getHash());

    // First scan to find if keep files and not keep files are found
    for(auto weakFile: duplicateList->m_fileList)
    {
      auto file = weakFile.lock();
      if(isAFileToKeep(*file))
      {
        keepOneFound = true;
      }
      if(isAFileToDelete(*file))
      {
         noKeepOneFound = true;
      }
    }
    // Second scan to set the list of files to delete
    for(auto weakFile: duplicateList->m_fileList)
    {
      auto file = weakFile.lock();
      APPLI_LOG(tools::TRACE_DEBUG_LIGHT,
                   file->getName()+ ";"
                   "is a keep one:" +  std::to_string(isAFileToKeep(*file))+ ";"
                   "is a not keep one:" +  std::to_string(isAFileToDelete(*file))
                  );
      if(isAFileToDelete(isAFileToKeep(*file),isAFileToDelete(*file), keepOneFound, noKeepOneFound))
      {
        APPLI_LOG(tools::TRACE_DEBUG_LIGHT,"Adding in delete list:" +  file->getFullName());
        filesToDelete.m_fileList.push_back(file);
      }
      else
      {
        APPLI_LOG(tools::TRACE_DEBUG_LIGHT,"NOT Adding in delete list:" +  file->getFullPath());
        filesToDelete.masterFile = file;
      }
    }
    // It must stay minumum one file
    std::size_t iRemaingFiles= duplicateList->m_fileList.size() - filesToDelete.m_fileList.size();
    // If no remaing file: we remove on file of the list to delete in order to be sure that it remains one file
    if(iRemaingFiles==0)
    {
      fileAlgorithme::removeBetterFile(filesToDelete);
    }

    deleteFile(filesToDelete);
  }

  std::cout << std::endl <<"deleting duplicated files:" << m_fileDeleted << std::endl;
}

void ManageStorage::renameFiles()
{
  if(isRequestedAction(Option::RENAME_WRONG_NAME))
  {
    std::cout << "Renaming files..." << std::endl;
    for(auto fileItem:m_fileList)
    {
      auto file = fileItem.lock();
      auto oldName = file->getName();
      bool isUpdated = false;
      isUpdated =  file->updateName();
      if(isUpdated)
      {
        if(isRequestedAction(Option::SIMULATE))
        {
         std::cout << "SIMU Rename File:"<< std::endl;
        }
        std::cout << file->getPath() << std::endl ;
        std::cout << "old:[" <<  oldName             << "]" << std::endl ;
        std::cout << "new:[" <<  file->getName() << "]"<< std::endl;
      }
    }

    std::cout << "Renaming directory..." << std::endl;
    for(auto dirItem:m_dirList)
    {
      auto curDir = dirItem.lock();
      auto oldItem = *curDir;
      bool isUpdated = false;
      isUpdated =  curDir->updateName();
      if(isUpdated)
      {
        if(isRequestedAction(Option::SIMULATE))
        {
          std::cout << "SIMU Rename Dir:";
        }
        else
        {
          curDir->updatePathDependancy();
        }
        std::cout << curDir->getFullPath() << "|update name|\t["<< oldItem.getName() << "]|\t["<< curDir->getName()  << "]"<< std::endl;
      }
    }  
  }
}

void ManageStorage::findUniquesFiles()
{
  if(isRequestedAction(Option::FIND_DIFFERENCE))
  {
    std::cout << std::endl <<"Start unique file analyzed" << std::endl;

    int countFile=0;
    // Init
    // Do a loop on the master dir list to create a file list (empty for the moment) for each master directory
    for(auto masterItem:m_masterDirList)
    {
      auto list = std::make_shared<fileAlgorithme::WFilesList> ();
      APPLI_LOG(tools::TRACE_DEBUG_LIGHT,"master:"+masterItem->getFullPath());
      m_listOfunique[masterItem->getFullPath()] = list;
    }

    fileAlgorithme::SStringFileMap hashLocalFilesList = m_hashFilesList;

    for(auto fileItem1:m_fileList)
    {
      countFile++;
      auto file = fileItem1.lock();
      if(file->isDuplicate())
      {
        // We find them in the hash map
        auto fileListItem = hashLocalFilesList.find(file->getHash());
        // If find it, we try to count theses files by master directory
        if(fileListItem!=hashLocalFilesList.end())
        {
          std::map<std::string,std::weak_ptr<File::FileInfo>> fileByPath;
          // For each hash entry (same files but duplicate), we populate the fileByPath map with one file of each master if file exists
          for(auto fileItem2: *(fileListItem->second))
          {
            auto file2 = fileItem2.lock();
            fileByPath[file2->getMaster()->getFullPath()]= fileItem2;
          }
          if(fileByPath.size()==1) // The files found only one master: unique
          {
            auto it = m_listOfunique.find(file->getMaster()->getFullPath());
            APPLI_LOG(tools::TRACE_DEBUG_LIGHT,"find unique (but duplicate on the same master) file:"+file->getMaster()->getFullPath());
            if (it != m_listOfunique.end())
            {
              ((it->second))->push_back(file);
            }
            // we must remove it from local hash map to avoid to add it a second time
            hashLocalFilesList.erase(file->getHash());
          }
          else
          {

          }
        }
      }
      else
      {
        auto it = m_listOfunique.find(file->getMaster()->getFullPath());
        APPLI_LOG(tools::TRACE_DEBUG_LIGHT,"find unique file:"+file->getMaster()->getFullPath());
        if (it != m_listOfunique.end())
        {
          ((it->second))->push_back(fileItem1);
        }
      }
      printInfo(limite100,countFile,"Files analyzed:");
    }
    printInfo(1,countFile,"Files analyzed:");
    std::cout << std::endl;
    tools::WriteReportUnique(m_reportFolder,m_listOfunique,false);
  }
}

void ManageStorage::setSizeOfFileToHash(std::size_t size)
{
  FileInfo::setMaxDataSize(size);
}

void ManageStorage::setAction(Option::EACTION action)
{
  m_action|=action;
}

bool ManageStorage::isRequestedAction(Option::EACTION action)
{
  return (m_action&action)!=0;
}

}// End namespace

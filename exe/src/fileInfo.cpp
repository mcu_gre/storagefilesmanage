/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

//#include <openssl/md5.h>
//#include <openssl/sha.h>
//#undef _GLIBCXX_USE_INT128
#include <md5.h>
//#include <sys/stat.h>

//#include <algorithm>
//#include <cctype>
#include <fstream>
//#include <iostream>
#include <string>

//#undef _GLIBCXX_USE_INT128

#include "fileInfo.h"
#include "filecommondef.h"
#include "log.h"

#include "fileStatistic.h"
#include "parseException.h"
#include "stringtools.h"
#include "systemtools.h"
#include "tools.h"


using namespace std;

namespace File
{
  FileInfo::TypeOfFile FileInfo::typeOfFile;
  const float FileInfo::m_pourcentOfFileToHash=0.5;
  std::size_t FileInfo::m_maxHashDataSrc=512;

  FileInfo::FileInfo(FileSystemItem& fileInfo,bool bBuildHash ):
  FileSystemItem(fileInfo),
  m_hashState(0),
  m_isADuplicate(false),
  m_theFile(nullptr),
  m_isMetaInfoRead(false),
  m_isWellFormated(false),
  m_formatingLevel(0),
  m_hashStatus(NO_HASH)
  {
    buildObject(bBuildHash);
  }

  FileInfo::FileInfo(Directory& parent,std::string& path,std::string& fileName,const std::size_t& iSize,bool bBuildHash):
  FileSystemItem(&parent,path,fileName),
  m_hashState(0),
  m_isADuplicate(false),
  m_theFile(nullptr),
  m_isMetaInfoRead(false),
  m_isWellFormated(false),
  m_formatingLevel(0),
  m_hashStatus(NO_HASH)
  {
    m_size = iSize;
    buildObject(bBuildHash);
  }

  void FileInfo::buildObject(bool bBuildHash)
  {
    splitName(m_name);

    try
    {
      if(bBuildHash)
      {
         buildHashFromAlgo();
      }
    }
    catch(FileException& e)
    {
      strcpy((char*)m_hash,(char*)"CannotOpenFile");
    }
  }

  void FileInfo::setMaxDataSize(std::size_t size)
  {
    if(size>=m_hashSize)
    {
      m_maxHashDataSrc = size;
    }
    else
    {
      std::cerr << "Err: Size must be grether that "<< m_hashSize << " size remained to " << m_maxHashDataSrc << std::endl;
    }
  }

/***********************************************************************
 *
 * ********************************************************************/
void FileInfo::openFile()
{
  if(m_theFile==nullptr)
  {
    m_theFile = new std::ifstream(getFullPath(),std::ios::binary);
  }
  else
  {
    throw FileException("FileInfo::openFile:" + getFullPath());
  }
}

/***********************************************************************
 *
 * ********************************************************************/
void FileInfo::closeFile()
{
  if(m_theFile)
  {
    m_theFile->close();
    delete m_theFile;
    m_theFile =nullptr;
  }
}

/***********************************************************************
 *
 * ********************************************************************/
void FileInfo::buildHash(std::size_t iMaxSize)
{
  if(m_hashStatus!=FULL_HASH)
  {
    tools::TimingMeasure timeMe(__FUNCTION__);
    mbedtls_md5_context md5Ctx;
    mbedtls_md5_init(&md5Ctx);
    memset(m_hash,0,m_hashSize+1);

    openFile();

    std::size_t iSize = 0 ;
    if(!iMaxSize)
    {
      iMaxSize=getSize();
    }

    if(m_theFile&&getSize()>0)
    {
      std::string ligne;

      while ( std::getline( *m_theFile, ligne ) )
      {
        iSize+= ligne.size();
        mbedtls_md5_update(&md5Ctx,(const unsigned char*)ligne.c_str(),ligne.size());
        if(iSize>iMaxSize)
          break;
      }
      mbedtls_md5_finish(&md5Ctx,m_hash);
      APPLI_LOG(tools::TRACE_DEBUG_MEDIUM,":hash of:" +getFullPath() + ":"+ tools::convertBinaryToReadeableFormat(m_hash,m_hashSize));
      m_hashStr = tools::convertBinaryToReadeableFormat(m_hash,m_hashSize);
    }
    closeFile();
    if(getSize())
    {
      m_hashState = ((iSize)*100)/getSize();
    }
    m_hashStatus=FULL_HASH;
  }
}

/***********************************************************************
 *
 * ********************************************************************/
void FileInfo::initHashAlgo2()
{
  // Compute the size to hash
  std::size_t dataSrcSizeToHash =  m_maxHashDataSrc;

  m_hashState=0;
  m_offset=0;

  dataSrcSizeToHash = std::min(dataSrcSizeToHash, static_cast<std::size_t> (getSize()*m_pourcentOfFileToHash));

  if(dataSrcSizeToHash < m_hashSize)
  {
    if(getSize()< m_hashSize)
    {
      dataSrcSizeToHash = getSize();
    }
    else
    {
      dataSrcSizeToHash = m_hashSize;
    }
  }
  // Compute the number of part and the size of the part
  if( (m_hashSize*m_nbrPart)> dataSrcSizeToHash)
  {
    if(dataSrcSizeToHash<m_hashSize)
    {
      m_nbrPart =1;
    }
    else
    {
      m_nbrPart = dataSrcSizeToHash/m_hashSize;
    }
  }

  m_blockSize = dataSrcSizeToHash/m_nbrPart;

  if(m_nbrPart>1)
  {
    m_offset = (getSize()-dataSrcSizeToHash)/(m_nbrPart-1);
  }

  if(getSize())
  {
    m_hashState = ((m_blockSize* m_nbrPart)*100)/getSize();
  }
}

/***********************************************************************
 *
 * ********************************************************************/
void FileInfo::initHashAlgo()
{
  if(getSize()<m_maxHashDataSrc)
  {
    m_blockSize = getSize()/m_nbrPart;
    m_offset=0;
    if(m_blockSize<m_minBlockSize)
    {
      if(m_minBlockSize>getSize())
      {
        m_blockSize=getSize();
        m_nbrPart=1;
      }
      else
      {
        m_blockSize=getSize();
        m_nbrPart=1;
      }
    }
  }
  else
  {
    m_blockSize = m_maxHashDataSrc/m_nbrPart;
    m_offset = (getSize()-m_maxHashDataSrc)/(m_nbrPart-1);
  }
  if(getSize())
  {
    m_hashState = ((m_blockSize* m_nbrPart)*100)/getSize();
  }
}

/***********************************************************************
 *
 * ********************************************************************/
void FileInfo::buildHashFromAlgo()
{
  if(m_hashStatus!=FULL_HASH&& m_hashStatus!=ALGO_HASH)
  {
    tools::TimingMeasure timeMe(__FUNCTION__);

    initHashAlgo2();

    mbedtls_md5_context md5Ctx;
    mbedtls_md5_init(&md5Ctx);
    memset(m_hash,0,m_hashSize+1);
    openFile();
    if(m_theFile&&getSize())
    {
      char * szBlockData = new char [m_blockSize];

      // cette boucle s'arrête dès qu'une erreur de lecture survient
      for(unsigned int i = 0;i<m_nbrPart;i++)
      {
        memset(szBlockData,0,m_blockSize);
        m_theFile->read (szBlockData,m_blockSize);
        mbedtls_md5_update(&md5Ctx,(const unsigned char*)szBlockData,m_blockSize);
        m_theFile->seekg(m_offset,ios_base::cur);
        if((m_theFile->rdstate()))
        {
          std::cout << "error in seekg" << std::endl;
        }
      }
      mbedtls_md5_finish(&md5Ctx,m_hash);
      delete[] szBlockData;

      APPLI_LOG(tools::TRACE_DEBUG_HIGH,":hash of:" +getFullPath() + ":"+ tools::convertBinaryToReadeableFormat(m_hash,m_hashSize));
    }
    closeFile();
    m_hashStr = tools::convertBinaryToReadeableFormat(m_hash,m_hashSize);
    m_hashStatus=ALGO_HASH;
  }
}

/***********************************************************************
 *
 * ********************************************************************/
void FileInfo::deleteFile()
{
  APPLI_LOG(tools::TRACE_DEBUG_LIGHT,"Deleting:" + getFullPath() );

  if(tools::remove_impl(getFullPath())!=true)
  {
    std::cout << "Error to delete:" << getFullPath() << "errno:" <<std::to_string(errno)<<std::endl;
  }
}

/***********************************************************************
 *
 * ********************************************************************/
long FileInfo::getMe()
{
  return (long) this;
}

/***********************************************************************
 *
 * ********************************************************************/
std::string FileInfo::getCategorie()
{
  return "";
}


  /***********************************************************************
 *
 * ********************************************************************/
bool FileInfo::isBinaryEqual(FileInfo& otherFileInfo)
{
  bool res = false;
  //std::cout << "Compare:[" << getFullPath() << "] with:["<< otherFileInfo.getFullPath()<< "]"<<std::endl;
  if(otherFileInfo.getSize()==getSize())
  {
    res = memcmp(m_hash,otherFileInfo.m_hash,(size_t)m_hashSize)==0;
  }
  return res;
};

  /***********************************************************************
 *
 * ********************************************************************/
std::string FileInfo::getFullName()
{
  std::string fullName = m_name;
  if(m_extension.size()!=0)
  {
    fullName+= "."+m_extension;
  }
  return fullName;
};

  /***********************************************************************
 *
 * ********************************************************************/
bool FileInfo::operator ==(FileInfo& otherFileInfo)
{
  bool ret= false;
  if(getMe()!= otherFileInfo.getMe())
  {
    ret = otherFileInfo.getFullPath()!=getFullPath() && this->isBinaryEqual(otherFileInfo);
  }
  return ret;
}

/***********************************************************************
*
* ********************************************************************/
std::string FileInfo::getHash()
{
  return m_hashStr;
}

const unsigned char* FileInfo::getBinaryHash()
{
  return m_hash;
}

bool FileInfo::getExtension(std::string fileName,std::string  ext)
{
  bool find = false;
  std::size_t found = fileName.find_last_of(".");
  if(found!= string::npos)
  {
    ext = fileName.substr(found+1);
    find = true;
  }
  return find;
}

void FileInfo::splitName(std::string& fileName)
{
  std::size_t positionOfLastPoint = fileName.find_last_of(".");
  if(positionOfLastPoint!= string::npos)
  {
    m_extension = fileName.substr(positionOfLastPoint+1);
    m_name = fileName.substr(0,positionOfLastPoint);
  }
  else
  {
    m_extension="";
    m_name = fileName;
  }
  return ;
}

File::FileInfo::ETYPE FileInfo::getType(std::string fileName)
{
  File::FileInfo::ETYPE type= File::FileInfo::UNKNOW;
  std::size_t positionOfLastPoint = fileName.find_last_of(".");
  if(positionOfLastPoint!= string::npos)
  {
    std::string ext = fileName.substr(positionOfLastPoint+1);
    //printf("fileName %s, ext:%s\n",fileName.c_str(),ext.c_str());

    auto it = typeOfFile.find(ext);

    if (it != typeOfFile.end())
    {
      type= (*it).second;
    }
  }
  else
  {
    type = File::FileInfo::NOEXT;
  }
  return type;
}

void FileInfo::initMapFileType()
{
  typeOfFile["epub"] = File::FileInfo::EBOOK;
  
  typeOfFile["mobi"] = File::FileInfo::EBOOK;
  
  typeOfFile["pdf"] = File::FileInfo::DOCUMENT;
  
  typeOfFile["doc"] = File::FileInfo::DOCUMENT;
  
  typeOfFile["mp3"] = File::FileInfo::MUSIC;
  
  typeOfFile["jpg"] = File::FileInfo::PHOTO;
  
  typeOfFile["png"] = File::FileInfo::PHOTO;
  
  typeOfFile["jpeg"] = File::FileInfo::PHOTO;
  
  typeOfFile["raw"] = File::FileInfo::PHOTO;
  
  typeOfFile["avi"] = File::FileInfo::VIDEO;
  
  typeOfFile["mkv"] = File::FileInfo::VIDEO;
  
  typeOfFile["mp4"] = File::FileInfo::VIDEO;
  
  printf("FileStatistic  %u\n",getList().size());
}


std::list<std::string> FileInfo::getInfos()
{
  readMetaInfo();
  std::list<std::string> vec;
  return vec;
}

void FileInfo::readMetaInfo()
{
  if(!m_isMetaInfoRead)
  {
    m_isWellFormated = true;
    m_uuid1 = tools::generateUUID();
    m_uuid2 = tools::generateUUID();
    m_isMetaInfoRead = true;
  }
}

std::string FileInfo::getUUId1()
{
  readMetaInfo();
  return m_uuid1;
}

std::string FileInfo::getUUId2()
{
  readMetaInfo();
  return m_uuid2;
}

unsigned int FileInfo::getFormatingLevel()
{
  return m_formatingLevel;
}



File::StaticInit staticInit;

}

/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#include <stdio.h>
#include <getopt.h>
#include <vector>

#include "log.h"
#include "directory.h"
#include "tools.h"
#include "manageStorage.h"
#include "parseException.h"
#include "unit_test_storage.h"
#include "fileStatistic.h"

using namespace std;

/**
 *
 *
 * */

/* Nom du programme.  */
const char* program_name;

/* Envoie les informations sur l'utilisation de la commande vers STREAM
   (typiquement stdout ou stderr) et quitte le programme avec EXIT_CODE.
   Ne retourne jamais.
*/

void print_usage (FILE* stream, int exit_code)
{
  fprintf (stream, "Utilisation : %s options [fichierentrée ...]\n",
                    program_name);
  std::string optionList=std::string("")+
      " -h --help                  Affiche ce message.\n"+
      " -v --verbose               Definit un niveau de verbose pour les messages de debug.\n"+
      " -d --directory directory   Ajoute un repertoire.\n"+
      " -s --hashSize size         definit la taille du hash.\n"+
      " -a --action                Action:\n"+
      File::ManageStorage::printAvailableCommands("                               - ")+
      " -p --patternKeep string    Pattern de chemin dans lequelles les fichiers NE sont PAS à detruire.\n"+
      " -n --patternDelete string  Pattern de chemin dans lequelles les fichiers SONT à detruire.\n"+
      " -r --reportPath path       Chemin ou ecrire les fichiers de report .\n"+
      " -m --minFileSize size      Definit une taille minimum de fichier à traiter.\n"+
      " -t --unitTest              Lance les tests unitaires.\n"      ;

  fprintf (stream,optionList.c_str());
  /*
           " -h --help                  Affiche ce message.\n"
           " -v --verbose               Definit un niveau de verbose pour les messages de debug.\n"
           " -d --directory directory   Ajoute un repertoire.\n"
           " -s --hashSize size         definit la taille du hash.\n"
           " -a --action                Action:\n"
           "                               - dup for duplicate.\n"
           "                               - rn for rename.\n"
           "                               - un for unique.\n"
           "                               - rm for remove (uniquement avec dup).\n"
           "                               - simu for simulatate deletion (uniquement avec dup).\n"
           " -p --patternKeep string    Pattern de chemin dans lequelles les fichiers NE sont PAS à detruire.\n"
           " -n --patternDelete string  Pattern de chemin dans lequelles les fichiers SONT à detruire.\n"
           " -r --reportPath path       Chemin ou ecrire les fichiers de report .\n"
           " -m --minFileSize size      Definit une taille minimum de fichier à traiter.\n"
           " -t --unitTest              Lance les tests unitaires.\n"
  ); */
  exit (exit_code);
}
//static File::StaticInit staticInit;

int main (int argc, char* argv[])
{
  int next_option;
  /* Chaîne listant les lettres valides pour les options courtes. */
  const char* const short_options = "tho:v:d:s:a:r:m:p:n:";

printf("main FileStatistic  %d\n",File::getList().size());
  /* Tableau décrivant les options longues valides. */
  const struct option long_options[] =
  {
    { "help",     0, nullptr, 'h' },
    { "verbose", 1, nullptr, 'v' },
    { "directory", 1, nullptr, 'd' },
    { "hashsize", 1, nullptr, 's' },
    { "action", 1, nullptr, 'a' },
    { "patternKeep", 1, nullptr, 'p' },
    { "patternDelete", 1, nullptr, 'n' },
    { "reportPath", 1, nullptr, 'r' },
    { "minFileSize", 1, nullptr, 'm' },
    { "unitTest", 0, nullptr, 't' },
    { nullptr,       0, nullptr, 0   }   /* Requis à la fin du tableau.  */
  };

  /* Nom du fichier vers lequel rediriger les sorties, ou NULL pour
    la sortie standard. */
  //const char* output_filename = NULL;
  /* Indique si l'on doit afficher les messages détaillés. */
  int verbose = 0;
  /* Mémorise le nom du programme, afin de l'intégrer aux messages.
    Le nom est contenu dans argv[0]. */
  program_name = argv[0];
  try
  {
    //tools::SetLevel(tools::TRACE_INFO);

    vector<string> listOfStringPathDir;

    int hashSize=0;
    tools::Etrace iLevel=tools::TRACE_INFO;
    std::string reportPath("/tmp");
    File::ManageStorage manageStorage;
    bool runUnitTest = false;
    do
    {
      next_option = getopt_long (argc, argv, short_options,
                                long_options, NULL);
      switch (next_option)
      {
        case 'h':   /* -h or --help */
          /* L'utilisateur a demandé l'aide-mémoire. L'affiche sur la sortie
            standard et quitte avec le code de sortie 0 (fin normale). */
          print_usage (stdout, 0);

        case 'd':   /* -d ou --directory */
          APPLI_LOG(tools::TRACE_INFO,"option d (directory):" +std::string( optarg));
          manageStorage.addMasterDirectories(optarg);
        break;

        case 'p':   /* -p ou --patternKeep */
          APPLI_LOG(tools::TRACE_INFO,"option p (string):" +std::string( optarg));
          manageStorage.addPatternKeepingFile(optarg);
        break;

        case 'n':   /* -n ou --patternDelete */
          APPLI_LOG(tools::TRACE_INFO,"option n (string):" +std::string( optarg));
          manageStorage.addPatternDeletingFile(optarg);
        break;

        case 's':
          APPLI_LOG(tools::TRACE_INFO,"option s (hash size):" + std::string( optarg));
          hashSize = std::stoi(optarg);
          manageStorage.setSizeOfFileToHash(hashSize);
        break;
        case 'f':
          APPLI_LOG(tools::TRACE_INFO,"option f (file size):" + std::string( optarg));
          manageStorage.setMinFileSize(std::stoi(optarg));
        break;
        case 'r':
           APPLI_LOG(tools::TRACE_INFO,"option r (report path):" + std::string( optarg));
           manageStorage.setReportFolder(optarg);
           reportPath = optarg;
        break;

        case 'a':
           APPLI_LOG(tools::TRACE_INFO,"option a (action):" + std::string( optarg));
           manageStorage.addCommand(optarg);
        break;

        case 'v':
           APPLI_LOG(tools::TRACE_INFO,"option v (level verbose):" + std::string( optarg));
           iLevel = (tools::Etrace) std::stoi(optarg);
           tools::SetLevel(iLevel);
        break;

        case 't':
          runUnitTest = true;
        break;
        case '?':   /* L'utilisateur a saisi une option invalide. */
          /* Affiche l'aide-mémoire sur le flux d'erreur et sort avec le code
            de sortie un (indiquant une fin anormale). */
          print_usage (stderr, 1);
        case -1:    /* Fin des options.  */
        break;
        default:    /* Quelque chose d'autre : inattendu.  */
          abort ();
      }
    }
    while (next_option != -1);
    /* Fin des options. OPTIND pointe vers le premier argument qui n'est pas une
        option. À des fins de démonstration, nous les affichons si l'option
        verbose est spécifiée. */
    if (verbose)
    {
      int i;
      for (i = optind; i < argc; ++i)
        printf ("Argument : %s\n", argv[i]);
    }

    manageStorage.printCommands();
    APPLI_TIMING(":Start Parse:");

    if(runUnitTest)
    {
#ifdef DEBUG
      tests::unit_tests();
#else
      std::cout << "No Debug release to run unit test" << std::endl;
#endif
      return 0;
    }
    manageStorage.run();
  }
  catch(File::FileException &fileException)
  {
    std::cout << fileException.what() <<std::endl;
  }

  tools::TimingMeasure::ShowMap();
  /* Le programme principal se place ici.  */
  return 0;
}


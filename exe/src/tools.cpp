/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */


#include <sys/time.h>
#include <unistd.h>
#include <ctime>
#include <locale>
#include <fstream>

#include "filecommondef.h"
#include "naming.h"
#include "log.h"
#include "tools.h"

using namespace std;

namespace tools
{

int limite100=100;
int limite1000=1000;

/**
 *
 *
 * */
void writeReportDuplicate(std::string & reportPath,fileAlgorithme::SListOfFileListExtended& list,File::DuplicateFiles& duplicateFiles,bool printScreen)
{
  // Build report path and folder
  std::string reportFolderPath(reportPath+File::separator+"report");
  tools::createDir(reportFolderPath);
  std::string reportFilePath(reportFolderPath+File::separator+"reportDuplicate.csv");
  std::cout << "Write report to:"<<reportFilePath  << std::endl;
  std::ofstream  m_theFile(reportFilePath,std::ios::out | std::ios::trunc);

  unsigned int iCout=1;
  unsigned int iNbreFile=0;

  if(printScreen)
  {
    std::cout << std::endl;
    std::cout << std::endl;
  }

  m_theFile << duplicateFiles.m_FilesCount << " files:" << duplicateFiles.m_FilesSize/1024 << " kB" <<std::endl;
  m_theFile << "Index;\tpath;\tsize\thash" <<std::endl;

  for(auto extentedList:list)
  {
     for(auto weakFile: extentedList->m_fileList)
     {
        auto file = weakFile.lock();
        File::FileInfo& fileInfo = *file;
        if(printScreen)
        {
          std::cout << fileInfo.getFullPath()<< std::endl;
        }
        m_theFile << std::to_string(iCout) << ";"<<fileInfo.getFullPath() << ";" << std::to_string(fileInfo.getSize()) << ";" << tools::convertBinaryToReadeableFormat(fileInfo.getBinaryHash(),fileInfo.getHashSize()) << std::endl;
        iNbreFile++;
     }
    iCout++;
  }

  if(printScreen)
  {
    std::cout <<"list:"<< list.size()<< std::endl;
    std::cout <<"Free files:"<< iNbreFile<< std::endl;
  }
  m_theFile << endl;
}

void WriteReportUnique(std::string & reportPath,fileAlgorithme::SStringFileMap& listOfunique,bool printScreen)
{
  // Build report path and folder
  std::string reportFolderPath(reportPath+File::separator+"report");
  tools::createDir(reportFolderPath);
  std::string reportFilePath(reportFolderPath+File::separator+"reportUnique.csv");
  std::cout << "Write report to:"<<reportFilePath  << std::endl;
  std::ofstream  m_theFile(reportFilePath,std::ios::out |std::ios::trunc);

  if(printScreen)
  {
    std::cout << std::endl;
    std::cout << std::endl;
  }
  m_theFile << "Unique Files" <<std::endl;
  for(auto fileItem:listOfunique)
  {
    auto & fileList = *(fileItem.second);
    auto  masterDir = (fileItem.first);

    if(printScreen)
    {
      std::cout<<"unique file of Master:" << masterDir << std::endl;
    }
    m_theFile <<"unique file of Master:" << masterDir << std::endl;

    for(auto weakFile:fileList)
    {
      auto file = weakFile.lock();
      if(printScreen)
      {
        std::cout << file->getFullPath()<< std::endl;
      }
      m_theFile << file->getFullPath() << std::endl;
    }
  }
  cout << "end"<< endl;
}


/**
 *
 *
 * */
unsigned long int TranslateTime(struct timeval timeval)
{
  return static_cast<unsigned long int> ( timeval.tv_usec +  timeval.tv_sec*1000*1000);
}

unsigned long int GetTime()
{
  static unsigned long int timeStart=0;
  if(timeStart==0)
  {
    timeval timeValStart;
    gettimeofday(&timeValStart, nullptr);
    timeStart =  TranslateTime(timeValStart);
  }

  struct timeval timeValNow;
  gettimeofday(&timeValNow, nullptr);

  unsigned long int timeNow = TranslateTime(timeValNow);

  return (  timeNow - timeStart);
}

/**
 *
 *
 * */
std::string convertBinaryToReadeableFormat(const unsigned char* szBinary,int iSize)
{
  std::string res;
  for(int i=0;i<iSize;i++)
  {
    res+= to_string(szBinary[i]);
  }

  return res;
}


/**
 *
 *
 * */
TimingMeasure::MapOfStatistics TimingMeasure::m_statObjets;

TimingMeasure::TimingMeasure(const char * szFunctionName):m_tempTime(GetTime())
{
  //std::cout << "mymap contains:\n"<<szFunctionName<<std::endl;
  m_myStatisticObjet = GetStatObj(szFunctionName);
  m_myStatisticObjet->m_instance++;
  m_myStatisticObjet->m_callNumber++;
}

/**
 *
 *
 * */
Statistic* TimingMeasure::GetStatObj(const char * szFunctionName)
{
  Statistic* ptrObj= nullptr;
  IteratorMapOfStatistics it = m_statObjets.find(szFunctionName);

  if (it != m_statObjets.end())
  {
    ptrObj = (*it).second;
  }
  else
  {
     ptrObj = new Statistic(szFunctionName);
     //m_statObjets.insert ( std::pair<const char*,Statistic*>(szFunctionName,ptrObj) );

     m_statObjets[szFunctionName] = ptrObj;
  }
  return ptrObj;
}

/**
 *
 *
 * */
TimingMeasure::~TimingMeasure()
{
  // Manage recursivity with m_instance
  m_myStatisticObjet->m_instance--;

  if(m_myStatisticObjet->m_instance==0)
  {
    m_myStatisticObjet->m_totaleTime+=(GetTime()- m_tempTime);
  }
}

/**
 *
 *
 * */
void TimingMeasure::ShowMap()
{
  std::cout << "mymap contains:\n";
  IteratorMapOfStatistics it;
  printf("%-20s\t%15s\t%15s\t%15s\n","Function","Total Call","Time (s)","Time per Call(us)");

  for (it=TimingMeasure::m_statObjets.begin(); it!=TimingMeasure::m_statObjets.end(); ++it)
  {
    printf("%-20s\t%15s\t%15s\t%15s\n",
    (it->second)->m_functionName,
    std::to_string((it->second)->m_callNumber).c_str(),
    std::to_string( (unsigned long int) ((it->second)->m_totaleTime/1000/1000) ).c_str(),
    std::to_string((it->second)->m_totaleTime/(it->second)->m_callNumber).c_str()
    );

    //std::cout << std::string((it->first)) << "\t\t => \t" << (it->second)->m_functionName << "\t:time:\t"<< std::to_string((it->second)->m_totaleTime) <<"\t:call:\t"<< std::to_string((it->second)->m_callNumber) << '\n';
  }
}

string formatPrintMsg(std::string msg)
{
  static std::size_t maxLn = 20;
  std::size_t ln =  msg.size();
  std::string spmsg(maxLn-ln,' ');
  return msg+spmsg;
}

void printInfo(int quota, int value,std::string msg,int value2,std::string msg2 )
{
  if((value%quota)==0)
  {
    std::cout << '\r' << formatPrintMsg(msg) << value << " " << formatPrintMsg(msg2) << value2;
    std::cout.flush();
  }
}

void printInfo(int quota, int value,std::string msg )
{
  if((value%quota)==0)
  {
    std::cout << '\r' << formatPrintMsg(msg) << value;
    std::cout.flush();
  }
}

void printListOfFileList(const fileAlgorithme::SListOfFileListExtended& lists)
{
  for(auto list:lists)
  {
    auto masterFile = list->masterFile.lock();
    std::cout <<"List of:" << masterFile->getFullPath() <<":"<< masterFile->getSize() << std::endl;
    for(auto weakFile: (list->m_fileList))
    {
      auto file = weakFile.lock();
      std::cout << "File:" << jacardSimilitude(masterFile->getName(),file->getName()) <<":"<< file->getSize()<<":" << file->getFullPath()<< std::endl;;
    }
    std::cout << std::endl;
  }
}


}


/*
 *  Tools Unit Test
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#ifndef __UNIT_TEST_STORAGE_
#define __UNIT_TEST_STORAGE_

#include <cppunit/extensions/HelperMacros.h>
#include "manageStorage.h"


/*! \namespace tools
 *
 * namespace aboot string and system tools
 */
namespace tests
{
  class UnitTestStorage: public CPPUNIT_NS::TestFixture
  {
  CPPUNIT_TEST_SUITE( UnitTestStorage );
//  CPPUNIT_TEST( tu_flnaTitle ) ;
  CPPUNIT_TEST( tu_movie ) ;
  CPPUNIT_TEST_SUITE_END();

   private:
 //   ParLed56 *parLed56; // un pointeur sur une instance de la classe à tester
    std::string m_stringTest;
    std::string m_stringResultTest;
    std::string m_stringWaitingtTest;
    std::list<std::string> m_listResultTest;
    std::list<std::string> m_listWaitingtTest;
    std::vector<std::string> m_vectorResultTest;
    std::vector<std::string> m_vectorWaitingtTest;
    wchar_t m_charac;
    unsigned char m_unicodeChar[4];

    File::ManageStorage m_manageStorage;
   public:
    UnitTestStorage();
    virtual ~UnitTestStorage();
  // Call before tests
  void setUp();
  // Call after tests
  void tearDown();
   // tests list
  void tu_flnaTitle();
  void tu_movie();

};
  void unit_tests();
  bool unit_tests_storage();
}
#endif

/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */


#include <iostream>
#include <regex>
#include "fileInfo.h"
#include "directory.h"
#include "manageStorage.h"
#include "stringtools.h"
#include "MediaInfo/MediaInfo.h"
#include "ZenLib/Ztring.h"
#include "naming.h"
#include "unit_test_tools.h"
#include "converter.h"
#include "unit_test_storage.h"
#include "filecommondef.h"
#include "fileSystemItem.h"
#include "eBookFileInfo.h"
#include "videoFileInfo.h"

#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

using namespace std;

namespace tests
{
void printInfoFSItemFile(File::FileSystemItem& aFile)
{
  cout <<"getName:" << aFile.getName()<< endl;
  cout <<"getFullName:" << aFile.getFullName()<< endl;
  cout <<"getPath:" << aFile.getPath()<< endl;
  cout <<"getFullPath:" << aFile.getFullPath()<< endl;
}
void printInfoFile(File::FileInfo& aFile)
{
  printInfoFSItemFile(aFile);
  cout <<"getExtension:" << aFile.getExtension()<< endl<< endl;
}

void test_dir_name()
{
  File::ManageStorage manageStorage;
  std::string path("/home/fold1");
  std::string path2("/usr/include/linux");
  File::Directory aDir(manageStorage,path,nullptr,nullptr);
  File::Directory aDir2(manageStorage,path2,nullptr,nullptr);
  printInfoFSItemFile(aDir);
  cout << endl;
  printInfoFSItemFile(aDir2);
  cout << endl;
}
void test_file_name()
{
  std::size_t size=0;
  std::string path("/home/fold1");
  std::string file("file.txt");
  std::string file2("filePasExt");
  File::ManageStorage manageStorage;
  File::Directory aDir(manageStorage,path,nullptr,nullptr);

  File::FileInfo aFile(aDir,path,file,size,false);
  File::FileInfo aFile2(aDir,path,file2,size,false);
  printInfoFile(aFile);
  printInfoFile(aFile2);
}

void initHashAlgo2(const std::size_t fileSize)
{
  // Compute the size to hash
  const std::size_t m_hashSize=16;
  const std::size_t m_maxHashDataSrc=1024;//1024*1024;
  const std::size_t m_minBlockSize=100;
  const float m_pourcentOfFileToHash=0.5;

  std::size_t m_hashState=0;
  unsigned int m_nbrPart=10;
  std::size_t m_blockSize;
  std::size_t m_offset=0;

  if(!fileSize)
  {
    m_nbrPart=0;
    m_blockSize=0;
    m_offset=0;
    return;
  }

  std::size_t dataSrcSizeToHash =  m_maxHashDataSrc;

  dataSrcSizeToHash = std::min(dataSrcSizeToHash, static_cast<std::size_t> (fileSize*m_pourcentOfFileToHash));

  if(dataSrcSizeToHash < m_hashSize)
  {
    if(fileSize< m_hashSize)
    {
      dataSrcSizeToHash = fileSize;
    }
    else
    {
      dataSrcSizeToHash = m_hashSize;
    }
  }

  // Compute the number of part and the size of the part
  if( (m_hashSize*m_nbrPart)> dataSrcSizeToHash)
  {
    if(dataSrcSizeToHash<m_hashSize)
    {
      m_nbrPart =1;
    }
    else
    {
      m_nbrPart = dataSrcSizeToHash/m_hashSize;
    }
  }

  m_blockSize = dataSrcSizeToHash/m_nbrPart;

  if(m_nbrPart>1)
  {
    m_offset = (fileSize-dataSrcSizeToHash)/(m_nbrPart-1);
  }

  if(fileSize)
  {
    m_hashState = ((m_blockSize* m_nbrPart)*100)/fileSize;
  }

  std::cout << "size:" << fileSize << ":dataSrcSizeToHash:" << dataSrcSizeToHash << ":m_nbrPart:"<< m_nbrPart << ":m_blockSize:"<< m_blockSize<<":m_hashState:"<<m_hashState <<":m_offset:"<<m_offset<< std::endl;
}

void test_algo()
{
  for(int i =0;i<5000;i++)
  {
    initHashAlgo2(i);
  }
}

void test_stringutile()
{
  std::cout << "**** is AlphaDigit"<< std::endl;
  wchar_t tt=L'ó';
  char ttt=L'ó';
  char tttt='.';
  std::wcout <<"[" <<tt << "] alpha digit:" <<std::boolalpha << tools::isAlphaDigit(tt) << std::endl;
  std::string accentstr = "François 1er, tête couronnée, n'est pas une chèvre! ó ¿";
  accentstr.push_back(tttt);
  std::wstring waccentstr=tools::convertToWs(accentstr);

  std::cout << accentstr << std::endl;
  std::size_t index = 0;
  for( wchar_t item:waccentstr)
  {
    unsigned int iC = tools::getNbreOfByteCode(accentstr.at(index));
    // std::cout << "Nbre of byte:" << iC << std::endl;
    std::string letter;
    for(int i=0; i<iC ;i++)
    {
      letter.push_back(accentstr.at(index+i));
    }
    std::cout <<"[" <<letter << "] alpha digit:" <<std::boolalpha << tools::isAlphaDigit(item) << std::endl;
    index+=iC;
  }

  std::cout << tools::getNbreOfByteCode(std::toupper(L'è',std::locale(tools::local_filter))) << std::endl;
  wchar_t Acc=L'É';
  std::wstring aa;
  aa.push_back(Acc);
  std::cout << tools::narrow(aa) << std::endl;
}

using namespace MediaInfoLib;

void testMediaInfo()
{
  //Information about MediaInfo
  MediaInfo MI;
  ZenLib::Ztring To_Display=MI.Option(__T("Info_Version"), __T("0.7.0.0;MediaInfoDLL_Example_MSVC;0.7.0.0")).c_str();
#if 0

  To_Display += __T("\r\n\r\nInfo_Parameters\r\n");
  To_Display += MI.Option(__T("Info_Parameters")).c_str();
  To_Display += __T("\r\n\r\nInfo_Capacities\r\n");
  To_Display += MI.Option(__T("Info_Capacities")).c_str();

  To_Display += __T("\r\n\r\nInfo_Codecs\r\n");
  To_Display += MI.Option(__T("Info_Codecs")).c_str();
#endif
  //An example of how to use the library
  To_Display += __T("\r\n\r\nOpen\r\n");
  ZenLib::Ztring To_Display2 = tools::convertToWs("vidtést.mp4");
  std::cout<<"To_UTF8:" << To_Display2.To_Local().c_str()<<std::endl;

  //
  MI.Open(To_Display2);
  //MI.Open(__T("/home/mcu/Téléchargements/Noeud papillon Lesnoeuds.com.mkv"));
  To_Display += __T("\r\n\r\nInform with Complete=false\r\n");
  MI.Option(__T("Complete"));
  To_Display += MI.Inform().c_str();

  To_Display += __T("\r\n\r\nInform with Complete=true\r\n");
  MI.Option(__T("Complete"), __T("1"));
  To_Display += MI.Inform().c_str();

  To_Display += __T("\r\n\r\nCustom Inform\r\n");
  MI.Option(__T("Inform"), __T("General;Example : FileSize=%FileSize%"));
  To_Display += MI.Inform().c_str();

  To_Display += __T("\r\n\r\nGet with Stream=General and Parameter=\"FileSize\"\r\n");
  To_Display += MI.Get(Stream_General, 0, __T("FileSize"), Info_Text, Info_Name).c_str();

  To_Display += __T("\r\n\r\nGetI with Stream=General and Parameter=46\r\n");
  To_Display += MI.Get(Stream_General, 0, 46, Info_Text).c_str();

  To_Display += __T("\r\n\r\nCount_Get with StreamKind=Stream_Audio\r\n");
  To_Display += ZenLib::Ztring::ToZtring(MI.Count_Get(Stream_Audio, -1)); //Warning : this is an integer

  To_Display += __T("\r\n\r\nGet with Stream=General and Parameter=\"AudioCount\"\r\n");
  To_Display += MI.Get(Stream_General, 0, __T("AudioCount"), Info_Text, Info_Name).c_str();

  To_Display += __T("\r\n\r\nGet with Stream=Audio and Parameter=\"StreamCount\"\r\n");
  To_Display += MI.Get(Stream_Audio, 0, __T("StreamCount"), Info_Text, Info_Name).c_str();

  To_Display += __T("\r\n\r\nClose\r\n");
  MI.Close();

  std::cout<<To_Display.To_Local().c_str()<<std::endl;

  return ;
}

void testRegex()
{
  std::string subject("Name: John Doe");
  std::string result;
  try
  {
    std::regex re("Name: (.*)");
    std::smatch match;
    if (std::regex_search(subject, match, re) && match.size() > 1)
    {
      result = match.str(1);
    }
    else
    {
      result = std::string("");
    }
  }
  catch (std::regex_error& e)
  {
    // Syntax error in the regular expression
  }
}

void test_split()
{

}

void test_distance()
{
  tools::testSimilitude( "Iain M. Banks-L'Essence De L'Art-[La Culture-8].epub","Banks,Iain M.-L'Essence de l'art-[La Culture-8].epub" );
  tools::testSimilitude( "dsfqsfvxfgdbwfra","tryfhcvwdqsgdhyt" );
}

void printWChar(wchar_t wval)
{
  unsigned int nbreOfByte = sizeof(wval);
  for(int i = 0;i< nbreOfByte;i++)
  {
    unsigned int val = wval&0xFF;
    std::cout <<"val "<< i << ":"<< val << std::endl;
    wval = wval >>8;
  }
}

void trashMultiByte()
{
  wchar_t tt=L'€';
  std::wstring wstest;
  wstest+= tt;

  std::setlocale(LC_CTYPE, "en_US.UTF8");
  //printWChar(tt);
  std::string test;//="ço pè à ê";
  std::wstring wsFrtest=L"ço pè à ê";
  test=tools::convertWSToUtf8(wsFrtest);

  {
    std::cout << "string :"<<  test << std::endl;
    std::setlocale(LC_CTYPE, "en_US.UTF8");
    std::wcout << "wstring:"<< wsFrtest << std::endl;
    std::wcout.flush();

    std::cout << test.size() << ":s(9):"<< test.length() << std::endl;
    std::wcout << wsFrtest.size() << ":w(9):"<< wsFrtest.length() << std::endl;
  }
  int uu=0;
  for(auto chh:test)
  {
    std::cout <<++uu << ":s:"<< chh<<  ":sizeof:" << sizeof(chh) << ":" << tools::getNbreOfByteCode(chh) << std::endl;
  }
  uu=0;
  for(auto chh:wsFrtest)
  {
    printWChar(chh);
    std::wcout <<++uu << ":ws:"<< chh<<  ":sizeof:" << sizeof(chh) << ":" << tools::getNbreOfByteCode(chh) << std::endl;
  }
  wchar_t m_charac = L'ę';
  printf("%d\n", sizeof(m_charac));
  printf("%d\n", sizeof(L'ç'));
  printf("%d\n", m_charac);
return;
  std::cout << "sizeof:" << sizeof(tt) << std::endl;
  std::cout << "sizeof:" << sizeof(wchar_t) << std::endl;
  unsigned char res = 243&248;
  std::cout << 243  << ":" << res << ":" << (243&248) << std::endl;
  tools::getNbreOfByteCode(tt);
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CPPUNIT_TEST_SUITE_REGISTRATION( UnitTestStorage );

UnitTestStorage::UnitTestStorage()
{}

UnitTestStorage::~UnitTestStorage()
{}

void UnitTestStorage::setUp()
{
/*  std::string path = tools::getExePath();
  std::cout << std::endl << "path:[" <<  path <<  "]" << std::endl;
  path+= File::separator +"tests"+ File::separator + "ebook";
  std::cout << std::endl << "path:[" <<  path <<  "]" << std::endl;
  m_manageStorage.addMasterDirectories(path);
  m_manageStorage.addCommand("simu");*/
  m_stringTest = "";
  m_stringResultTest= "";
  m_stringWaitingtTest= "";
}

void UnitTestStorage::tearDown()
{
}

//<dc:title>FNA 1547 - Alex Courville - 08 - Captifs De Corvus</dc:title>
// Emile Zola
void UnitTestStorage::tu_flnaTitle()
{
 // m_manageStorage.addCommand("rename");
 // m_manageStorage.run();
  std::string path = tools::getExePath();
  std::cout << std::endl << "path:[" <<  path <<  "]" << std::endl;
  path+= File::separator +"tests"+ File::separator + "ebook"+ File::separator + "fleuveNoir";
  std::cout << std::endl << "path:[" <<  path <<  "]" << std::endl;

  // FNA 1547 - Alex Courville - 08 - Captifs De Corvus
  std::string fileName = "FNA XXX - Serie Name - Serie Number - Title.epub";
  File::FileSystemItem fileSystemItem(nullptr,path,fileName);
  File::EBookFileInfo ebookFile(fileSystemItem,false);
  m_stringResultTest = ebookFile.buildNewName();
  // Ici correct mais le builder s est trompe entre Alex et Alec ( c est Alex)
  m_stringWaitingtTest = "Barbet,Pierre-Captifs De Corvus-[Alec Courville-8]";
  std::cout << std::endl <<"Result: ["<< m_stringResultTest << "]"<<std::endl;
  std::cout << std::endl <<"Waiting:["<< m_stringWaitingtTest << "]"<<std::endl;
  CPPUNIT_ASSERT_MESSAGE( "Fleuve Noir: [FNA XXX - Serie Name - Serie Number - Title]", m_stringWaitingtTest==m_stringResultTest );

  // FNA 0765 - La Révolte de Zarmou
  fileName = "FNA XXXX - Title.epub"  ;
  fileSystemItem= File::FileSystemItem(nullptr,path,fileName);
  ebookFile = File::EBookFileInfo(fileSystemItem,false);
  m_stringResultTest = ebookFile.buildNewName();
  m_stringWaitingtTest = "Murcie,Goerges-La Révolte de Zarmou";
  std::cout << std::endl <<"Result: ["<< m_stringResultTest << "]"<<std::endl;
  std::cout << std::endl <<"Waiting:["<< m_stringWaitingtTest << "]"<<std::endl;
  CPPUNIT_ASSERT_MESSAGE( "Fleuve Noir FNA XXXX - Title", m_stringWaitingtTest==m_stringResultTest );

  // FNA 971 - Moi, le feu - Maurice Limat
  fileName = "FNA XXX - Title - Serie Name.epub"  ;
  fileSystemItem= File::FileSystemItem(nullptr,path,fileName);
  ebookFile = File::EBookFileInfo(fileSystemItem,false);
  m_stringResultTest = ebookFile.buildNewName();
  m_stringWaitingtTest = "Maurice,Limat-Moi, le feu";
  std::cout << std::endl <<"Result: ["<< m_stringResultTest << "]"<<std::endl;
  std::cout << std::endl <<"Waiting:["<< m_stringWaitingtTest << "]"<<std::endl;
  CPPUNIT_ASSERT_MESSAGE( "Fleuve Noir, FNA XXX - Title - Serie Name", m_stringWaitingtTest==m_stringResultTest );

  // FNA 0710-Bera Paul-La nuit est morte
  fileName = "FNA XXX-Title-Serie Name.epub"  ;
  fileSystemItem= File::FileSystemItem(nullptr,path,fileName);
  ebookFile = File::EBookFileInfo(fileSystemItem,false);
  m_stringResultTest = ebookFile.buildNewName();
  m_stringWaitingtTest = "Bera,Paul - La nuit est morte";
  std::cout << std::endl <<"Result: ["<< m_stringResultTest << "]"<<std::endl;
  std::cout << std::endl <<"Waiting:["<< m_stringWaitingtTest << "]"<<std::endl;
  CPPUNIT_ASSERT_MESSAGE( "Fleuve Noir, FNA XXX-Title-Serie Name", m_stringWaitingtTest==m_stringResultTest );
}

void UnitTestStorage::tu_movie()
{
  std::string path = tools::getExePath();
  std::cout << std::endl << "path:[" <<  path <<  "]" << std::endl;
  path+= File::separator +"tests"+ File::separator + "movie";//+ File::separator + "fleuveNoir";
  std::cout << std::endl << "path:[" <<  path <<  "]" << std::endl;	
  
  std::string fileName = "[ www.Torrent9.PH ] Doctor.Who.2005.S11E04.VOSTFR.HDTV.XviD-EXTREME.avi"  ;
  File::FileSystemItem  fileSystemItem= File::FileSystemItem(nullptr,path,fileName);  
  File::VideoFileInfo movieFile = File::VideoFileInfo(fileSystemItem,false);

  m_stringResultTest = movieFile.buildNewName();
  m_stringWaitingtTest = "Bera,Paul - La nuit est morte";
  std::cout << std::endl <<"Result: ["<< m_stringResultTest << "]"<<std::endl;
  std::cout << std::endl <<"Waiting:["<< m_stringWaitingtTest << "]"<<std::endl;
  CPPUNIT_ASSERT_MESSAGE( "Fleuve Noir, FNA XXX-Title-Serie Name", m_stringWaitingtTest==m_stringResultTest );  
}

bool unit_tests_storage()
{
  // Create the event manager and test controller
  CPPUNIT_NS::TestResult controller;

  // Add a listener that colllects test result
  CPPUNIT_NS::TestResultCollector result;
  controller.addListener( &result );

  // Add a listener that print dots as test run.
  CPPUNIT_NS::BriefTestProgressListener progress;
  controller.addListener( &progress );

  // Add the top suite to the test runner
  CPPUNIT_NS::TestRunner runner;
  runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest() );
  runner.run( controller );

  // Print test in a compiler compatible format.
  CPPUNIT_NS::CompilerOutputter outputter( &result, CPPUNIT_NS::stdCOut() );
  outputter.write();

  return result.wasSuccessful()  ;
}

void unit_tests()
{
  //unit_tests_storage();
  tests::unitTeststools();
}
}

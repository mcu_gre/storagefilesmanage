cwd=`dirname $0`
BASEDIR=$(realpath "$cwd")
export PATH=$PATH:$BASEDIR

. "$BASEDIR/def.sh"

onexit()
{
  while caller $((n++)); do :; done;
  #echo "Stop cleaning after error"
}

#trap 'onexit' ERR

cleanMsg "main folder: $PWD"
$SCRIPTPATH/clearcmake.sh
set -e

cleanMsg "ext_inc"
cd "$BASEDIR/ext_inc"
set +e
rm -r * 2> /dev/null 1>&2
cd ..

cleanMsg "ext_lib"
set -e
cd "$BASEDIR/ext_lib"
set +e
rm -r * 2> /dev/null 1>&2
cd ..


cd libebook/lib/
cleanMsg "ebook: $PWD"
$SCRIPTPATH/clearcmake.sh
cd ..
cd  ..
cd tools

cleanMsg "tools: $PWD"

$SCRIPTPATH/clearcmake.sh
cd ..
cd exe

cleanMsg "exe: $PWD"
$SCRIPTPATH/clearcmake.sh

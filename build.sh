#!/bin/bash


DIR=$1

FOLDER_DONT_EXIST=1

cwd=`dirname $0`
BASEDIR=$(realpath "$cwd")
export PATH=$PATH:$BASEDIR

cd $BASEDIR

. "$BASEDIR/def.sh"

MEDIAINFO_BASEDIR="$BASEDIR""/mediaInfo"
ZENLIB_BASEDIR="$MEDIAINFO_BASEDIR""/ZenLib"
ZENLIB_CONFIGURE_DIR="$ZENLIB_BASEDIR/Project/GNU/Library"
ZENLIB_LIBRARY_FILE="$ZENLIB_CONFIGURE_DIR""/.libs/libzen.a"
MEDIAINFO_DIR="$BASEDIR""/mediaInfo/MediaInfoLib/Project/GNU/Library"
MEDIAINFO_LIB_FILE="$MEDIAINFO_DIR""/.libs/libmediainfo.a"

BUILD_LIB_UTILITY="$BASEDIR""/buildroot/package/mediaInfoA/buildStaticLib.sh"

EXT_LIB_DIR="$BASEDIR""/ext_lib"
EXT_INC_DIR="$BASEDIR""/ext_inc"

function initFolder()
{
  printMsg "create $EXT_LIB_DIR"
  mkdir -p "$EXT_LIB_DIR"
  mkdir -p "$EXT_INC_DIR"
}

function testFolder
{
  if [ -e "$2" ] ;
  then
    printMsg "$1:$2 Ok"
  else
    printErrorMsg "Error: no [$1:$2] folder"
    exit $FOLDER_DONT_EXIST
  fi
}

function TestVar()
{
  testFolder "ZENLIB_BASEDIR" "$ZENLIB_BASEDIR"
  testFolder "ZENLIB_CONFIGURE_DIR" "$ZENLIB_CONFIGURE_DIR"

  testFolder "BUILD_LIB_UTILITY" "$BUILD_LIB_UTILITY"

  testFolder "MEDIAINFO_DIR" "$MEDIAINFO_DIR"
  testFolder "EXT_LIB_DIR" "$EXT_LIB_DIR"
}

compileLibAndInstall()
{
  printMsg "compileLibAndInstall $1  $2"
  cd $1
  printMsg "Compiling: " $(basename "$2")
  autoreconf -f -i
  ./configure --enable-static --disable-shared --includedir "$EXT_INC_DIR" --libdir="$EXT_LIB_DIR"
  make clean
  make
  make install
}

compileMediaInfo()
{
  compileLibAndInstall "$ZENLIB_CONFIGURE_DIR" "$ZENLIB_LIBRARY_FILE" "$ZENLIB_INCLUDE_FILES"
  compileLibAndInstall "$MEDIAINFO_DIR" "$MEDIAINFO_LIB_FILE" "$MEDIAINFO_INCLUDE_FILES"
  $BUILD_LIB_UTILITY ar "$EXT_LIB_DIR" "$EXT_LIB_DIR" storageStatic ranlib
}

compileFileStorage()
{
  cd "$BASEDIR"
  printMsg "Compiling File Management...."
  cmake -G "Unix Makefiles" -DAUTONOME_COMPILATION=on -DENABLE_UNICODE=on -DCMAKE_BUILD_TYPE=Debug -DENABLE_STATIC=YES
  make
}

clean()
{
  cleanMsg "Zenlib"
  cd "$ZENLIB_CONFIGURE_DIR"
  make clean
  cleanMsg "Media Info"
  cd "$MEDIAINFO_DIR"
  make clean
  "$BASEDIR"/clean.sh
}


TARGET=$1
ACTION=$2

initFolder
TestVar

usage()
{
echo   "usage: build.sh TARGET"
echo   "  where TARGET:"
echo   "    - mediaInfo: build Multimedia video tools"
echo   "    - fileStorage: build fileStorage"
echo   "    - all"
echo   "    - clean: clean all"
}

if [ "$TARGET" == "mediaInfo" ] ;
then
  compileMediaInfo
elif [ "$TARGET" == "fileStorage" ] ;
then
  compileFileStorage
elif [ "$TARGET" == "clean" ] ;
then
  clean
elif [ "$TARGET" == "all" ] ;
then
  compileMediaInfo
  compileFileStorage
else
  usage
fi

exit 0

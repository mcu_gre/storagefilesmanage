/*
 *  Tools Unit Test
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#include <sys/stat.h>
#include <errno.h>
#include <iostream>
#include "systemtools.h"
#include "stringtools.h"
#include "converter.h"
#include "unit_test_tools.h"

#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

namespace tests
{
// Enregistrement des différents cas de tests
CPPUNIT_TEST_SUITE_REGISTRATION( UnitTestStringTools );



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
UnitTestStringTools::UnitTestStringTools() {}
UnitTestStringTools::~UnitTestStringTools() {}
void UnitTestStringTools::setUp()
{
  m_stringTest = "";
  m_stringResultTest= "";
  m_stringWaitingtTest= "";
}
void UnitTestStringTools::tearDown()
{
}

void UnitTestStringTools::tu_checkAlpha()
{
  m_stringTest = "zer er 4";
  m_stringWaitingtTest = "zerer";
  m_stringResultTest = tools::checkAlpha(m_stringTest);
  CPPUNIT_ASSERT_MESSAGE( "classe valide ascii characteres ", m_stringResultTest.compare(m_stringWaitingtTest)==0 );

  m_stringTest= "&;:.ézer er 4";
  m_stringWaitingtTest = "zerer";
  m_stringResultTest = tools::checkAlpha(m_stringTest);
  CPPUNIT_ASSERT_MESSAGE( "classe valide special char"+m_stringTest, m_stringResultTest.compare(m_stringWaitingtTest)==0 );
}

void UnitTestStringTools::tu_trim()
{
  m_stringTest = "  part1 part2 part3  ";
  m_stringWaitingtTest = "part1 part2 part3";
  m_stringResultTest = tools::trim(m_stringTest);
  CPPUNIT_ASSERT_MESSAGE( "classe valide espace avt milieu apres", m_stringResultTest.compare(m_stringWaitingtTest)==0 );

  m_stringTest = "  part1 part2 part3";
  m_stringWaitingtTest = "part1 part2 part3";
  m_stringResultTest = tools::trim(m_stringTest);
  CPPUNIT_ASSERT_MESSAGE( "classe valide espace avt milieu", m_stringResultTest.compare(m_stringWaitingtTest)==0 );

  m_stringTest = "part1 part2 part3";
  m_stringWaitingtTest = "part1 part2 part3";
  m_stringResultTest = tools::trim(m_stringTest);
  CPPUNIT_ASSERT_MESSAGE( "classe valide pas d espace ext", m_stringResultTest.compare(m_stringWaitingtTest)==0 );

  m_stringTest = "part1part2part3";
  m_stringWaitingtTest = "part1part2part3";
  m_stringResultTest = tools::trim(m_stringTest);
  CPPUNIT_ASSERT_MESSAGE( "classe valide pas d espace", m_stringResultTest.compare(m_stringWaitingtTest)==0 );

  m_stringTest = "   ";
  m_stringWaitingtTest = "";
  m_stringResultTest = tools::trim(m_stringTest);
  CPPUNIT_ASSERT_MESSAGE( "classe valide que des espaces", m_stringResultTest.compare(m_stringWaitingtTest)==0 );
}

void UnitTestStringTools::tu_split()
{
// Hello- my World - bye-bye
  m_stringTest = "Hello World - bye - bye";
  m_listWaitingtTest = {"Hello World","bye","bye"};
  m_listResultTest = tools::split(m_stringTest,'-');
  CPPUNIT_ASSERT_MESSAGE( "item with space -without option", m_listResultTest==m_listWaitingtTest );

  m_stringTest = "Hello World - bye - bye";
  m_listWaitingtTest = {"Hello World","bye","bye"};
  m_listResultTest = tools::split(m_stringTest,'-',true);
  CPPUNIT_ASSERT_MESSAGE( "item with space -with option", m_listResultTest==m_listWaitingtTest );

  m_stringTest = "Hello World - bye-bye";
  m_listWaitingtTest = {"Hello World","bye-bye"};
  m_listResultTest = tools::split(m_stringTest,'-',true);
  CPPUNIT_ASSERT_MESSAGE( "item without space  -with option", m_listResultTest==m_listWaitingtTest );

  m_listWaitingtTest = {"Hello World","bye","bye"};
  m_listResultTest = tools::split(m_stringTest,'-',false);
  CPPUNIT_ASSERT_MESSAGE( "item without space  -with option", m_listResultTest==m_listWaitingtTest );

  m_stringTest = "Hello World - bye- bye";
  m_listWaitingtTest = {"Hello World","bye","bye"};
  m_listResultTest = tools::split(m_stringTest,'-',true);
  CPPUNIT_ASSERT_MESSAGE( "item without space left -with option", m_listResultTest==m_listWaitingtTest );

  m_stringTest = "Hello World - bye -bye";
  m_listWaitingtTest = {"Hello World","bye","bye"};
  m_listResultTest = tools::split(m_stringTest,'-',true);
  CPPUNIT_ASSERT_MESSAGE( "item without space right -with option", m_listResultTest==m_listWaitingtTest );

  m_stringTest = "Hello World - bye -";
  m_listWaitingtTest = {"Hello World","bye"};
  m_listResultTest = tools::split(m_stringTest,'-',true);
  CPPUNIT_ASSERT_MESSAGE( "last char only separator -with option", m_listResultTest==m_listWaitingtTest );

  m_stringTest = "Hello World - bye-";
  m_listWaitingtTest = {"Hello World","bye"};
  m_listResultTest = tools::split(m_stringTest,'-',true);
  CPPUNIT_ASSERT_MESSAGE( "last char only separator -with option", m_listResultTest==m_listWaitingtTest );

  m_stringTest = "Hello World - bye- ";
  m_listWaitingtTest = {"Hello World","bye"};
  m_listResultTest = tools::split(m_stringTest,'-',true);
  CPPUNIT_ASSERT_MESSAGE( "last char only separator -with option", m_listResultTest==m_listWaitingtTest );

  m_stringTest = "Hello World - bye - ";
  m_listWaitingtTest = {"Hello World","bye"};
  m_listResultTest = tools::split(m_stringTest,'-',true);
  CPPUNIT_ASSERT_MESSAGE( "last char only separator -with option", m_listResultTest==m_listWaitingtTest );

  m_stringTest = "-Hello World - bye";
  m_listWaitingtTest = {"Hello World","bye"};
  m_listResultTest = tools::split(m_stringTest,'-',true);
  CPPUNIT_ASSERT_MESSAGE( "item without space right -with option", m_listResultTest==m_listWaitingtTest );

  m_stringTest = "- Hello World - bye";
  m_listWaitingtTest = {"Hello World","bye"};
  m_listResultTest = tools::split(m_stringTest,'-',true);
  CPPUNIT_ASSERT_MESSAGE( "item without space right -with option", m_listResultTest==m_listWaitingtTest );

  m_stringTest = " - Hello World - bye";
  m_listWaitingtTest = {"Hello World","bye"};
  m_listResultTest = tools::split(m_stringTest,'-',true);
  CPPUNIT_ASSERT_MESSAGE( "item without space right -with option", m_listResultTest==m_listWaitingtTest );

  m_stringTest = "|";
  m_listWaitingtTest = {};
  m_listResultTest = tools::split(m_stringTest,'|',true);
  CPPUNIT_ASSERT_MESSAGE( "item without space right -with option", m_listResultTest==m_listWaitingtTest );

  m_stringTest = " |";
  m_listWaitingtTest = {};
  m_listResultTest = tools::split(m_stringTest,'|',true);
  CPPUNIT_ASSERT_MESSAGE( "item without space right -with option", m_listResultTest==m_listWaitingtTest );

  m_stringTest = " | ";
  m_listWaitingtTest = {};
  m_listResultTest = tools::split(m_stringTest,'|',true);
  CPPUNIT_ASSERT_MESSAGE( "item without space right -with option", m_listResultTest==m_listWaitingtTest );

  m_stringTest = "| ";
  m_listWaitingtTest = {};
  m_listResultTest = tools::split(m_stringTest,'|',true);
  CPPUNIT_ASSERT_MESSAGE( "item without space right -with option", m_listResultTest==m_listWaitingtTest );

  m_stringTest = " first | |seconde";
  m_listWaitingtTest = {"first","seconde"};
  m_listResultTest = tools::split(m_stringTest,'|',true);
  CPPUNIT_ASSERT_MESSAGE( "item without space right -with option", m_listResultTest==m_listWaitingtTest );

  m_stringTest = " sans separator ";
  m_listWaitingtTest = {"sans separator"};
  m_listResultTest = tools::split(m_stringTest,'|',true);
  CPPUNIT_ASSERT_MESSAGE( "item without space right -with option", m_listResultTest==m_listWaitingtTest );
};

void UnitTestStringTools::tu_formatName()
{
  m_stringTest ="CECIESTENMAJUSCULE";
  m_stringResultTest=tools::formatName(m_stringTest);
  m_stringWaitingtTest = "Ceciestenmajuscule";
  CPPUNIT_ASSERT_MESSAGE( "All majuscule", m_stringWaitingtTest==m_stringResultTest );

  m_stringTest ="estbienmaintenant";
  m_stringResultTest=tools::formatName(m_stringTest);
  m_stringWaitingtTest = "Estbienmaintenant";
  CPPUNIT_ASSERT_MESSAGE( "All minuscule", m_stringWaitingtTest==m_stringResultTest );

  m_stringTest ="Estbienmaintenant";
  m_stringResultTest=tools::formatName(m_stringTest);
  m_stringWaitingtTest = "Estbienmaintenant";
  CPPUNIT_ASSERT_MESSAGE( "Already done", m_stringWaitingtTest==m_stringResultTest );

  m_stringTest ="EstbienMaintenant";
  m_stringResultTest=tools::formatName(m_stringTest);
  m_stringWaitingtTest = "Estbienmaintenant";
  CPPUNIT_ASSERT_MESSAGE( "One inside", m_stringWaitingtTest==m_stringResultTest );

  m_stringTest ="";
  m_stringResultTest=tools::formatName(m_stringTest);
  m_stringWaitingtTest = "";
  CPPUNIT_ASSERT_MESSAGE( "Empty", m_stringWaitingtTest==m_stringResultTest );

  m_stringTest ="jeAn-franCois";
  m_stringResultTest=tools::formatName(m_stringTest);
  m_stringWaitingtTest = "Jean-Francois";
  CPPUNIT_ASSERT_MESSAGE( "Particule name with -", m_stringWaitingtTest==m_stringResultTest );

  m_stringTest ="jeAn.franCois";
  m_stringResultTest=tools::formatName(m_stringTest);
  m_stringWaitingtTest = "Jean.Francois";
  CPPUNIT_ASSERT_MESSAGE( "Particule name with -", m_stringWaitingtTest==m_stringResultTest );
};

void UnitTestStringTools::tu_isAForbiddenCharacter()
{
  CPPUNIT_ASSERT_MESSAGE( "character /", tools::isAForbiddenCharacter('/') );
  CPPUNIT_ASSERT_MESSAGE( "character \\", tools::isAForbiddenCharacter('\\') );
  CPPUNIT_ASSERT_MESSAGE( "character <", tools::isAForbiddenCharacter('<') );
  CPPUNIT_ASSERT_MESSAGE( "character >", tools::isAForbiddenCharacter('>') );
  CPPUNIT_ASSERT_MESSAGE( "character *", tools::isAForbiddenCharacter('*') );
  CPPUNIT_ASSERT_MESSAGE( "character ?", tools::isAForbiddenCharacter('?') );
  CPPUNIT_ASSERT_MESSAGE( "character |", tools::isAForbiddenCharacter('|') );
  CPPUNIT_ASSERT_MESSAGE( "character \\", tools::isAForbiddenCharacter('\\') );
  CPPUNIT_ASSERT_MESSAGE( "character :", tools::isAForbiddenCharacter(':') );
  CPPUNIT_ASSERT_MESSAGE( "character s", !tools::isAForbiddenCharacter('s') );
}

void UnitTestStringTools::tu_isAlphaDigit()
{
  m_charac = 'a';
  CPPUNIT_ASSERT_MESSAGE( "character a", tools::isAlphaDigit(m_charac) );
  m_charac = L'ç';
  CPPUNIT_ASSERT_MESSAGE( "character ç", tools::isAlphaDigit(m_charac) );
  m_charac = L'à';
  CPPUNIT_ASSERT_MESSAGE( "character à", tools::isAlphaDigit(m_charac) );
  m_charac = L'ę';
  CPPUNIT_ASSERT_MESSAGE( "character ę", tools::isAlphaDigit(m_charac) );
  m_charac = '7';
  CPPUNIT_ASSERT_MESSAGE( "character 7", tools::isAlphaDigit(m_charac) );
  m_charac = '&';
  CPPUNIT_ASSERT_MESSAGE( "character &", !tools::isAlphaDigit(m_charac) );
};

void UnitTestStringTools::tu_getNbreOfByteCode()
{
  m_stringTest ="ç";
  CPPUNIT_ASSERT_MESSAGE( "character ç", tools::getNbreOfByteCode(m_stringTest[0])==2 );
  m_stringTest ="ę";
  CPPUNIT_ASSERT_MESSAGE( "character ę", tools::getNbreOfByteCode(m_stringTest[0])==2 );
  m_stringTest ="ᘃ";
  CPPUNIT_ASSERT_MESSAGE( "character ᘃ", tools::getNbreOfByteCode(m_stringTest[0])==3 );
  m_stringTest ="字";
  CPPUNIT_ASSERT_MESSAGE( "character 字", tools::getNbreOfByteCode(m_stringTest[0])==3 );
};

void UnitTestStringTools::tu_replaceForbiddenCharacter()
{
  m_stringTest="Forbiden [/]-[\\]-[<]-[>]-[*]-[?]-[|]-[\"]-[:] est oui";
  m_stringResultTest = tools::replaceForbiddenCharacter( m_stringTest,'&');
  m_stringWaitingtTest ="Forbiden [&]-[&]-[&]-[&]-[&]-[&]-[&]-[&]-[&] est oui";
  CPPUNIT_ASSERT_MESSAGE( "all forbidden characters by &", m_stringWaitingtTest==m_stringResultTest );
};
void UnitTestStringTools::tu_convertToWs(){};
void UnitTestStringTools::tu_convertToSs(){};
void UnitTestStringTools::tu_narrow(){};

void UnitTestStringTools::tu_findWithoutCase()
{
  m_stringTest="Ceci esT ma DEStinatioN";
  std::size_t pos= tools::findWithoutCase( m_stringTest,"destiNation");
  CPPUNIT_ASSERT_MESSAGE( "end", pos==12 );

  pos= tools::findWithoutCase( m_stringTest,"cecI");
  CPPUNIT_ASSERT_MESSAGE( "beginning", pos==0 );

  pos= tools::findWithoutCase( m_stringTest,"Est");
  CPPUNIT_ASSERT_MESSAGE( "middle", pos==5 );

  pos= tools::findWithoutCase( m_stringTest,"ma");
  CPPUNIT_ASSERT_MESSAGE( "middle same case", pos==9 );

  pos= tools::findWithoutCase( m_stringTest,"absent");
  CPPUNIT_ASSERT_MESSAGE( "no present", pos==std::string::npos );
};

void UnitTestStringTools::tu_findAndReplace()
{
  m_stringTest = "Ceci et ma destination, ceci et mon reve ";
  m_stringResultTest   = tools::findAndReplace( m_stringTest,"et","est");
  m_stringWaitingtTest = "Ceci est ma destination, ceci est mon reve ";
  CPPUNIT_ASSERT_MESSAGE( "full", m_stringWaitingtTest==m_stringResultTest );

  m_stringTest = "Ceci et ma destination, ceci et mon reve ";
  m_stringResultTest   = tools::findAndReplace( m_stringTest,"la","absent");
  m_stringWaitingtTest = m_stringTest;
  CPPUNIT_ASSERT_MESSAGE( "nothing to do", m_stringWaitingtTest==m_stringResultTest );

  m_stringTest = "Ceci et ma destination, ceci et mon reve ";
  m_stringResultTest   = tools::findAndReplace( m_stringTest,"Ceci et ma destination, ","");
  m_stringWaitingtTest = "ceci et mon reve ";
  CPPUNIT_ASSERT_MESSAGE( "Remplace by empty string", m_stringWaitingtTest==m_stringResultTest );

  m_stringTest = "Ceci et ma destination, ceci et mon reve ";
  m_stringResultTest   = tools::findAndReplace( m_stringTest,"","aa");
  m_stringWaitingtTest = "Ceci et ma destination, ceci et mon reve ";
  CPPUNIT_ASSERT_MESSAGE( "Remplace search an empty string", m_stringWaitingtTest==m_stringResultTest );
}

void UnitTestStringTools::tu_toLowerCase()
{
  m_stringTest = "Ceci Est mA desTination, ceci est mon revE";
  m_stringResultTest   = tools::toLowerCase( m_stringTest);
  m_stringWaitingtTest = "ceci est ma destination, ceci est mon reve";
  CPPUNIT_ASSERT_MESSAGE( "full", m_stringWaitingtTest==m_stringResultTest);

  m_stringTest = "ceci est ma destination, ceci est mon reve";
  m_stringResultTest   = tools::toLowerCase( m_stringTest);
  m_stringWaitingtTest = "ceci est ma destination, ceci est mon reve";
  CPPUNIT_ASSERT_MESSAGE( "nothing to do", m_stringWaitingtTest==m_stringResultTest);
};

void UnitTestStringTools::tu_generateUUID()
{
  int waitingPart = 5;
  int waitingLen = 59;
  m_stringTest = tools::generateUUID();
  auto list = tools::split(m_stringTest,'-');
  int part = list.size();
  int len = m_stringTest.size();
std::cout << "tu_generateUUID" << waitingLen << " " <<  len << std::endl;
  CPPUNIT_ASSERT_MESSAGE( "number of part", waitingPart==part);
  CPPUNIT_ASSERT_MESSAGE( "Size", waitingLen==len);
  CPPUNIT_ASSERT_MESSAGE( "Regenerate different", tools::generateUUID()!=tools::generateUUID());
};

void UnitTestStringTools::tu_splitbySep()
{
  m_stringTest= "ceci est mon test (458) et encore ...";
  m_vectorResultTest  = tools::splitbySep(m_stringTest,"()");
  m_vectorWaitingtTest = {"ceci est mon test", "458", "et encore ..."};
  CPPUNIT_ASSERT_MESSAGE( "n", m_vectorWaitingtTest==m_vectorResultTest);

  m_stringTest= "ceci est mon test [ 458 ] et encore ...";
  m_vectorResultTest  = tools::splitbySep(m_stringTest,"[]");
  m_vectorWaitingtTest = {"ceci est mon test", "458", "et encore ..."};
  CPPUNIT_ASSERT_MESSAGE( "n", m_vectorWaitingtTest==m_vectorResultTest);

  m_stringTest= "ceci est mon test [ 458  et encore ...";
  m_vectorResultTest  = tools::splitbySep(m_stringTest,"[]");
  m_vectorWaitingtTest = {};
  CPPUNIT_ASSERT_MESSAGE( "n", m_vectorWaitingtTest==m_vectorResultTest);

  m_stringTest= "ceci est mon test 458 ]  et encore ...";
  m_vectorResultTest  = tools::splitbySep(m_stringTest,"[]");
  m_vectorWaitingtTest = {};
  CPPUNIT_ASSERT_MESSAGE( "n", m_vectorWaitingtTest==m_vectorResultTest);

  m_stringTest= "ceci est mon test 458  et encore ...";
  m_vectorResultTest  = tools::splitbySep(m_stringTest,"[]");
  m_vectorWaitingtTest = {};
  CPPUNIT_ASSERT_MESSAGE( "n", m_vectorWaitingtTest==m_vectorResultTest);

  m_stringTest= "ceci est mon test [  ] et encore ...";
  m_vectorResultTest  = tools::splitbySep(m_stringTest,"[]");
  m_vectorWaitingtTest = {"ceci est mon test", "", "et encore ..."};
  CPPUNIT_ASSERT_MESSAGE( "n", m_vectorWaitingtTest==m_vectorResultTest);
};

void UnitTestStringTools::tu_extractNumberFromString()
{
  m_stringTest = "5";
  bool res =true;
  try
  {
    m_intResultTest = tools::extractNumberFromString( m_stringTest,nullptr);
    res =true;
  }
  catch(std::exception & e)
  {
    res = false;
  };
  m_intWaitingtTest = 5;
  CPPUNIT_ASSERT_MESSAGE( "nominal", (res && (m_intResultTest==m_intWaitingtTest) ));

  m_stringTest = "5 Postfix";
  res =false;
  try
  {
    m_intResultTest = tools::extractNumberFromString( m_stringTest,nullptr);
    res =true;
  }
  catch(std::exception & e){res = false;};
  m_intWaitingtTest = 5;
  CPPUNIT_ASSERT_MESSAGE( "nominal", (res && (m_intResultTest==m_intWaitingtTest) ));

  m_stringTest = "Vol. 5";
  m_arg1 = "Vol.";
  res =false;
  try { m_intResultTest = tools::extractNumberFromString( m_stringTest,&m_arg1);res =true;} catch(std::exception & e){res = false;};
  m_intWaitingtTest = 5;
  CPPUNIT_ASSERT_MESSAGE( "with prefix", (res && (m_intResultTest==m_intWaitingtTest) ));

  m_stringTest = "Tome 5";
  m_arg1 = "Tome";
  res =false;
  try { m_intResultTest = tools::extractNumberFromString( m_stringTest,&m_arg1);res =true;} catch(std::exception & e){res = false;};
  m_intWaitingtTest = 5;
  CPPUNIT_ASSERT_MESSAGE( "with prefix", (res && (m_intResultTest==m_intWaitingtTest) ));

  m_stringTest = "Tome cinq";
  m_arg1 = "Tome";
  res =false;
  try { m_intResultTest = tools::extractNumberFromString( m_stringTest,&m_arg1);res =true;} catch(std::exception & e){res = false;};
  m_intWaitingtTest = 5;
  CPPUNIT_ASSERT_MESSAGE( "no number", (!res));

  m_stringTest = "Tome 5";
  m_arg1 = "absent";
  res =false;
  try { m_intResultTest = tools::extractNumberFromString( m_stringTest,&m_arg1);res =true;} catch(std::exception & e){res = false;};
  m_intWaitingtTest = 5;
  CPPUNIT_ASSERT_MESSAGE( "no prefix", (!res));

  m_stringTest = "Tome cinq";
  m_arg1 = "Tome";
  res =false;
  try { m_intResultTest = tools::extractNumberFromString( m_stringTest,&m_arg1);res =true;} catch(std::exception & e){res = false;};
  m_intWaitingtTest = 5;
  CPPUNIT_ASSERT_MESSAGE( "no prefix", (!res));

  m_stringTest = "Tome cinq";
  m_arg1 = "";
  res =false;
  try { m_intResultTest = tools::extractNumberFromString( m_stringTest,&m_arg1);res =true;} catch(std::exception & e){res = false;};
  m_intWaitingtTest = 5;
  CPPUNIT_ASSERT_MESSAGE( "no number", (!res));

  m_stringTest = "5";
  m_arg1 = "";
  res =false;
  try { m_intResultTest = tools::extractNumberFromString( m_stringTest,&m_arg1);res =true;} catch(std::exception & e){res = false;};
  m_intWaitingtTest = 5;
  CPPUNIT_ASSERT_MESSAGE( "with prefix", (res && (m_intResultTest==m_intWaitingtTest) ));
}

void UnitTestStringTools::tu_removeBracketTag()
{
  m_stringTest = " helloe [Inside Bracket] qsfa";
  m_stringWaitingtTest = " helloe  qsfa";

  m_stringResultTest = tools::removeBracketTag( m_stringTest);

  CPPUNIT_ASSERT_MESSAGE( "normal case", m_stringWaitingtTest==m_stringResultTest);
}
void UnitTestStringTools::tu_findFirstTag()
{

}

bool unitTeststools()
{
  // Create the event manager and test controller
  CPPUNIT_NS::TestResult controller;

  // Add a listener that colllects test result
  CPPUNIT_NS::TestResultCollector result;
  controller.addListener( &result );

  // Add a listener that print dots as test run.
  CPPUNIT_NS::BriefTestProgressListener progress;
  controller.addListener( &progress );

  // Add the top suite to the test runner
  CPPUNIT_NS::TestRunner runner;
  runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest() );
  runner.run( controller );

  // Print test in a compiler compatible format.
  CPPUNIT_NS::CompilerOutputter outputter( &result, CPPUNIT_NS::stdCOut() );
  outputter.write();

  return result.wasSuccessful()  ;
}
}

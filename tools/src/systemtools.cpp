/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#include <sys/stat.h>
#include <libgen.h>
#include <cerrno>
#include <iostream>
#include <string.h>
#include "unistd.h"
#include "systemtools.h"

namespace tools
{

  static bool gSimulated = true;

  void setSimulated(bool simulated )
  {
    gSimulated = simulated;
  }

  bool getSimulated()
  {
    return gSimulated ;
  }

  bool mkdir_impl(std::string newDirectory)
  {
	 mkdir(newDirectory.c_str(),S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	 return true; 
  }

  bool rename_impl(std::string oldName, std::string newName,bool bForce)
  {
    struct stat st = {0};

    bool isUpdated = false;
    
    // Rename only if newName doesn t exist or if the caller wants to force the renaming 
    if ( bForce || stat(newName.c_str(), &st) == -1)
    {
      isUpdated = true;
      if ( !gSimulated && (0 != rename(oldName.c_str(),newName.c_str())) )
      {
         isUpdated = false;
         perror("rename() failed");
         //throw SystemeException(strerror(errno) ,errno,oldName,newName);
      }
    }
    else
    {
	  //throw SystemeException("File exists",0,oldName,newName);
	}
    return isUpdated;
  }


  bool remove_impl(const std::string& name)
  {
    return !gSimulated && (0 == remove(name.c_str()));
  }

  std::string getExePath()
  {
    const int PATH_MAX = 500;
    char result[ PATH_MAX ];
    size_t count = readlink( "/proc/self/exe", result, PATH_MAX );
     char *dirc = strdup(result);
    std::string path =dirname(dirc);
    free(dirc);
    return path;
    return std::string( path, (count > 0) ? count : 0 );
  }
};


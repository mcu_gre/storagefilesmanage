/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */


#include <algorithm>
#include <iostream>
#include <regex>
#include <string>

#include "tag.h"
#include "stringtools.h"


namespace tools
{
//const char local[] ="fr_FR.UTF-8" ;
//const char local[] ="en_US.UTF8" ;

  std::string trim(const std::string& s)
  {
    auto wsfront=std::find_if_not(s.begin(),s.end(),[](int c){return std::isspace(c);});
    auto wsback=std::find_if_not(s.rbegin(),s.rend(),[](int c){return std::isspace(c);}).base();
    return (wsback<=wsfront ? std::string() : std::string(wsfront,wsback));
  }

  std::string checkAlpha(std::string str)
  {
    std::setlocale(LC_ALL, local_filter);
    str.erase(
               std::remove_if( str.begin(),
                               str.end(),
                               [](const wchar_t x){return !std::isalpha(x, std::locale(local_filter));}),
                               str.end()
               );
    return str;
  }

  std::string formatName(const std::string& str)
  {
    bool nextMustBeAlpha=false;
    std::string returnChar;
    std::string sep= " .-,;:!/\\()&'[]|@{}";
    for(auto ch: str)
    {
      if(nextMustBeAlpha)
      {
        returnChar+= ::tolower(ch);
      }
      else
      {
        returnChar+= ::toupper(ch);
      }
      nextMustBeAlpha = (sep.find(ch)==std::string::npos);
    }
    return returnChar;
  }

/* Not used
  bool isAlphaDigit2(wchar_t character)
  {
    std::setlocale(LC_ALL, local_filter);
    return std::isalpha(character, std::locale(local_filter))||std::isdigit(character, std::locale(local_filter));
  }
*/

  std::list<std::string> split(const std::string &s, char delim,bool mustHaveSpaceBeforeAfterDelimiter)
  {
    std::stringstream ss(s);
    std::string item;
    std::list<std::string> tokens;
    std::string itemWithNoSpace;
    while (getline(ss, item, delim))
    {
      if(trim(item).size()!=0)
      {
        if(mustHaveSpaceBeforeAfterDelimiter)
        {
          bool hasPrevious = itemWithNoSpace.size()!=0;
          bool hasFirstSpace = std::isspace(item.front());
          bool hasLastSpace = std::isspace(item.back());

          if(!hasPrevious && !hasLastSpace)
          {
            itemWithNoSpace = trim(item);
          }
          else
          if(!hasFirstSpace && !hasLastSpace)
          {
            itemWithNoSpace += "-" + trim(item) ;
          }
          else
          if(hasLastSpace)
          {
            std::string toAdd = trim(item);
            if(hasPrevious)
            {
              toAdd = itemWithNoSpace +"-"+ trim(item);
              itemWithNoSpace = "";
            }
            tokens.push_back(toAdd);
          }
          else
          {
            if(hasPrevious)
            {
              tokens.push_back(itemWithNoSpace);
              itemWithNoSpace = "";
            }
            tokens.push_back(trim(item));
          }
        }
        else
        {
          tokens.push_back(trim(item));
        }
      }
    }
    if(itemWithNoSpace.size()!=0)
    {
      //itemWithNoSpace.pop_back();
      tokens.push_back( trim( itemWithNoSpace));
    }

    return tokens;
  }

  // example with "()" :Parse "partOne (partInside) partTwo"
  std::vector<std::string> splitbySep(std::string name,std::string sep)
  {
    std::string partOne;
    std::string partTwo;
    std::string partInside;
    std::vector<std::string> vec;
    bool isFullPartition = false;
    size_t  posOfOpenCroch = name.find_first_of(sep[0]);
    if(posOfOpenCroch!=std::string::npos)
    {
      size_t  posOfCloseCroch = name.find_first_of(sep[1]);
      if(posOfCloseCroch!=std::string::npos)
      {
        isFullPartition = true;
        partInside = name.substr(posOfOpenCroch+1,posOfCloseCroch-posOfOpenCroch-1);

        if(posOfOpenCroch>1 && name[posOfOpenCroch-1]=='.' )
          posOfOpenCroch--;

        if((posOfCloseCroch+1)<name.size()  && name[posOfCloseCroch+1]=='.' )
          posOfCloseCroch++;

        partOne = name.substr(0,posOfOpenCroch);
        partTwo = name.substr(posOfCloseCroch+1);
      }
    }
    if(isFullPartition)
    {
       vec.push_back(trim(partOne));
       vec.push_back(trim(partInside));
       vec.push_back(trim(partTwo));
    }
    return vec;
  }

  bool isAForbiddenCharacter(char character)
  {
    static std::vector<char> forbiddenChars = { '/', '\\' , '<','>','*','?','|','\"',':'};
    for(auto forbiddenChar:forbiddenChars)
    {
      if(forbiddenChar==character)
        return true;
    }
    return false;
  }

  bool hasForbiddenCharacter(const std::string& name)
  {
    for(auto& ItemChar:name)
    {
      if(isAForbiddenCharacter(ItemChar))
      {
        return true;
      }
    }

    return false;
  }

  std::string replaceForbiddenCharacter(std::string name, char newCaracter)
  {
    for(auto& ItemChar:name)
    {
      if(isAForbiddenCharacter(ItemChar))
      {
        ItemChar = newCaracter;
      }
    }

    return name;
  }

template <typename charT>
struct ichar
{
  operator charT() const
  {
    return toupper(x);
  }
  charT x;
};

template <typename charT>
static std::basic_string<ichar<charT> > *istring(const std::basic_string<charT> &s)
{
  return (std::basic_string<ichar<charT> > *)&s;
}

template <typename charT>
static ichar<charT> *istring(const charT *s)
{
  return (ichar<charT> *)s;
}

std::size_t findWithoutCase(const std::string& strToCompare,const char * strToSearch )
{
  return istring(strToCompare)->find(istring(strToSearch));
}

std::size_t findWithoutCase(const std::string&  strToCompare,std::string & strToSearch )
{
  return findWithoutCase(strToCompare,strToSearch.c_str());
}

std::string findAndReplace(std::string  subject, const std::string &  search,  const std::string &   sreplace )
{
  size_t pos = 0;
  if(search.size()>0&&subject.size()>0)
  while((pos = subject.find(search, pos)) != std::string::npos)
  {
    subject.replace(pos, search.length(), sreplace);
    pos += sreplace.length();
  }
  return subject;
}

int extractNumberFromString(std::string strNumber,const std::string * prefix)
{
  if(prefix)
  {
    strNumber = findAndReplace(strNumber,*prefix,"");
  }
  return std::stoi(strNumber);
}


std::string toLowerCase(const std::string & src)
{
  std::string lowerCase;
  for(auto c: src)
  {
    lowerCase.push_back(std::tolower(c,std::locale(tools::local_filter)));
  }
  return lowerCase;
}

std::size_t findFirstTag(std::string name,std::vector<Tag>& tagList)
{
  // Use end file position to init the position value to find
  size_t  firstPos=name.size();
  // Loop on all tags to find if some are presents in the name and if yes, find the first one
  for(auto& tag : tagList )
  {
    size_t  pos = tools::findWithoutCase(name,tag.m_name);
    if(pos<firstPos)
    {
      firstPos= pos;
    }
  }

  if(name.size()==firstPos)
  {
    firstPos = std::string::npos;
  }
  return firstPos;
}

std::string removeBracketTag(std::string name)
{
  std::string nameWithoutBracket=name;
  std::string workingName = nameWithoutBracket;

  do
  {
    workingName = nameWithoutBracket;
    size_t  posOfOpenCroch = workingName.find_first_of("[");
    if(posOfOpenCroch!=std::string::npos)
    {
      size_t  posOfCloseCroch = workingName.find_first_of("]");
      if(posOfCloseCroch!=std::string::npos)
      {
        std::string bracketName = workingName.substr(posOfOpenCroch,posOfCloseCroch-posOfOpenCroch);

        //if(tools::findWithoutCase(bracketName,"cpasbien")|| tools::findWithoutCase(bracketName,"Torrent9"))
        {
          if(posOfOpenCroch>1 && workingName[posOfOpenCroch-1]=='.' )
          {
            posOfOpenCroch--;
          }

          if((posOfCloseCroch+1)<workingName.size()  && workingName[posOfCloseCroch+1]=='.' )
          {
            posOfCloseCroch++;
          }

          nameWithoutBracket = workingName.substr(0,posOfOpenCroch);
          nameWithoutBracket += workingName.substr(posOfCloseCroch+1);
        }
      }
    }
  }
  while(workingName.compare(nameWithoutBracket)!=0);

  return nameWithoutBracket;
}


std::string generateUUID()
{
  char strUuid[100]={0};
  //srand(time(NULL));
  sprintf(strUuid, "%x%x-%x-%x-%x-%x%x%x",
  rand(), rand(),                 // Generates a 64-bit Hex number
  rand(),                         // Generates a 32-bit Hex number
  ((rand() & 0x0fff) | 0x4000),   // Generates a 32-bit Hex number of the form 4xxx (4 indicates the UUID version)
  rand() % 0x3fff + 0x8000,       // Generates a 32-bit Hex number in the range [0x8000, 0xbfff]
  rand(), rand(), rand());        // Generates a 96-bit Hex number
  //printf("UUID generated: %s\n",strUuid);
  return strUuid;
}


std::string getEndPrefix(std::string name, std::string prefix, size_t startPosition,char separator)
{
  size_t  endPosition =name.find(separator, startPosition);
  if(endPosition==std::string::npos)
  {
    endPosition = name.length();
  }
  // tag is between pos and dotPos
  return name.substr(startPosition+prefix.length(),endPosition-startPosition-prefix.length());
}
}



#include <iostream>
#include <string>
#include <vector>
#include <locale>
#ifdef QNAP
//#include <boost/locale/encoding_utf.hpp>
//using boost::locale::conv::utf_to_utf;
#else
#include <codecvt>
#endif
#include <iomanip>
#include "converter.h"



/*
std::wstring utf8_to_wstring(const std::string& str)
{
    return utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size());
}

std::string wstring_to_utf8(const std::wstring& str)
{
    return utf_to_utf<char>(str.c_str(), str.c_str() + str.size());
}

*/

namespace tools
{
void hex_print(const std::string& s)
{
  std::cout << std::hex << std::setfill('0');
  for(unsigned char c : s)
  {
    std::cout << std::setw(2) << static_cast<int>(c) << ' ';
  }
  std::cout << std::dec << '\n';
}


unsigned int getNbreOfByteCode(unsigned char car)
{
  unsigned char mask1 =128;//0b10000000;
  unsigned char oneByte  =0;

  unsigned char mask2 =224;//0b11100000;
  unsigned char twoByte  =192;//0b11000000;

  unsigned char mask3 =240;//0b11110000;
  unsigned char threeByte  =224;//0b11100000;

  unsigned char mask4 =248;//0b11111000;
  unsigned char fourByte  =240;//0b11110000;

  unsigned int byteNbre = 2;

  if( (car&mask1) == oneByte)
  {
    byteNbre = 1;
  }
  else
  if( (car&mask2) == twoByte)
  {
    byteNbre = 2;
  }
  else
  if( (car&mask3) == threeByte)
  {
    byteNbre = 3;
  }
  else
  if( (car&mask4) == fourByte)
  {
    byteNbre = 4;
  }
  //int val = car;
  //std::cout << "getNbreOfByteCode:" << val << ":" << byteNbre << std::endl;
  return   byteNbre;
}

#ifndef QNAP
/*
std::string convertUtf16ToUtf8(std::u16string inStr)
{
  std::string utf8 = std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t>{}.from_bytes(inStr.data());
  return utf16;
}
*/
// Convert a Ut8 to utf16
std::u16string convertUtf8ToUtf16(std::string inStr)
{
  std::u16string utf16 = std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t>{}.from_bytes(inStr.data());
  return utf16;
}

// Convert a Ut16 to utf8


std::string convertWSToUtf8(std::wstring inStr)
{
  std::wstring_convert<std::codecvt_utf8<wchar_t>> conv1;
  std::string u8str = conv1.to_bytes(inStr);
  return u8str;
}

std::string convertWSToUtf16(std::wstring inStr)
{
  std::wstring_convert<std::codecvt_utf16<wchar_t, 0x10ffff, std::little_endian>> conv2;
  std::string u16str = conv2.to_bytes(inStr);
  return u16str;
}

#endif
std::string narrow(const std::wstring& ws)
{
  std::vector<char> buffer(ws.size());

  std::locale loc("fr_FR.UTF-8");

  std::use_facet< std::ctype<wchar_t> >(loc).narrow(ws.data(), ws.data() + ws.size(), '?', &buffer[0]);

  return std::string(&buffer[0], buffer.size());
}



std::wstring convertToWs(std::string str)
{
   //std::cout << "tools::convertToWs: size :" << str.size() << " " << str <<std::endl;

  std::wstring ws;//(str.size()*2, L' '); // Overestimate number of code points.
  unsigned int count = 0;
  for(auto charac: str)
  {
    tools::getNbreOfByteCode(charac);
    ws.push_back(charac);
    count++;
  }

//std::size_t iWritted = std::mbstowcs(&ws[0], str.c_str(), ws.size());
//if ( iWritted == (size_t)-1)
{

}
//else
{
//std::cout << "tools::convertToWs writted:" << iWritted<<  std::endl;
    //std::wcout << "converted:[" <<  ws << "]"<< std::endl;

}

  return ws;
}

std::string convertToSs(std::wstring ws)
{
  std::string str(ws.begin(), ws.end());
  return str;
}

#if 0
int main()
{
  std::string utf8 =  u8"z\u00df\u6c34\U0001d10b"; // or u8"zß水𝄋"
                        // or "\x7a\xc3\x9f\xe6\xb0\xb4\xf0\x9d\x84\x8b";
  // the UTF-8 / UTF-16 standard conversion facet
  std::u16string utf16 = std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t>{}.from_bytes(utf8.data());
  std::cout << "UTF16 conversion produced " << utf16.size() << " code units:\n";
  for (char16_t c : utf16)
  {
    std::cout << std::hex << std::showbase << c << '\n';
  }
  // the UTF-8 / UTF-32 standard conversion facet
  std::u32string utf32 = std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>{}.from_bytes(utf8);
  std::cout << "UTF32 conversion produced " << std::dec << utf32.size() << " code units:\n";
  for (char32_t c : utf32)
  {
    std::cout << std::hex << std::showbase << c << '\n';
  }
}
#endif
}

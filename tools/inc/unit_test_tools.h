/*
 *  Tools Unit Test
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#ifndef __UNIT_TEST_TOOLS_
#define __UNIT_TEST_TOOLS_

#include <cppunit/extensions/HelperMacros.h>
#include "systemtools.h"
#include "stringtools.h"

/*! \namespace tests
 *
 * namespace aboot string and system tools
 */
namespace tests
{
  class UnitTestStringTools: public CPPUNIT_NS::TestFixture
  {
    CPPUNIT_TEST_SUITE( UnitTestStringTools );
    CPPUNIT_TEST( tu_checkAlpha ) ;
    CPPUNIT_TEST( tu_trim ) ;
    CPPUNIT_TEST( tu_split ) ;
    CPPUNIT_TEST( tu_formatName ) ;
    CPPUNIT_TEST( tu_isAForbiddenCharacter ) ;
    CPPUNIT_TEST( tu_isAlphaDigit ) ;
    CPPUNIT_TEST( tu_getNbreOfByteCode ) ;
    CPPUNIT_TEST( tu_replaceForbiddenCharacter ) ;
    CPPUNIT_TEST( tu_findWithoutCase ) ;
    CPPUNIT_TEST( tu_findAndReplace ) ;
    CPPUNIT_TEST( tu_toLowerCase ) ;
    CPPUNIT_TEST( tu_generateUUID ) ;
    CPPUNIT_TEST( tu_splitbySep ) ;
    CPPUNIT_TEST( tu_extractNumberFromString ) ;
    CPPUNIT_TEST( tu_removeBracketTag ) ;
    CPPUNIT_TEST( tu_findFirstTag ) ;
    CPPUNIT_TEST_SUITE_END();

   private:
 //   ParLed56 *parLed56; // un pointeur sur une instance de la classe à tester
    std::string m_stringTest;
    std::string m_arg1;
    std::string m_stringResultTest;
    std::string m_stringWaitingtTest;
    int m_intResultTest;
    int m_intWaitingtTest;
    std::list<std::string> m_listResultTest;
    std::list<std::string> m_listWaitingtTest;
    std::vector<std::string> m_vectorResultTest;
    std::vector<std::string> m_vectorWaitingtTest;
    wchar_t m_charac;
    unsigned char m_unicodeChar[4];
   public:
    UnitTestStringTools();
    virtual ~UnitTestStringTools();
    // Call before tests
    void setUp();
    // Call after tests
    void tearDown();
   // tests list
   void tu_checkAlpha();
   void tu_trim();
   void tu_split() ;
   void tu_formatName();
   void tu_isAForbiddenCharacter();
   void tu_isAlphaDigit();
   void tu_getNbreOfByteCode();
   void tu_replaceForbiddenCharacter();

   void tu_convertToWs();
   void tu_convertToSs();
   void tu_narrow();
   void tu_findWithoutCase();
   void tu_findAndReplace( );
   void tu_toLowerCase();
   void tu_generateUUID();
   void tu_splitbySep();
   void tu_extractNumberFromString();
   void tu_removeBracketTag();
   void tu_findFirstTag();

};

  bool unitTeststools();
#if 0

  bool tu_split() ;
  bool tu_formatName();
  bool tu_isAForbiddenCharacter();

  bool tu_isAlphaDigit();
  bool tu_getNbreOfByteCode();
  bool tu_isAlphaDigit2( );
  bool tu_replaceForbiddenCharacter();
  bool tu_convertToWs();
  bool tu_convertToSs();
  bool tu_narrow();

  bool tu_findWithoutCase();
  bool tu_findWithoutCase();

  bool tu_findAndReplace( );
  bool tu_hasForbiddenCharacter();
  bool tu_toLowerCase();
  bool tu_generateUUID();
  bool tu_splitbySep();
#endif
}
#endif

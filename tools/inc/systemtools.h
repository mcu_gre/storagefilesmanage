/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#ifndef SYSTEM_TOOLS_H
#define  SYSTEM_TOOLS_H

#include <stdio.h>
#include <string>

/*! \namespace tools
 *
 * namespace aboot string and system tools
 */
namespace tools
{
  void setSimulated(bool );
  bool getSimulated();

  /*!
   *  \brief rename a file
   *
   *  \param oldName : the file to rename (full path)
   *  \param newName : the new name (full path)
   *  \param bForce  : force to rename even if the newName is a file which already exists (-> the newName file will be replaced by the oldName file)
   *  \return true if the file has been renamed
   */  
  bool rename_impl(std::string oldName, std::string newName,bool bForce = false);
  bool remove_impl(const std::string& name);
  bool mkdir_impl(std::string newDirectory);
  std::string getExePath();
  
class SystemeException : public std::exception
{
  public:
    enum ErrorId
    {
       E_RENAME_ERROR_FILE_EXIST,
       E_RENAME_UNKNOW_ERROR_FILE
    };
    SystemeException(const char * err):SystemeException(err,0,"",""){};
    SystemeException(const std::string err):SystemeException(err,0,"",""){};
    SystemeException(const std::string err,int aErrno,std::string originFile,std::string newFile):
      m_myerror(err),
      m_errno(aErrno),
      m_originFile(newFile),
      m_newFile(originFile)
      {};
    virtual const char * what () const throw ()
    {
        return m_myerror.c_str();
    };
  
  protected:  
    std::string m_myerror;
    int m_errno;    
    std::string m_originFile;
    std::string m_newFile;
};  
};

#endif

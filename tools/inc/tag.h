/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#ifndef TAG_INFO_H
#define TAG_INFO_H

#include <string>
#include <list>

namespace tools
{
  class Tag
  {
public:
   enum Type
   {
      AUDIO_LANGUAGE,
      VIDEO_RESOLUTION,
      VIDEO_SUBTITLE,
      OTHER_INFO,
      PUB,
   } ;

    Tag(std::string name,std::string nameToSet,Type type, bool enable ):m_name(name),m_nameToSet(nameToSet),m_type(type),m_enable(enable){};
    std::string m_name;
    std::string m_nameToSet;
    Type m_type;
    bool m_enable;
  };

  class NameRefactored
  {
public:
    std::string m_name;
    std::list<Tag> m_tags;
  };
};
#endif

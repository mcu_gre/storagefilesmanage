/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#ifndef CONVERTER_STRING_TOOLS_H
#define  CONVERTER_STRING_TOOLS_H

/*!
 * \file converter.h
 * \brief Provide convert string tools
 * \author Manuel Curcio
 */

/*! \namespace tools
 * 
 * namespace aboot string and system tools 
 */
namespace tools
{
void hex_print(const std::string& s);

  /*!
   *  \brief : return the number of byte used to encode the utf8 charactere
   * 
   *  parse the first byte of a utf8 multi byte character to compute the number of byte used
   *
   *  \param name : the first byte of an utf8 multibyte character
   *  \return the number of byte used to code the utf8 character
   */   
  unsigned int getNbreOfByteCode(unsigned char car);

  std::wstring convertToWs(std::string str);
  std::string convertToSs(std::wstring ws);
  std::string narrow(const std::wstring& ws);
 /*
std::string convertUtf16ToUtf8(std::u16string inStr)
{
  std::string utf8 = std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t>{}.from_bytes(inStr.data());	
  return utf16;	
}
*/
// Convert a Ut8 to utf16
std::u16string convertUtf8ToUtf16(std::string inStr);

std::string convertWSToUtf8(std::wstring inStr);

std::string convertWSToUtf16(std::wstring inStr);
}
#endif


/*
 *  Multimedia Files Management
 *
 * Copyright (C) 2016  by Manuel Curcio <manuel.curcio@gmail.com>
 *
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

#ifndef STRING_TOOLS_H
#define  STRING_TOOLS_H

/*!
 * \file stringtools.h
 * \brief Provide string tools
 * \author Manuel Curcio
 */


#include <string>
#include <vector>
#include <list>
#include "tag.h"

/*! \namespace tools
 *
 * namespace aboot string and system tools
 */
namespace tools
{
  //const char local_filter[] ="fr_FR.UTF-8" ;
  const char local_filter[] ="en_US.UTF8" ;

  /*!
   *  \brief Remove no alpha character
   *
   *  Remove All no alpha characters ; remain some problem with no ascii letter as éç ...
   *
   *  \param str : the string to format
   *  \return the formated
   */
  std::string checkAlpha(std::string str);

  /*!
   *  \brief Remove all space at the beginning and at the end
   *
   *
   *  \param str : the string to trim
   *  \return the trimed string
   */
  std::string trim(const std::string& strToTrim);

  /*!
   *  \brief split a string in several item from a specified separator
   *
   *  Methode to split a string:
   *   - add a maj at the begining
   *   - set all remaining letter in minuscule
   *    An option allowes to ignore the separator if there no space around it
   *    ex: <Hello-World - bye-bye> returns:
   *           <Hello> <World> <bye> <bye> with lookNoSpace at false
   *           <Hello-World> <bye-bye> with lookNoSpace at true   *
   *  \param str : the string to format
   *  \param delim : the delimiter
   *  \param mustHaveSpaceBeforeAfterDelimiter : an option to mandatory a space before and after the delimiter
   *  \return a list of item
   */
  std::list<std::string> split(const std::string &s, char delim,bool mustHaveSpaceBeforeAfterDelimiter=false) ;

  /*!
   *  \brief Format a name
   *
   *  Methode to format a name:
   *   - add a maj at the begining of each word (words are deduced from a   delimiter list: " .-,;:!/\\()&'[]|@{}")
   *   - set all remaining letter in minuscule
   *
   *  \param str : the string to format
   *  \return the formated
   */
  std::string formatName(const std::string& str);

  /*!
   *  \brief Check if the character is a valid character in a file name (windows&linux)
   *
   *
   *  \param character : the character to check
   *  \return true if the character is a valid character
   *  false else
   */
  bool isAForbiddenCharacter(char character);

  /*!
   *  \brief : test if the character is a alphanumerical character
   *
   *  \param character : to character to test
   *  \return true if the character is alphanumeric
   *  false else
   */
  template <class charT>
  bool isAlphaDigit(charT character)
  {
    std::setlocale(LC_ALL, local_filter);
    return std::isalpha(character, std::locale(local_filter))||std::isdigit(character, std::locale(local_filter));
  };


  //bool isAlphaDigit2(wchar_t character);

  /*!
   *  \brief replace all forbidden character by a specified character
   *
   *
   *  \param name : the name to check and modify
   *  \param newCaracter : the character to use to replace forbiden character
   *  \return the transformed name
   */
  std::string replaceForbiddenCharacter(std::string name, char newCaracter);

  /*!
   *  \brief find the first occurence of a string inside another string
   *
   *  \param scrString : the source string
   *  \param strToSearch : the string to find inside the source tring
   *  \return the position of the fisrt occurence of strToSearch inside scrString
   *       or string::npos if not found
   */
  std::size_t findWithoutCase(const std::string&  scrString,const char * strToSearch );

  /*!
   *  \brief find the first occurence of a string inside another string
   *
   *  \param scrString : the source string
   *  \param strToSearch : the string to find inside the source tring
   *  \return the position of the fisrt occurence of strToSearch inside scrString
   *       or string::npos if not found
   */
  std::size_t findWithoutCase(const std::string&  strToCompare,std::string & strToSearch );

  /*!
   *  \brief find the first occurence of a string inside another string
   *
   *  \param scrString : the source string
   *  \param strToSearch : the string to find inside the source tring
   *  \return the position of the fisrt occurence of strToSearch inside scrString
   *       or string::npos if not found
   */
  std::string findAndReplace( std::string  subject, const std::string &  search,  const std::string &   sreplace );

  bool hasForbiddenCharacter(const std::string& name);

  /*!
   *  \brief replace all high case in lower case
   *
   *  \param scrString : the source string
   *  \return the string in lower case
   */
  std::string toLowerCase(const std::string & src);

  /*!
   *  \brief generate a random uid
   *  The uid has the following form: 7545e14679e2a9e3-41b71efb-41f2-99ab-3d1b58ba46e87ccd238e1f29
   *
   *  \return a random uid string
   */
  std::string generateUUID();

  /*!
   *  \brief split a string in three parts from a couple of separator
   *    A couple of seperator is a specific delimiter to open and another one (different) to close
   *      for example : [] or () or <> ...
   *   example: splitbySep( "my name is <robert> and I am french", "<>");
   *        returns a vector of 3 elements: { "my name is","robert", "and I am french"}
   *
   *  \param name : the source string
   *  \param sep : couple of separator : "[]", or "()" or "<>", or ...
   *  \return a vector of three element or empty if not couple of seperator is found
   *       or string::npos if not found
   */
  std::vector<std::string> splitbySep(std::string name,std::string sep);

  /*!
   *  \brief extract a number from a string
   *     It is possible to remove a psot fixe string before the number by using prefix param

   *   example: "Tome: 5" with prefix "Tome:" return 5
   *   example: "5 azeae" without prefix (nullptr) return 5
   *   example: "Tome: cinq"  return false
   *
   *  \param strNumber : the source string
   *  \param sep : an optional prefixber
   *  \return the number if success
   *       or an exception is throw
   */
  int extractNumberFromString(std::string strNumber,const std::string * prefix);

  /*!
   *  \brief find the first tag
   *
   *  \param name : the name
   *  \param tagList : a list of tag
   *  \return the position of the first tag found
   *       or std::string:npos
   */
  std::size_t findFirstTag(std::string name,std::vector<Tag>& tagList);

  /*!
   *  \brief remove bracket
   *
   *  \param name : the name
   *  \return the name without bracket
   */
  std::string removeBracketTag(std::string name);
  
  /*!
   *  \brief find and extract end of substring
   *
   *   For example, a name is my.title.movietype-horror.duration-164.mkv
   *     I know there are movietype-XXXX substring at 9 position and i want get XXXX substring
   *     I call getEndPrefix("my.title.movietype-horror.duration-164.mkv","movietype-",9,'.');
   * 
   *  \param name : the name to check and modify
   *  \param prefix : the character to use to replace forbiden character
   *  \param startPosition : the character to use to replace forbiden character
   *  \param separator : the character to use to replace forbiden character
   *  \return the end of prefix name
   */
  std::string getEndPrefix(std::string name, std::string prefix, size_t startPosition,char separator);
};

#endif

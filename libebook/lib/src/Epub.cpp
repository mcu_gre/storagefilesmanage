/*
 * Epub
 * Support for epub ebooks
 *
 * Author:  Domenico Rotiroti
 * License: GPL3 (see COPYING)
 */

#include "Epub.h"
#include "JsonObj.h"
#include "Xml.h"
#include "Utils.h"
#include <algorithm>
#include <iostream>
#include "stringtools.h"

const string Epub::dcns = "http://purl.org/dc/elements/1.1/";
const string Epub::dcpref = "dc";
const string Epub::opfns = "http://www.idpf.org/2007/opf";
const string Epub::opfpref = "opf";

Epub *  Epub::createFromFile(const char *fileName) {
    Epub * book = new Epub();
    book->zf = new Zip(fileName);

    if(!book->check())
    {
      delete book;
      return NULL;
    }

    return book;
}

bool Epub::searchObfContent(std::string &opfpath)
{
  bool obfFileFound = false;
  if(zf->hasFile("META-INF/container.xml"))
  {
    // read opf path from container
    string container = zf->getFile("META-INF/container.xml");
    Xml cx(container);
    Xpath cp = cx.xpath();
// Query is : tag@aatribut
    vector<string> xr = cp.query("//rootfile/@full-path");
    if(xr.size() != 0 )
    {
      obfFileFound = true;
      opfpath = xr[0];
    }
    else
    {
      std::cerr << "Epub::searchObfContent: no obf path found in container.xml (reading error)" << std::endl;
    }
  }
  if(!obfFileFound)
  {
    // try content.opf
    opfpath = "content.opf";
    if(zf->hasFile(opfpath.c_str()))
    {
      obfFileFound = true;
    }
    else
    {
      std::cerr << "Epub::searchObfContent content.opf not found" << std::endl;
    }
  }
  return obfFileFound;
}

std::string extractAuter(Xpath& ox)
{
  std::string author;
  std::string query = "//dc:creator[@opf:role=\"aut\"]/@opf:file-as";
  vector<string> xrAuthor = ox.query(query);
  if(xrAuthor.size() != 0 )
    author = tools::trim(xrAuthor[0]);
  else
  {
    query = "//dc:creator/@opf:file-as";
    xrAuthor = ox.query(query);
    if(xrAuthor.size() != 0 )
      author = tools::checkAlpha(tools::trim(xrAuthor[0]));
  }
  return author;
}

bool Epub::check()
{
    //this checks both archive validity and mimetype presence
    if(!zf->hasFile("mimetype"))
    {
      std::cerr << "Epub::check: no mimetype" << std::endl;
// But suppose zip file
//      return false;
    }
    string opfpath;
    if(!searchObfContent(opfpath))
    {
      return false;
    }

    base = opfpath.substr(0, opfpath.find_last_of('/')+1);
    // parse opf
    nslist * ns = new nslist();
    string opfxml = zf->getFile(opfpath);
    (*ns)[dcpref] = dcns;
    (*ns)[opfpref] = opfns;
    string mydc = (opfxml.find("<title")==string::npos ? "//dc:" : "//");
    string myopf = (opfxml.find("<item")==string::npos ? "//opf:" : "//");
    string mymeta = (opfxml.find("<meta")==string::npos ? "//opf:" : "//");
    Xml opf(opfxml);
    Xpath ox = opf.xpath(ns);
// GEt is tag as argu and return the text content


//<meta name="calibre:title_sort" content="Shaan !"/>
    title = ox.get(mydc+"title");
    author = extractAuter(ox);
    vector<string> xr;

    publisher = ox.get(mydc+"publisher");
    // Search isbn opf:scheme="ISBN"
    std::string queryIsbn= "//dc:identifier[@opf:scheme=\"ISBN\"]";
    vector<string> xrIsbn = ox.query(queryIsbn);
    if(xrIsbn.size() != 0 )
       m_isbn=xrIsbn[0];

    std::string queryUUId= "//dc:identifier[@opf:scheme=\"uuid\"]";
    vector<string> xrUUId = ox.query(queryUUId);
    if(xrUUId.size() != 0 )
       m_uuid=xrUUId[0];

    std::string querySerieIndex = "//meta[@name=\"calibre:series_index\" and @content]/@content";
   // std::cout << querySerieIndex << std::endl;
    vector<string> xr3 = ox.query(querySerieIndex);
    if(xr3.size() != 0 )
       m_serieNum=xr3[0];

    std::string querySerieName = "//meta[@name=\"calibre:series\" and @content]/@content";
    //std::cout << querySerieName << std::endl;
    xr3 = ox.query(querySerieName);
    if(xr3.size() != 0 )
       m_serieName=xr3[0];

    std::string subject = "//dc:subject";
    //std::cout << querySerieName << std::endl;
    m_subject = ox.query(subject);
    std::sort(m_subject.begin(), m_subject.end());


// qqserieName
#if 0
    std::string queryProspect = "//meta[@name and @content]/@name";
    //std::cout << queryProspect << std::endl;
    vector<string> xrqueryProspect = ox.query(queryProspect);
    for(auto stt:xrqueryProspect)
    {
     std::cout << "meta query:"<< stt <<std::endl;
    }
    std::cout << "End of meta query" <<std::endl;
#endif
///////////////

    // cover info
    string coverId = ox.get("//meta[@name='cover']/@content");
    string coverHref = ox.get(myopf+"item[@id='"+coverId+"']/@href");

    // Items:
    // get //itemref/@idref and read the item's href
    xr = ox.query(myopf+"itemref/@idref");
    string ix;
    vector<string> nestx;
    for(vector<string>::iterator it = xr.begin(); it != xr.end(); ++it) {
  ix = myopf+"item[@id='";
  ix.append(*it);
  ix.append("']/@href");
  nestx = ox.query(ix);
  if(nestx.size()>0) {
      items.push_back(nestx[0]);
  }
    }

    // Resources:
    // <item> not in items vector
    xr = ox.query(myopf+"item/@href");
    for(vector<string>::iterator it = xr.begin(); it != xr.end(); ++it) {
  if(std::find(items.begin(), items.end(), *it) == items.end()) {
    if(coverHref.compare(*it) == 0) coverIndex = resources.size();
    resources.push_back(*it);
  }
    }
    delete ns;

    return true;
}

Dumper * Epub::getDumper(const char * outdir) {
    return new EpubDumper(this, outdir);
}

Epub::~Epub() {
    delete zf;
}

void EpubDumper::dumpMetadata() {
    JsonObj meta;
    meta.add("author", book->getAuthor());
    meta.add("title", book->getTitle());
    meta.add("publisher", book->getPublisher());
    if(epub->getCover() >= 0)
  meta.add("cover", epub->resourceNames()[epub->getCover()]);
    vector<JsonObj> res;
    for(int i = 0; i < epub->resourceCount(); ++i) {
  JsonObj ares;
  ares.add("path", epub->resourceNames()[i]);
  res.push_back(ares);
    }
    meta.add("res", res);
    vector<string> itemNames = epub->itemNames();
    meta.add("items", itemNames);

    write("info.json", meta.json());
}

void EpubDumper::dumpText() {
    vector<string> items = epub->itemNames();
    int pos;
    vector<string>::iterator it;
    for(it = items.begin(), pos = 0; it != items.end(); ++it, ++pos) {
  write(it->c_str(), epub->getItem(pos));
    }
}

void EpubDumper::dumpResources() {
    vector<string> items = epub->resourceNames();
    int pos;
    vector<string>::iterator it;
    for(it = items.begin(), pos = 0; it != items.end(); ++it, ++pos) {
  vector<unsigned char> res = epub->getResource(pos);
  write(it->c_str(), (char*)&res[0], res.size());
    }
}


# Color definitions
VERT="\\033[1;32m"
NORMAL="\\033[0;39m"
ROUGE="\\033[1;31m"
ROSE="\\033[1;35m"
BLEU="\\033[1;34m"
BLANC="\\033[0;02m"
BLANCLAIR="\\033[1;08m"
JAUNE="\\033[1;33m"
CYAN="\\033[1;36m"

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

function printMsg()
{
  echo -e "$CYAN" "$1" "$NORMAL"
}

function printErrorMsg()
{
  echo -e "$ROUGE" "$1" "$NORMAL"
}


function cleanMsg()
{
   printMsg "********************************"
   printMsg "******** Cleaning $1"
   printMsg "********************************"
}

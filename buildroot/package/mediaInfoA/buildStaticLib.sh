
arCmd=$1
srcLib=$2
dstLib=$3
libName=$4
ranLib=$5
tempDir=$dstLib/tempdir


check_status() {
    ret=$?
    if [ "$ret" -ne "0" ];
    then
        echo "Failed: $1"
        case $2 in
           e) echo "aborting.."
           exit 1
         ;;
        *)
        esac
    fi
}


mkdir -p $tempDir
cd $tempDir
cp $srcLib/libmediainfo.a .
check_status "libmediainfo.a" "e"
#cp $srcLib/libtools.a .
cp $srcLib/libzen.a .
check_status "libzen.a" "e"
$arCmd x libmediainfo.a
#$arCmd x libtools.a
$arCmd x libzen.a
echo "rm $dstLib/lib$libName.a"
rm $dstLib/lib$libName.a
echo "$arCmd q $dstLib/lib$libName.a *.o"
$arCmd q $dstLib/lib$libName.a *.o
$ranLib $dstLib/lib$libName.a

sizeFile=$(du -smh $dstLib/lib$libName.a | awk '{print $1}')
echo "done !!" $(ls -l $dstLib/lib$libName.a) " : $sizeFile)"

rm *.o
rm *.a
cd ..
rmdir $tempDir

TEST_VERSION = 1
TEST__VERSION_MAJOR = 1.0
TEST_VERSION = 1.0
TEST_SITE = $(BR2_EXTERNAL_QNAP_STORAGE_PATH)/../../test
TEST_SITE_METHOD = local
TEST_INSTALL_STAGING = YES
TEST_INSTALL_TARGET = YES
TEST_DEPENDENCIES+= zenlib

$(eval $(cmake-package))
